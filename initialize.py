#!/usr/bin/env python3
# from script.run import Run
import sys
import re
from script.run import Run
import script.user as user
from script.urls import URLS

if len(sys.argv) < 2:
    print("Please supply the base URL (https://qwerty.appspot.com)")
    sys.exit()

url = sys.argv[1]

if url.endswith('/'):
    url = url[:-1]

print('Using : ' + url)

urls = URLS().set_url(url)

ok, status, reply = user.ping()

if not ok:
    print(f'Invalid URL: {url}\nPing returned {status}, cause: {reply}')
    sys.exit()

fill = input('Initialize or Fill (I || F): ').upper() == 'F'

if fill:
    print('TODO: Not available')
    sys.exit()

Run(url, fill)
