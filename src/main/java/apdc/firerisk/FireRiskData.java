package apdc.firerisk;

public class FireRiskData {
    public final String windDirection;
    public final int windForce;
    public final float maxHumidity;
    public final float minHumidity;
    public final float maxTemperature;
    public final float minTemperature;
    public final int fireRisk;
    public final int rainRisk;

    public FireRiskData(String windDirection, int windForce, float maxHumidity, float minHumidity, float maxTemperature, float minTemperature, int fireRisk, int rainRisk) {
        this.windDirection = windDirection;
        this.windForce = windForce;
        this.maxHumidity = maxHumidity;
        this.minHumidity = minHumidity;
        this.maxTemperature = maxTemperature;
        this.minTemperature = minTemperature;
        this.fireRisk = fireRisk;
        this.rainRisk = rainRisk;
    }
}
