package apdc.firerisk;

public class FireRiskStats {
    public final int maxF;
    public final int minF;
    public final double avgF;

    public FireRiskStats(int maxF, int minF, double avgF) {
        this.maxF = maxF;
        this.minF = minF;
        this.avgF = avgF;
    }
}