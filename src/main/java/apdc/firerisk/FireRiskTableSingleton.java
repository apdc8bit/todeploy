package apdc.firerisk;

import apdc.dico.DicoTableSingleton;
import apdc.input.verification.values.NotificationValues;
import apdc.output.info.CountyInfo;
import apdc.util.CalendarUtils;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

public class FireRiskTableSingleton {
    private final Logger LOG = Logger.getLogger(FireRiskTableSingleton.class.getName());
    private static final int DAYS = 1;

    private JsonObject risk;
    private Date loadTime;

    private static FireRiskTableSingleton instance = null;

    private FireRiskTableSingleton() {

    }

    public static FireRiskTableSingleton getInstance() {
        if (instance == null) {
            instance = new FireRiskTableSingleton();
        }
        return instance;
    }

    public FireRiskData getRisk(String dico) {
        checkJson();

        try {
            JsonObject local = risk.getAsJsonObject(dico);
            JsonObject data = local.getAsJsonObject("data");

            String windDirection = data.get("ff_dir_id").getAsString();
            int windForce = data.get("ff_int_id").getAsInt();
            float maxHumidity = data.get("hrMax").getAsFloat();
            float minHumidity = data.get("hrMin").getAsFloat();
            float maxTemperature = data.get("tMax").getAsFloat();
            float minTemperature = data.get("tMin").getAsFloat();
            int fireRisk = data.get("rcm").getAsInt();
            int rainRisk = data.get("rr_id").getAsInt();

            return new FireRiskData(windDirection, windForce, maxHumidity, minHumidity, maxTemperature, minTemperature, fireRisk, rainRisk);
        } catch (Exception e) {
            LOG.warning("DICO: " + dico);
            LOG.severe("Error getting fire risk data" + e.getMessage());
            return new FireRiskData("?", -1, -1, -1, -1, -1, -1, -1);
        }
    }

    public List<CountyInfo> getAtRiskCounties() {
        checkJson();

        int threshold = 4;
        List<CountyInfo> out = new LinkedList<>();
        DicoTableSingleton dico = DicoTableSingleton.getInstance();

        try {
            for (Map.Entry<String, JsonElement> v : risk.entrySet()) {
                JsonObject e = v.getValue().getAsJsonObject().getAsJsonObject("data");

                int f = e.get("rcm").getAsInt();

                if (f >= threshold) {
                    String county = dico.getInfoFromDico(v.getKey()).local;
                    if (county != null) {
                        out.add(new CountyInfo(county, v.getKey()));
                        NotificationValues.userNearRisk(f, v.getKey());
                    }
                }
            }
            return out;
        } catch (Exception e) {
            LOG.severe("Error getting fire risk stats");
            return out;
        }
    }

    public FireRiskStats getFireRiskStats() {
        checkJson();

        try {
            int maxF = Integer.MIN_VALUE, minF = Integer.MAX_VALUE;
            double avgF = 0.0;
            int counter = 1;
            for (Map.Entry<String, JsonElement> v : risk.entrySet()) {
                JsonObject e = v.getValue().getAsJsonObject().getAsJsonObject("data");

                //Get the fire risk of the county
                int f = e.get("rcm").getAsInt();

                //Calculate the max and min
                if (f > maxF)
                    maxF = f;
                if (f < minF)
                    minF = f;

                //Calculates the avg
                avgF = avgF == 0.0 ? f : ((avgF * counter) + f) / ++counter;
            }

            return new FireRiskStats(maxF, minF, avgF);
        } catch (Exception e) {
            LOG.severe("Error getting fire risk stats");
            return new FireRiskStats(-1, -1, -1);
        }
    }

    private void checkJson() {
        if (risk == null || loadTime == null || isExpired(loadTime))
            getJsonFromPage();
    }

    private static boolean isExpired(Date loadDate) {
        return CalendarUtils.addDays(loadDate, DAYS).before(new Date());
    }

    private void getJsonFromPage() {
        /*
        Loads the entire html page and uses jsoup to
        go to the <script> element number 11
        then gets the index of the variable name that contains
        the fire risk data and adds 8 to get the index of the start of the json data
        then does the same for the end of the json
        then parses the json into a JsonObject tha can be easily used.
        The data is updated every day
         */

        try {
            Document doc = Jsoup.connect("https://www.ipma.pt/pt/ambiente/risco.incendio/index.jsp").get();
            Elements scriptElements = doc.getElementsByTag("script");
            int a = 0;
            for (Element element : scriptElements) {
                a++;
                if (a == 11) {
                    int index = element.html().indexOf("rcmF[0]=") + 8;
                    int last = element.html().indexOf("rcmF[1]=") - 9;
                    String json = element.html().substring(index, last);
                    JsonParser parser = new JsonParser();
                    JsonObject o = parser.parse(json).getAsJsonObject();
                    risk = o.getAsJsonObject("local");
                    LOG.info("Risk date: " + o.get("dataPrev"));
                    loadTime = new Date();
                }
            }

        } catch (IOException e) {
            LOG.severe("IMPA is down!");
        }
    }
}
