package apdc.util.email;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.io.File;
import java.io.IOException;
import java.util.logging.Logger;

public class EmailResource {
    private static final Logger LOG = Logger.getLogger(EmailResource.class.getName());

    public static void sendEmailValidation(String username, String email, String key) {
        String keyURL = "https://gaea-8bit.appspot.com/#/email/" + key;

        String emailText = loadHTML("emailvalidation");

        if (emailText == null)
            emailText = EMAIL_CONFIRMATION_BODY.concat(keyURL);

        emailText = emailText.replace("##verify-email##", keyURL);
        emailText = emailText.replace("##username##", username);

        GoogleMail.sendEmail(email, EMAIL_CONFIRMATION_SUBJECT, emailText);
        LOG.info("Email sent: " + keyURL);
    }

    public static void sendPasswordResetEmail(String user, String email, String key) {
        String keyURL = "https://gaea-8bit.appspot.com/#/recoverpassword/" + key;

        String emailText = loadHTML("passwordrecovery");

        if (emailText == null)
            emailText = PASSWORD_RESET_BODY.concat(keyURL);

        emailText = emailText.replace("##recover-password##", keyURL);

        GoogleMail.sendEmail(email, PASSWORD_RESET_SUBJECT, emailText);
        LOG.info("Email sent: " + keyURL);
    }

    public static void sendAccountDeletion(String username, String email, String key) {
        String keyURL = "https://gaea-8bit.appspot.com/#/deleteUser/" + key;

        String emailText = loadHTML("accountelimination");

        if (emailText == null)
            emailText = ACCOUNT_DEL_BODY.concat(keyURL);

        emailText = emailText.replace("##eliminate-account##", keyURL);

        GoogleMail.sendEmail(email, ACCOUNT_DEL_SUBJECT, emailText);
        LOG.info("Email sent: " + keyURL);
    }

    public static void sendNewEmailConfirmation(String username, String email) {
        String emailText = loadHTML("emailvalidation");

        if (emailText == null)
            emailText = CONFIRMATION_RESET_BODY;

        emailText = emailText.replace("##username##", username);

        GoogleMail.sendEmail(email, CONFIRMATION_RESET_SUBJECT, emailText);
        LOG.info("Email sent");
    }

    public static void sendEntityRegistration(String username, String email) {
        String emailText = loadHTML("entityregistration");

        if (emailText == null)
            emailText = ENTITY_REGISTRATION_BODY;

        emailText = emailText.replace("##username##", username);

        GoogleMail.sendEmail(email, ENTITY_REGISTRATION_SUBJECT, emailText);
        LOG.info("Email sent");
    }

    public static void sendEntityRegistrationResult(String username, String email, boolean result) {
        //TODO needs html file
        if (result)
            GoogleMail.sendEmail(email, "O seu pedido foi aceite", "Agora poderá trabalhar conosco para criar um futuro melhor!");
        else
            GoogleMail.sendEmail(email, "O pedido foi rejeitado", "Não aprovamos do seu pedido");

        LOG.info("Email sent");
    }

    public static void sendLoginNewIP(String username, String email, String city) {
        String emailText = loadHTML("loginnewcity");

        if (emailText == null)
            emailText = LOGIN_NEW_CITY_BODY;

        emailText = emailText.replace("##email##", city);

        GoogleMail.sendEmail(email, LOGIN_NEW_CITY_SUBJECT, emailText);
        LOG.info("Email sent");
    }

    public static void sendAdminAccountCompromised(String username, String email) {
        String emailText = loadHTML("adminblocked");

        if (emailText == null)
            emailText = ADMIN_BLOCKED_BODY;

        emailText = emailText.replace("##username##", username);

        GoogleMail.sendEmail(email, ADMIN_BLOCKED_SUBJECT, emailText);
        LOG.info("Email sent");
    }

    public static void sendJobConfirmed(String username, String email, boolean confirmed, long id_job) {
        //TODO needs html file
        if (confirmed)
            GoogleMail.sendEmail(email, "O Trabalho foi aceite", "Boa!\nID do Trabalho: " + id_job);
        else
            GoogleMail.sendEmail(email, "O Trabalho foi rejeitado", "Ve se trabalhas como deve ser!\nID do Trabalho: " + id_job);

        LOG.info("Email sent");
    }

    private static String loadHTML(String fileName) {
        try {
            //Load file and add username and email
            String filename = new java.io.File("").getAbsolutePath().concat(String.format("/secret/%s.html", fileName));
            Document doc = Jsoup.parse(new File(filename), "utf-8");
            return doc.html();
        } catch (IOException e) {
            //If file cannot be loaded, use basic template
            return null;
        }
    }

    public static void dev(String text) {
        GoogleMail.sendEmail("d.carpinteiro@campus.fct.unl.pt", "SERVER", text);
    }

    private static final String ADMIN_BLOCKED_SUBJECT = "A sua conta foi bloqueada";
    private static final String ADMIN_BLOCKED_BODY = "Reparamos num numero de logins falhados e a sua conta foi bloqueada por 24 horas";

    private static final String LOGIN_NEW_CITY_SUBJECT = "Foi você que fez login?";
    private static final String LOGIN_NEW_CITY_BODY = "Reparamos que fez login de outro local, voi você?\nSenão foi por favor vá aqui: //TODO link pagina pass change";

    private static final String ENTITY_REGISTRATION_SUBJECT = "O seu pedido foi submetido";
    private static final String ENTITY_REGISTRATION_BODY = "Terá que aguardar que o seu pedido seja confirmado\nObrigado";

    private static final String CONFIRMATION_RESET_SUBJECT = "Email alterado!";
    private static final String CONFIRMATION_RESET_BODY = "O seu email foi alterado";

    private static final String PASSWORD_RESET_SUBJECT = "Recuperação da password";
    private static final String PASSWORD_RESET_BODY = "Por favor, clique no link para redefinir a sua password.\n";

    private static final String ACCOUNT_DEL_SUBJECT = "Confirmação da eliminaçao da conta";
    private static final String ACCOUNT_DEL_BODY = "Por favor clique no link para eliminar a sua conta";

    private static final String EMAIL_CONFIRMATION_SUBJECT = "Validação do email";
    private static final String EMAIL_CONFIRMATION_BODY = "Por favor clique no link para validar o seu email: ";
}
