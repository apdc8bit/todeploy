package apdc.util;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.logging.Logger;

public class Notifications {
    private static final Logger LOG = Logger.getLogger(Notifications.class.getName());
    private static final Gson g = new GsonBuilder().disableHtmlEscaping().create();
    private static final String APP_ID = "5fed326c-45c4-4da6-aef8-d37094efa08d";

    public static void sendToTag(String key, String value, String title, String body) {
        FilterTag[] filters = {new FilterTag("tag", key, "=", value)};

        String payload = g.toJson(new PayloadTag(
                APP_ID, filters, new Contents(body), new Headings(title)
        ));

        sendNotification(payload);
    }

    public static void sendtoUserID(List<String> userIDs, String title, String body) {

        String payload = g.toJson(new PayloadID(
                APP_ID, userIDs, new Contents(body), new Headings(title)
        ));

        sendNotification(payload);
    }


    public static void sendtoAll(String title, String body) {
        String[] filters = {"All"};


        String payload = g.toJson(new PayloadAll(
                APP_ID, filters, new Contents(body), new Headings(title)
        ));

        sendNotification(payload);
    }

    private static JsonArray getLocationFilter(double lat, double lng, long radius) {
        //Because 'long' as a json key name was a good idea, smh
        JsonObject location = new JsonObject();

        location.addProperty("field", "location");
        location.addProperty("radius", "" + radius);
        location.addProperty("lat", "" + lat);
        location.addProperty("long", "" + lng);

        JsonArray array = new JsonArray();
        array.add(location);

        return array;
    }

    public static void sendtoLocation(double lat, double lng, long radius, String title, String body, boolean delay) {
        JsonObject json = new JsonObject();
        json.add("filters", g.toJsonTree(getLocationFilter(lat, lng, radius)));
        json.addProperty("app_id", APP_ID);
        json.add("contents", g.toJsonTree(new Contents(body)));
        json.add("headings", g.toJsonTree(new Headings(title)));

        if (delay) {
            json.addProperty("delayed_option", "timezone");
            json.addProperty("delivery_time_of_day", "9:00AM");
        } else {
            json.addProperty("ttl", "86400");
        }

        String payload = g.toJson(json);
        sendNotification(payload);
    }

    private static void sendNotification(String payload) {
        try {
            URI uri = new URI("https://onesignal.com/api/v1/notifications");
            Client client = ClientBuilder.newClient();
            WebTarget target = client.target(uri);

            Response resp = target
                    .request()
                    .header("Authorization", "Basic MDYwYWFhYTMtYjBiMi00NzJlLTlhZDctMTdkMDE1NDJhN2I0")
                    .post(Entity.entity(payload, MediaType.APPLICATION_JSON));

            LOG.info("OneSignal: " + resp.getStatus());

        } catch (URISyntaxException e) {
            LOG.warning("OneSignal: " + e.getMessage());
        }
    }

    private static class Contents {
        public final String pt;
        public final String en;

        public Contents(String pt) {
            this.pt = pt;
            this.en = pt;
        }
    }

    private static class Headings {
        public final String pt;
        public final String en;

        public Headings(String pt) {
            this.pt = pt;
            this.en = pt;
        }
    }

    private static class Payload {
        public final String app_id;
        public final Contents contents;
        public final Headings headings;

        public Payload(String app_id, Contents contents, Headings headings) {
            this.app_id = app_id;
            this.contents = contents;
            this.headings = headings;
        }
    }

    public static class PayloadTag extends Payload {
        public final FilterTag[] filters;

        public PayloadTag(String app_id, FilterTag[] filters, Contents contents, Headings headings) {
            super(app_id, contents, headings);
            this.filters = filters;
        }
    }

    public static class PayloadAll extends Payload {
        public final String[] included_segments;

        public PayloadAll(String app_id, String[] included_segments, Contents contents, Headings headings) {
            super(app_id, contents, headings);
            this.included_segments = included_segments;
        }
    }

    public static class PayloadID extends Payload {
        public final List<String> include_player_ids;

        public PayloadID(String app_id, List<String> include_player_ids, Contents contents, Headings headings) {
            super(app_id, contents, headings);
            this.include_player_ids = include_player_ids;
        }
    }

    public static class Filter {
        public final String field;

        public Filter(String field) {
            this.field = field;
        }
    }

    public static class FilterTag extends Filter {
        public final String key;
        public final String relation;
        public final String value;

        public FilterTag(String field, String key, String relation, String value) {
            super(field);
            this.key = key;
            this.relation = relation;
            this.value = value;
        }
    }

    public static class FilterLocation extends Filter {
        public final String radius;
        public final String lat;
        public final String lng;

        public FilterLocation(String field, String radius, String lat, String lng) {
            super(field);
            this.radius = radius;
            this.lat = lat;
            this.lng = lng;
        }
    }

}

