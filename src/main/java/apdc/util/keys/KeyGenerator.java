package apdc.util.keys;

import apdc.exceptions.ExpiredKeyException;
import apdc.exceptions.InvalidKeyException;
import apdc.exceptions.InvalidTextException;
import apdc.util.encryption.Encryption;
import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;

import java.util.logging.Logger;

public class KeyGenerator implements KeyInterface {
    private static final Logger LOG = Logger.getLogger(KeyGenerator.class.getName());
    private static final DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();

    public static String getCypher(String value, String sessionID, Type type) throws InvalidTextException {
        long expiration;

        //Expiration time depends on device
        if (type.equals(Type.MOBILE))
            expiration = System.currentTimeMillis() + EXPIRATION_TIME_MOBILE;
        else
            expiration = System.currentTimeMillis() + EXPIRATION_TIME_WEB;

        //Encrypt and return cypher
        return Encryption.encrypt(type + DIVIDER + value + DIVIDER + expiration, sessionID);
    }

    public static String[] getValues(String cypher, String sessionID, Type type) throws InvalidKeyException, ExpiredKeyException {
        //Decrypt and get all the values
        String values[] = Encryption.decrypt(cypher, sessionID).split(DIVIDER);

        //Check size
        if (values.length != SIZE) {
            LOG.info("Size does not match");
            throw new InvalidKeyException(type.name());
        }

        //Check values are not null or empty
        for (String s : values) {
            if (s == null || s.isEmpty()) {
                LOG.info("A value is empty");
                throw new InvalidKeyException(type.name());
            }
        }

        //Confirm the type matches
        if (!type.equals(KeyInterface.Type.PICTURE))
            if (!values[TYPE].equals(type.name())) {
                LOG.info("Type does not match: " + values[TYPE] + " != " + type.name());
                throw new InvalidKeyException(type.name());
            }

        //Confirm it is not expired
        Long expiration = Long.parseLong(values[EXPIRATION]);

        if (expiration == 0L) {
            LOG.info("Invalid expiration time");
            throw new InvalidKeyException(type.name());
        }

        if (System.currentTimeMillis() > expiration) {
            LOG.info("Token is expired");
            throw new ExpiredKeyException(type.name());
        }

        /*
        //Check if is not in the invalid tokens table
        try {
            Key tokenKey = KeyFactory.createKey(InvalidToken.kind, cypher);
            datastore.get(tokenKey);
            LOG.info("Token is marked has invalid (logout)");
            throw new InvalidKeyException(type.name());
        } catch (EntityNotFoundException e) {
            //token is valid
            LOG.info("token is ok!");
            return values;
        }
        */
        return values;
    }

    public static void invalidateToken(String cipherText, String sessionID, Type type) {
        try {
            Key key = KeyFactory.stringToKey(cipherText);
            datastore.delete(key);
        } catch (Exception e) {
            LOG.warning("Invalid Key");
        }


        /*
        String[] values;
        //See if token is valid
        try {
            values = getValues(cipherText, sessionID, type);
        } catch (InvalidKeyException | ExpiredKeyException e) {
            //Token is not valid, do nothing
            return;
        }
        //Add token to the table with his expiration date
        Long expiration = Long.parseLong(values[KeyInterface.EXPIRATION]);
        Entity tokenEntity = new Entity(InvalidToken.kind, cipherText);
        tokenEntity.setProperty(InvalidToken.expirationDate, expiration);
        datastore.put(tokenEntity);
        */

        LOG.info("Token Invalidated");
    }

    public static Type getTypeFromDevice(String device) {
        if (device.equalsIgnoreCase("m"))
            return Type.MOBILE;
        else
            return Type.TOKEN;
    }


}