package apdc.util.encryption;

import apdc.exceptions.InvalidKeyException;
import apdc.exceptions.InvalidTextException;
import apdc.tables.user.Tokens;
import apdc.util.keys.KeyGenerator;
import com.google.appengine.api.datastore.*;

import java.util.logging.Logger;

public class Encryption {

    /*

    Encryption was removed in favor of storing the tokens in the DB
    because the java library the handles the encryption is very slow
    when running on google's servers (avg 4 seconds to encrypt and decrypt)

     */


    private static final Logger LOG = Logger.getLogger(Encryption.class.getName());
    private static final DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();

    /*
    private static final String ALGORITHM = "AES";// /CBC/PKCS5Padding initialize vector
    private static final String BASE_KEY = "MZygpewJsCpRrfOr";
    private static byte[] salt = {
            (byte) 0xc7, (byte) 0x73, (byte) 0x21, (byte) 0x8c,
            (byte) 0x7e, (byte) 0xc8, (byte) 0xee, (byte) 0x99
    };
    */

    /*
    private static SecretKeySpec generateKey(String value) {
        try {
            SecretKeyFactory factory = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1");
            KeySpec spec = new PBEKeySpec((BASE_KEY + value).toCharArray(), salt, 65536, 256);
            SecretKey tmp = factory.generateSecret(spec);
            return new SecretKeySpec(tmp.getEncoded(), ALGORITHM);
        } catch (Exception e) {
            return null;
        }

        return KeyFactory.keyToString(values.getKey());
    }
*/

    public static String encrypt(String plainText, String partKey) throws InvalidTextException {
        /*
        try {
            SecretKeySpec secretKey = generateKey(partKey);
            Cipher cipher = Cipher.getInstance(ALGORITHM);
            cipher.init(Cipher.ENCRYPT_MODE, secretKey);
            byte[] encryptText = cipher.doFinal(plainText.getBytes());
            return DatatypeConverter.printHexBinary(encryptText); //Encrypted Hexadecimal Text
        } catch (Exception e) {
            LOG.warning("Error encrypting token");
            throw new InvalidTextException();
        }
        */

        LOG.info("T: " + plainText);
        LOG.info("S: " + partKey);
        Entity values = new Entity(Tokens.kind, partKey);
        values.setProperty(Tokens.value, plainText);

        long expirationTime = Long.parseLong(plainText.split(KeyGenerator.DIVIDER)[KeyGenerator.EXPIRATION]);
        values.setProperty(Tokens.expirationDate, expirationTime);

        datastore.put(values);

        return KeyFactory.keyToString(values.getKey());
    }

    public static String decrypt(String cipherText, String partKey) throws InvalidKeyException {
        /*
        try {
            SecretKeySpec secretKey = generateKey(partKey);
            Cipher cipher = Cipher.getInstance(ALGORITHM);
            cipher.init(Cipher.DECRYPT_MODE, secretKey);
            byte[] encryptedText = hexStringToByteArray(cipherText);
            byte[] plainText = cipher.doFinal(encryptedText);
            return new String(plainText);
        } catch (Exception e) {
            LOG.warning("Error decrypting token");
        }
        */

        LOG.info("T: " + cipherText);
        LOG.info("S: " + partKey);

        if (cipherText == null || cipherText.isEmpty() || partKey == null || partKey.isEmpty()) {
            LOG.warning("Null of Empty Keys");
            throw new InvalidKeyException();
        }


        Key keyGood, key;

        try {
            keyGood = KeyFactory.createKey(Tokens.kind, partKey);
            key = KeyFactory.stringToKey(cipherText);
        } catch (Exception e) {
            LOG.warning("Error creating keys");
            throw new InvalidKeyException();
        }

        if (!key.equals(keyGood)) {
            LOG.warning("Comparing keys: not equal");
            throw new InvalidKeyException();
        }

        try {
            Entity values = datastore.get(keyGood);
            return (String) values.getProperty(Tokens.value);
        } catch (EntityNotFoundException e) {
            LOG.warning("Token not Found");
            throw new InvalidKeyException();
        }
    }

    private static byte[] hexToBytes(String s) {
        int len = s.length();
        byte[] data = new byte[len / 2];
        for (int i = 0; i < len; i += 2) {
            data[i / 2] = (byte) ((Character.digit(s.charAt(i), 16) << 4)
                    + Character.digit(s.charAt(i + 1), 16));
        }
        return data;
    }

    private final static char[] hexArray = "0123456789ABCDEF".toCharArray();

    public static String bytesToHex(byte[] bytes) {
        char[] hexChars = new char[bytes.length * 2];
        for (int j = 0; j < bytes.length; j++) {
            int v = bytes[j] & 0xFF;
            hexChars[j * 2] = hexArray[v >>> 4];
            hexChars[j * 2 + 1] = hexArray[v & 0x0F];
        }
        return new String(hexChars);
    }

}
