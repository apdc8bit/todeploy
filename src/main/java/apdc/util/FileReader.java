package apdc.util;

import com.google.gson.stream.JsonReader;

import java.io.FileNotFoundException;

import static apdc.resources.api.Base.LOG;

public class FileReader {
    public static JsonReader read(String fileName) {
        LOG.info("Loading Permissions Table");

        String file = new java.io.File("").getAbsolutePath().concat(String.format("/secret/%s.json", fileName));

        try {
            return new JsonReader(new java.io.FileReader(file));
        } catch (FileNotFoundException e) {
            LOG.severe("No File Found!");
            return null;
        }
    }
}
