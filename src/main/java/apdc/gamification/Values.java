package apdc.gamification;

public class Values {
    private final long points;
    private final long level;

    public Values(long points, long level) {
        this.points = points;
        this.level = level;
    }

    public long getPoints() {
        return points;
    }

    public long getLevel() {
        return level;
    }
}
