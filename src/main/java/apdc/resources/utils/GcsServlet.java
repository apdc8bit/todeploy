package apdc.resources.utils;

import apdc.input.verification.utils.Reply;
import apdc.input.verification.utils.RequestDataVerification;
import apdc.permissions.Actions;
import apdc.tables.special.Images;
import com.google.appengine.api.blobstore.BlobKey;
import com.google.appengine.api.blobstore.BlobstoreService;
import com.google.appengine.api.blobstore.BlobstoreServiceFactory;
import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.images.Image;
import com.google.appengine.api.images.ImagesService;
import com.google.appengine.api.images.ImagesServiceFactory;
import com.google.appengine.api.images.Transform;
import com.google.appengine.tools.cloudstorage.*;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.ByteBuffer;
import java.nio.channels.Channels;
import java.util.Date;
import java.util.logging.Logger;

@SuppressWarnings("serial")
public class GcsServlet extends HttpServlet {
    private static final boolean SERVE_USING_BLOBSTORE_API = true;
    private static final Logger LOG = Logger.getLogger(GcsServlet.class.getName());

    private final static String BUCKET = "/gs/gaea_resources/";

    private final static String OCCURRENCES = BUCKET + "occurences_images/";
    private final static String PROFILE = BUCKET + "profile_images/";

    private final static String IMAGES = "images/";
    private final static String MEDIUMS = "mediums/";
    private final static String AVATARS = "avatars/";

    private final static String OCCURRENCES_IMAGES = OCCURRENCES + IMAGES;
    private final static String OCCURRENCES_MEDIUMS = OCCURRENCES + MEDIUMS;
    private final static String OCCURRENCES_AVATARS = OCCURRENCES + AVATARS;

    private final static String PROFILE_IMAGES = PROFILE + IMAGES;
    private final static String PROFILE_MEDIUMS = PROFILE + MEDIUMS;
    private final static String PROFILE_AVATARS = PROFILE + AVATARS;

    /**
     * Used below to determine the size of chucks to read in. Should be > 1kb and < 10MB
     */
    private static final int BUFFER_SIZE = 2 * 1024 * 1024;

    /**
     * This is where backoff parameters are configured. Here it is aggressively retrying with
     * backoff, up to 10 times but taking no more that 15 seconds total to do so.
     */
    private final GcsService gcsService = GcsServiceFactory.createGcsService(
            new RetryParams.Builder()
                    .initialRetryDelayMillis(10)
                    .retryMaxAttempts(10)
                    .totalRetryPeriodMillis(15000)
                    .build());

    @Override
    public void doOptions(HttpServletRequest req, HttpServletResponse resp) {
        setHeaders(req, resp);
    }


    @Override
    public void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        GcsFilename fileName = getFileName(req);

        if (fileName.getObjectName().contains("undefined")) {
            LOG.info("Request malformed, image name is 'undefined'");
            return;
        }

        GcsFileMetadata metadata = gcsService.getMetadata(fileName);

        if (metadata == null) {
            LOG.warning("File not found");
            return;
        }

        String contentType = metadata.getOptions().getContentEncoding();
        if (SERVE_USING_BLOBSTORE_API) {
            BlobstoreService blobstoreService = BlobstoreServiceFactory.getBlobstoreService();
            BlobKey blobKey = blobstoreService.createGsBlobKey(
                    "/gs/" + fileName.getBucketName() + "/" + fileName.getObjectName());

            resp.setHeader("content-type", contentType);
            blobstoreService.serve(blobKey, resp);

        } else {
            GcsInputChannel readChannel = gcsService.openPrefetchingReadChannel(fileName, 0, BUFFER_SIZE);
            resp.setHeader("content-type", contentType);
            copy(Channels.newInputStream(readChannel), resp.getOutputStream());
        }

        setHeaders(req, resp);
    }


    @Override
    public void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        Reply reply = RequestDataVerification.validateTokenPermission(req.getHeader("Authorization"), req.getHeader("SessionID"), "picture", Actions.add_image);
        if (!reply.ok())
            return;

        String type = req.getContentType();
        if (!type.split("/")[0].equalsIgnoreCase("image")) {
            LOG.warning("Invalid Image Type: " + type);
            return;
        }

        String imageIDD = getImageID(req);
        if (imageIDD == null) {
            LOG.warning("Name not found");
            return;
        }

        GcsFileOptions instance = new GcsFileOptions.Builder().mimeType(req.getContentType()).build();
        GcsFilename fileName = getFileName(req);
        GcsOutputChannel outputChannel;
        outputChannel = gcsService.createOrReplace(fileName, instance);

        byte[] imageBytes = copy(req.getInputStream(), Channels.newOutputStream(outputChannel));
        resizeImage(imageBytes, getFileNameThumbnail(req), req.getContentType(), 100);
        resizeImage(imageBytes, getFileNameMedium(req), req.getContentType(), 300);

        DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
        Entity imageID = new Entity(Images.kind, imageIDD);
        imageID.setProperty(Images.addedTime, new Date());
        datastore.put(imageID);

        setHeaders(req, resp);
    }

    private String getImageID(HttpServletRequest req) {
        try {
            String[] splits = req.getRequestURI().split("/");
            return splits[5];
        } catch (NullPointerException e) {
            return null;
        }
    }

    private void resizeImage(byte[] imageBytes, GcsFilename filename, String filetype, int size) {
        // Get an instance of the imagesService we can use to transform images.
        ImagesService imagesService = ImagesServiceFactory.getImagesService();

        // Make an image directly from a byte array, and transform it.
        Image image = ImagesServiceFactory.makeImage(imageBytes);
        Transform resize = ImagesServiceFactory.makeResize(size, size);
        Image resizedImage = imagesService.applyTransform(resize, image);

        // Write the transformed image back to a Cloud Storage object.
        try {
            gcsService.createOrReplace(filename, new GcsFileOptions
                    .Builder()
                    .mimeType(filetype)
                    .build(), ByteBuffer.wrap(resizedImage.getImageData()));

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private GcsFilename getFileName(HttpServletRequest req) {
        String[] splits = req.getRequestURI().split("/", 4);
        if (!splits[0].equals("") || !splits[1].equals("gcs")) {
            throw new IllegalArgumentException("The URL is not formed as expected. " +
                    "Expecting /gcs/<bucket>/<object>");
        }
        return new GcsFilename(splits[2], splits[3]);
    }

    private GcsFilename getFileNameThumbnail(HttpServletRequest req) {
        String[] splits = req.getRequestURI().split("/", 4);
        if (!splits[0].equals("") || !splits[1].equals("gcs")) {
            throw new IllegalArgumentException("The URL is not formed as expected. " +
                    "Expecting /gcs/<bucket>/<object>");
        }
        String file_location = splits[3].replaceAll("/images", "/avatars");
        return new GcsFilename(splits[2], file_location);
    }

    private GcsFilename getFileNameMedium(HttpServletRequest req) {
        String[] splits = req.getRequestURI().split("/", 4);
        if (!splits[0].equals("") || !splits[1].equals("gcs")) {
            throw new IllegalArgumentException("The URL is not formed as expected. " +
                    "Expecting /gcs/<bucket>/<object>");
        }
        String file_location = splits[3].replaceAll("/images", "/mediums");
        return new GcsFilename(splits[2], file_location);
    }

    /**
     * Transfer the info from the inputStream to the outputStream. Then close both streams.
     */
    private byte[] copy(InputStream input, OutputStream output) throws IOException {
        ByteArrayOutputStream out_buffer = new ByteArrayOutputStream();

        try {
            byte[] buffer = new byte[BUFFER_SIZE];
            int bytesRead = input.read(buffer);
            while (bytesRead != -1) {
                output.write(buffer, 0, bytesRead);

                out_buffer.write(buffer, 0, bytesRead);

                bytesRead = input.read(buffer);
            }
        } finally {
            input.close();
            output.close();
        }

        out_buffer.flush();
        return out_buffer.toByteArray();
    }

    private void setHeaders(HttpServletRequest req, HttpServletResponse resp) {
        resp.setHeader("Access-Control-Allow-Origin", req.getHeader("Origin"));
        resp.setHeader("Allow", "GET, HEAD, POST, TRACE, OPTIONS");
        resp.setHeader("Access-Control-Allow-Credentials", "true");
        resp.setHeader("Access-Control-Allow-Headers", "Content-Actions, X-Requested-With, Authorization, Content-Type, SessionID");
    }

    public enum FILE {
        occurrence_image,
        user_image
    }

    public static void deleteFile(String filename, FILE file) {
        BlobstoreService blobstoreService = BlobstoreServiceFactory.getBlobstoreService();
        BlobKey blobKeyA, blobKeyI, blobKeyM;

        if (filename == null)
            return;

        switch (file) {
            case user_image:
                blobKeyA = blobstoreService.createGsBlobKey(PROFILE_AVATARS + filename);
                blobKeyI = blobstoreService.createGsBlobKey(PROFILE_IMAGES + filename);
                blobKeyM = blobstoreService.createGsBlobKey(PROFILE_MEDIUMS + filename);
                blobstoreService.delete(blobKeyA, blobKeyI, blobKeyM);
                break;
            case occurrence_image:
                blobKeyA = blobstoreService.createGsBlobKey(OCCURRENCES_AVATARS + filename);
                blobKeyI = blobstoreService.createGsBlobKey(OCCURRENCES_IMAGES + filename);
                blobKeyM = blobstoreService.createGsBlobKey(OCCURRENCES_MEDIUMS + filename);
                blobstoreService.delete(blobKeyA, blobKeyI, blobKeyM);
                break;
        }
    }
}