package apdc.resources.entities;

import apdc.input.data.entity.*;
import apdc.input.data.occurrences.AssociateWorkerOccurrenceData;
import apdc.input.verification.utils.Reply;
import apdc.input.verification.utils.RequestDataVerification;
import apdc.input.verification.values.*;
import apdc.output.info.WorkersInfo;
import apdc.output.messages.OutputStatus;
import apdc.permissions.Actions;
import apdc.permissions.UserPermissions;
import apdc.tables.occurrence.Occurrences;
import apdc.tables.special.NewEntityRequests;
import apdc.tables.user.User;
import apdc.tables.worker.WorkerByEntity;
import apdc.tables.worker.WorkerJobs;
import apdc.util.email.EmailResource;
import com.google.appengine.api.datastore.*;
import com.google.gson.Gson;
import org.apache.commons.codec.digest.DigestUtils;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.*;
import java.util.logging.Logger;

@Path("/{device}/entity")
@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
public class EntityResource {
    private static final Logger LOG = Logger.getLogger(EntityResource.class.getName());
    private static final DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
    private final Gson g = new Gson();

    public EntityResource() {
    }

    @POST
    @Path("/register")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    public Response requestRoleChange(RegisterWorkerData data, @PathParam("device") String device, @HeaderParam("Authorization") String tokenID, @HeaderParam("SessionID") String sessionID) {
        Reply reply = RequestDataVerification.validateData(data);
        if (!reply.ok())
            return Response.status(reply.getStatus()).entity(reply.getResponse()).build();

        LOG.info("Attempting to register entity: " + data.username);

        TransactionOptions options = TransactionOptions.Builder.withXG(true);
        Transaction txn = datastore.beginTransaction(options);

        //Check Email and Username
        Reply replyCheck = UserValues.usernameAvailable(data.username);
        if (!replyCheck.ok())
            return Response.status(replyCheck.getStatus()).entity(replyCheck.getResponse()).build();

        replyCheck = UserValues.emailAvailable(data.email);
        if (!replyCheck.ok())
            return Response.status(replyCheck.getStatus()).entity(replyCheck.getResponse()).build();

        try {
            Entity user = UserValues.addUserEntity(data);
            datastore.put(txn, user);

            Key entity = user.getKey();
            Entity request = new Entity(NewEntityRequests.kind, entity.getName());
            request.setProperty(NewEntityRequests.role, UserPermissions.ROLE.u_entity.name());

            datastore.put(txn, request);
            LOG.info("Request Confirmed");

            EmailResource.sendEntityRegistration(user.getKey().getName(), data.email);

            txn.commit();
            return Response.ok(g.toJson(OutputStatus.Messages.USER_REG)).build();
        } finally {
            if (txn.isActive()) {
                LOG.severe("SERVER ERROR");
                txn.rollback();
            }
        }
    }

    @POST
    @Path("/create_worker")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    public Response createWorker(RegisterWorkerData data, @PathParam("device") String device, @HeaderParam("Authorization") String tokenID, @HeaderParam("SessionID") String sessionID) {
        Reply reply = RequestDataVerification.validateDataTokenPermission(data, tokenID, sessionID, device, Actions.create_worker);
        if (!reply.ok())
            return Response.status(reply.getStatus()).entity(reply.getResponse()).build();

        LOG.info("Editing occurrence status");

        TransactionOptions options = TransactionOptions.Builder.withXG(true);
        Transaction txn = datastore.beginTransaction(options);

        //Check Email and Username
        Reply replyCheck = UserValues.usernameAvailable(data.username);
        if (!replyCheck.ok())
            return Response.status(replyCheck.getStatus()).entity(replyCheck.getResponse()).build();

        replyCheck = UserValues.emailAvailable(data.email);
        if (!replyCheck.ok())
            return Response.status(replyCheck.getStatus()).entity(replyCheck.getResponse()).build();

        try {
            //Creates a new user entity and sets valid to true
            Entity worker = UserValues.addUserWorker(data);

            Entity workerAss = new Entity(WorkerByEntity.kind, reply.getUser().getKey());
            workerAss.setProperty(WorkerByEntity.worker_key, worker.getKey());
            workerAss.setProperty(WorkerByEntity.added_time, new Date());

            //datastore.put(txn, worker);
            datastore.put(txn, Arrays.asList(workerAss, worker));

            LOG.info("Status updated");

            txn.commit();
            return Response.ok(g.toJson(OutputStatus.Messages.USER_REG)).build();
        } finally {
            if (txn != null && txn.isActive()) {
                txn.rollback();
                LOG.severe("SERVER ERROR");
            }
        }
    }

    @PUT
    @Path("/edit_worker")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    public Response editWorker(EditWorkerInfoData data, @PathParam("device") String device, @HeaderParam("Authorization") String tokenID, @HeaderParam("SessionID") String sessionID) {
        Reply reply = RequestDataVerification.validateDataTokenPermission(data, tokenID, sessionID, device, Actions.edit_self_worker);
        if (!reply.ok())
            return Response.status(reply.getStatus()).entity(reply.getResponse()).build();

        Transaction txn = datastore.beginTransaction(TransactionOptions.Builder.withXG(true));
        try {
            Reply reply2 = WorkerValues.isWorkerFromEntity(data.username, reply.getUser().getKey());
            if (!reply2.ok())
                return Response.status(reply2.getStatus()).entity(reply2.getResponse()).build();

            //Edit Values
            Entity userEntity = reply2.getUser();
            userEntity = UserValues.editUser(userEntity, data);
            datastore.put(txn, userEntity);

            txn.commit();
            return Response.ok().entity(g.toJson(OutputStatus.Messages.WORKER_EDITED)).build();
        } finally {
            if (txn != null && txn.isActive()) {
                txn.rollback();
                LOG.severe("SERVER ERROR");
            }
        }

    }

    @PUT
    @Path("/delete_info")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    public Response deleteWorkerInfo(DeleteWorkerInfoData data, @PathParam("device") String device, @HeaderParam("Authorization") String tokenID, @HeaderParam("SessionID") String sessionID) {
        Reply reply = RequestDataVerification.validateDataTokenPermission(data, tokenID, sessionID, device, Actions.edit_self_worker);
        if (!reply.ok())
            return Response.status(reply.getStatus()).entity(reply.getResponse()).build();

        LOG.info("Deleting user's information.");

        Transaction txn = datastore.beginTransaction(TransactionOptions.Builder.withXG(true));

        try {
            Reply reply2 = WorkerValues.isWorkerFromEntity(data.username, reply.getUser().getKey());
            if (!reply2.ok())
                return Response.status(reply2.getStatus()).entity(reply2.getResponse()).build();

            //Delete Values
            Entity userEntity = reply2.getUser();
            UserValues.deleteInfoUser(userEntity, data);
            datastore.put(txn, userEntity);

            LOG.info("Info deleted from user: " + userEntity.getKey().getName());

            txn.commit();
            return Response.ok(g.toJson(OutputStatus.Messages.INFO_DELETED)).build();
        } finally {
            if (txn != null && txn.isActive()) {
                LOG.severe("SERVER ERROR");
                txn.rollback();
            }
        }
    }

    @PUT
    @Path("/delete_worker")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    public Response deleteWorker(WorkerIDData data, @PathParam("device") String device, @HeaderParam("Authorization") String tokenID, @HeaderParam("SessionID") String sessionID) {
        Reply reply = RequestDataVerification.validateDataTokenPermission(data, tokenID, sessionID, device, Actions.delete_self_worker);
        if (!reply.ok())
            return Response.status(reply.getStatus()).entity(reply.getResponse()).build();

        Reply reply2 = WorkerValues.isWorkerFromEntity(data.username, reply.getUser().getKey());
        if (!reply2.ok())
            return Response.status(reply2.getStatus()).entity(reply2.getResponse()).build();

        Transaction txn = datastore.beginTransaction(TransactionOptions.Builder.withXG(true));

        try {
            //Remove User
            Key workerKey = reply2.getUser().getKey();
            datastore.delete(txn, workerKey);

            Query ctrQuery = new Query(WorkerByEntity.kind)
                    .setFilter(new Query.FilterPredicate(WorkerByEntity.worker_key, Query.FilterOperator.EQUAL, workerKey));
            List<Entity> workerAssEntity = datastore.prepare(ctrQuery).asList(FetchOptions.Builder.withDefaults());

            if (workerAssEntity.size() > 0)
                datastore.delete(txn, workerAssEntity.get(0).getKey());
            else
                LOG.warning("Worker Ass not found");

            Query ctrQuery2 = new Query(WorkerJobs.kind).setAncestor(workerKey)
                    .setFilter(new Query.FilterPredicate(WorkerJobs.state, Query.FilterOperator.NOT_EQUAL, WorkerValues.States.COMPLETED.name()));
            List<Entity> workerJobs = datastore.prepare(ctrQuery2).asList(FetchOptions.Builder.withDefaults());

            for (Entity job : workerJobs)
                datastore.delete(txn, job.getKey());

            txn.commit();
            return Response.ok().entity(g.toJson(OutputStatus.Messages.USER_DELETED)).build();
        } finally {
            if (txn != null && txn.isActive()) {
                txn.rollback();
                LOG.severe("SERVER ERROR");
            }
        }
    }

    @POST
    @Path("/associate_occurrence")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    public Response associateOccurrence(AssociateWorkerOccurrenceData data, @PathParam("device") String device, @HeaderParam("Authorization") String tokenID, @HeaderParam("SessionID") String sessionID) {
        Reply reply = RequestDataVerification.validateDataTokenPermission(data, tokenID, sessionID, device, Actions.associate_self_occurrence_worker);
        if (!reply.ok())
            return Response.status(reply.getStatus()).entity(reply.getResponse()).build();

        TransactionOptions options = TransactionOptions.Builder.withXG(true);
        Transaction txn = datastore.beginTransaction(options);

        try {
            Reply reply2 = WorkerValues.isWorkerFromEntity(data.username_worker, reply.getUser().getKey());
            if (!reply2.ok()) {
                txn.rollback();
                return Response.status(reply2.getStatus()).entity(reply2.getResponse()).build();
            }

            Key workerKey = reply2.getUser().getKey();

            Key userKey = KeyFactory.createKey(User.kind, data.username_occurrence);
            Key occurrenceKey = KeyFactory.createKey(userKey, Occurrences.kind, data.id_occurrence);

            Entity occ = datastore.get(txn, occurrenceKey);
            String state = (String) occ.getProperty(Occurrences.status);

            //Entity dico
            String dicoEntity = (String) reply.getUser().getProperty(User.dico);
            String dico = (String) occ.getProperty(Occurrences.dico);

            if (!dico.equals(dicoEntity)) {
                LOG.warning("Dicos do not match, Entity: " + dicoEntity + ", Occ: " + dico);
                txn.rollback();
                return Response.status(Response.Status.BAD_REQUEST).entity(g.toJson("occurrence_is_in_a_different_county")).build();
            }

            Boolean volunteer = (Boolean) reply.getUser().getProperty(User.volunteer);
            if (volunteer != null && volunteer) {
                LOG.warning("Entity is volunteer");
                return Response.status(Response.Status.BAD_REQUEST).entity(g.toJson("entity_is_volunteer")).build();
            }

            //Check if status is on_hold
            if (!state.equals(OccurrenceValues.STATUS.on_hold.name())) {
                LOG.warning("Occurrence status is not on_hold");
                txn.rollback();
                return Response.status(Response.Status.BAD_REQUEST).entity(g.toJson(OutputStatus.Messages.OCCURRENCE_IS_NOT_ON_HOLD)).build();
            }

            Key occass = KeyFactory.createKey(workerKey, WorkerJobs.kind, occurrenceKey.getId());

            try {
                //Check if occurrence is already associated
                datastore.get(txn, occass);
                LOG.warning("Occurrence already associated");
                txn.rollback();
                return Response.status(Response.Status.BAD_REQUEST).entity(OutputStatus.Messages.OCCURRENCE_ALREADY_ASSOCIATED).build();
            } catch (EntityNotFoundException e) {
                //Associate occurrence to worker
                Date date = new Date();
                Entity ass = new Entity(WorkerJobs.kind, occurrenceKey.getId(), workerKey);
                ass.setProperty(WorkerJobs.occurrence_key, occurrenceKey);
                ass.setProperty(WorkerJobs.added_time, date);
                ass.setProperty(WorkerJobs.state, WorkerValues.States.TODO.name());
                ass.setProperty(WorkerJobs.entity_key, reply.getUser().getKey());

                occ.setProperty(Occurrences.status, OccurrenceValues.STATUS.in_progress.name());
                occ.setProperty(Occurrences.initiation_time, date);

                datastore.put(txn, Arrays.asList(occ, ass));

                LOG.info("occurrence Associated");
                EntityValues.startedOccurrence(reply.getUser().getKey(), ass, occ);

                NotificationValues.occurrenceStateNotification(occ);
                NotificationValues.workerNewJob(occ, workerKey.getName());

                txn.commit();
                return Response.ok().entity(g.toJson(OutputStatus.Messages.OCCURRENCE_ASSOCIATED)).build();
            }

        } catch (EntityNotFoundException e) {
            LOG.warning("Occurrence doesn't exist.");
            txn.rollback();
            return Response.status(Response.Status.BAD_REQUEST).entity(g.toJson(OutputStatus.Messages.OCCURRENCE_DOES_NOT_EXIST)).build();
        } finally {
            if (txn != null && txn.isActive()) {
                txn.rollback();
                LOG.severe("SERVER ERROR");
            }
        }

    }

    @PUT
    @Path("/remove_association")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    public Response removeAssociation(AssociateWorkerOccurrenceData data, @PathParam("device") String device, @HeaderParam("Authorization") String tokenID, @HeaderParam("SessionID") String sessionID) {
        Reply reply = RequestDataVerification.validateDataTokenPermission(data, tokenID, sessionID, device, Actions.disassociate_self_occurrence_worker);
        if (!reply.ok())
            return Response.status(reply.getStatus()).entity(reply.getResponse()).build();

        TransactionOptions options = TransactionOptions.Builder.withXG(true);
        Transaction txn = datastore.beginTransaction(options);

        try {
            Reply reply2 = WorkerValues.isWorkerFromEntity(data.username_worker, reply.getUser().getKey());
            if (!reply2.ok()) {
                txn.rollback();
                return Response.status(reply2.getStatus()).entity(reply2.getResponse()).build();
            }

            Boolean volunteer = (Boolean) reply.getUser().getProperty(User.volunteer);
            if (volunteer != null && volunteer) {
                LOG.warning("Entity is volunteer");
                return Response.status(Response.Status.BAD_REQUEST).entity(g.toJson("entity_is_volunteer")).build();
            }

            Entity userEntity = reply2.getUser();
            Key workerKey = userEntity.getKey();

            Key jobKey = KeyFactory.createKey(workerKey, WorkerJobs.kind, data.id_occurrence);

            Key userKey = KeyFactory.createKey(User.kind, data.username_occurrence);
            Key occurrenceKey = KeyFactory.createKey(userKey, Occurrences.kind, data.id_occurrence);

            datastore.get(txn, jobKey);

            Entity occ = datastore.get(occurrenceKey);
            occ.setProperty(Occurrences.status, OccurrenceValues.STATUS.on_hold.name());
            occ.removeProperty(Occurrences.initiation_time);

            datastore.delete(txn, jobKey);
            datastore.put(txn, occ);

            txn.commit();
            return Response.ok().entity(g.toJson(OutputStatus.Messages.OCCURRENCE_ASSOCIATION_REMOVED)).build();
        } catch (EntityNotFoundException e) {
            LOG.warning("Job does not exist or Occurrence not found");
            txn.rollback();
            return Response.status(Response.Status.BAD_REQUEST).entity(g.toJson(OutputStatus.Messages.JOB_DOES_NOT_EXIST)).build();
        } finally {
            if (txn != null && txn.isActive()) {
                txn.rollback();
                LOG.severe("SERVER ERROR");
            }
        }
    }

    @PUT
    @Path("/confirm_job")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    public Response confirmJob(ConfirmJobData data, @PathParam("device") String device, @HeaderParam("Authorization") String tokenID, @HeaderParam("SessionID") String sessionID) {
        Reply reply = RequestDataVerification.validateDataTokenPermission(data, tokenID, sessionID, device, Actions.confirm_self_job_completion);
        if (!reply.ok())
            return Response.status(reply.getStatus()).entity(reply.getResponse()).build();

        TransactionOptions options = TransactionOptions.Builder.withXG(true);
        Transaction txn = datastore.beginTransaction(options);

        Boolean volunteer = (Boolean) reply.getUser().getProperty(User.volunteer);
        if (volunteer != null && volunteer) {
            LOG.warning("Entity is volunteer");
            return Response.status(Response.Status.BAD_REQUEST).entity(g.toJson("entity_is_volunteer")).build();
        }

        try {
            Reply reply2 = WorkerValues.isWorkerFromEntity(data.username_worker, reply.getUser().getKey());
            if (!reply2.ok()) {
                txn.rollback();
                return Response.status(reply2.getStatus()).entity(reply2.getResponse()).build();
            }

            Entity workerEntity = reply2.getUser();
            String email = (String) workerEntity.getProperty(User.email);
            Key workerKey = workerEntity.getKey();

            Key jobKey = KeyFactory.createKey(workerKey, WorkerJobs.kind, data.id_occurrence);

            Key userKey = KeyFactory.createKey(User.kind, data.username_occurrence);
            Key occurrenceKey = KeyFactory.createKey(userKey, Occurrences.kind, data.id_occurrence);

            Entity ass = datastore.get(txn, jobKey);
            Entity occ = datastore.get(occurrenceKey);

            if (data.confirm) {
                ass.setProperty(WorkerJobs.confirmed_time, new Date());
                ass.setProperty(WorkerJobs.state, WorkerValues.States.COMPLETED.name());
                String id = (String) ass.getProperty(WorkerJobs.imageID);

                occ.setProperty(Occurrences.status, OccurrenceValues.STATUS.completed.name());
                occ.setProperty(Occurrences.completion_time, new Date());
                occ.setProperty(Occurrences.imageIDCompleted, id);

                datastore.put(txn, Arrays.asList(occ, ass));

                EntityValues.finishedOccurrence(reply.getUser().getKey(), ass, occ);

                EmailResource.sendJobConfirmed(workerKey.getName(), email, true, data.id_occurrence);
                NotificationValues.occurrenceStateNotification(occ);
                NotificationValues.workerConfirmedJob(occ, workerKey.getName());

                txn.commit();
                return Response.ok().entity(g.toJson(OutputStatus.Messages.JOB_CONFIRMED)).build();
            } else {
                ass.removeProperty(WorkerJobs.finish_time);
                ass.setProperty(WorkerJobs.state, WorkerValues.States.TODO.name());
                EmailResource.sendJobConfirmed(workerKey.getName(), email, false, data.id_occurrence);
                datastore.put(txn, ass);
                txn.commit();
                return Response.ok().entity(g.toJson(OutputStatus.Messages.JOB_NOT_CONFIRMED)).build();
            }


        } catch (EntityNotFoundException e) {
            LOG.warning("Job does not exist or Occurrence not found");
            txn.rollback();
            return Response.status(Response.Status.BAD_REQUEST).entity(g.toJson(OutputStatus.Messages.JOB_DOES_NOT_EXIST)).build();
        } finally {
            if (txn != null && txn.isActive()) {
                txn.rollback();
                LOG.severe("SERVER ERROR");
            }
        }
    }

    @GET
    @Path("/list_workers")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    public Response listWorkers(@PathParam("device") String device, @HeaderParam("Authorization") String tokenID, @HeaderParam("SessionID") String sessionID) {
        Reply reply = RequestDataVerification.validateTokenPermission(tokenID, sessionID, device, Actions.list_self_workers);
        if (!reply.ok())
            return Response.status(reply.getStatus()).entity(reply.getResponse()).build();

        Query ctrQuery = new Query(WorkerByEntity.kind).setAncestor(reply.getUser().getKey()).addSort(WorkerByEntity.added_time, Query.SortDirection.DESCENDING);
        List<Entity> results = datastore.prepare(ctrQuery).asList(FetchOptions.Builder.withDefaults());

        List<WorkersInfo> out = WorkerValues.listWorkers(results);

        return Response.ok().entity(g.toJson(out)).build();
    }

    @GET
    @Path("/list_jobs")
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    public Response listJobs(@PathParam("device") String device, @HeaderParam("Authorization") String tokenID, @HeaderParam("SessionID") String sessionID, @QueryParam("filter") String filter, @QueryParam("worker") String worker) {
        Reply reply = RequestDataVerification.validateTokenPermission(tokenID, sessionID, device, Actions.list_self_jobs);
        if (!reply.ok())
            return Response.status(reply.getStatus()).entity(reply.getResponse()).build();

        Boolean volunteer = (Boolean) reply.getUser().getProperty(User.volunteer);
        if (volunteer != null && volunteer) {
            LOG.warning("Entity is volunteer");
            return Response.status(Response.Status.BAD_REQUEST).entity(g.toJson("entity_is_volunteer")).build();
        }

        List<Entity> results;
        Query.Filter propertyFilter;

        WorkerValues.States state = WorkerValues.getState(filter);
        if (filter != null && !filter.isEmpty() && state != null)
            propertyFilter = new Query.FilterPredicate(WorkerJobs.state, Query.FilterOperator.EQUAL, state.name());
        else
            propertyFilter = new Query.FilterPredicate(WorkerJobs.state, Query.FilterOperator.NOT_EQUAL, "");

        Query.Filter propertyFilter2 = new Query.FilterPredicate(WorkerJobs.entity_key, Query.FilterOperator.EQUAL, reply.getUser().getKey());

        Query.CompositeFilter and_filter = Query.CompositeFilterOperator.and(propertyFilter, propertyFilter2);

        worker = worker == null ? "" : worker.toLowerCase();

        if (!worker.isEmpty()) {
            Reply reply2 = WorkerValues.isWorkerFromEntity(worker, reply.getUser().getKey());
            if (!reply2.ok())
                return Response.status(reply.getStatus()).entity(reply.getResponse()).build();

            Key workerKey = reply2.getUser().getKey();
            Query ctrQuery = new Query(WorkerJobs.kind)
                    .setAncestor(workerKey)
                    .addSort(WorkerJobs.state, Query.SortDirection.ASCENDING)
                    .addSort(WorkerJobs.added_time, Query.SortDirection.DESCENDING)
                    .setFilter(and_filter);
            results = datastore.prepare(ctrQuery).asList(FetchOptions.Builder.withDefaults());
        } else {
            Query ctrQuery = new Query(WorkerJobs.kind)
                    .addSort(WorkerJobs.state, Query.SortDirection.ASCENDING)
                    .addSort(WorkerJobs.added_time, Query.SortDirection.DESCENDING)
                    .setFilter(and_filter);
            results = datastore.prepare(ctrQuery).asList(FetchOptions.Builder.withDefaults());
        }

        return Response.ok().entity(g.toJson(WorkerValues.listJobs(results))).build();
    }

    @GET
    @Path("/listOccurrencesInArea")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    public Response listOccurrencesInArea(@PathParam("device") String device, @HeaderParam("Authorization") String tokenID, @HeaderParam("SessionID") String sessionID) {
        Reply reply = RequestDataVerification.validateTokenPermission(tokenID, sessionID, device, Actions.list_occurrences_in_area);
        if (!reply.ok())
            return Response.status(reply.getStatus()).entity(reply.getResponse()).build();

        Boolean volunteer = (Boolean) reply.getUser().getProperty(User.volunteer);
        if (volunteer != null && volunteer) {
            LOG.warning("Entity is volunteer");
            return Response.status(Response.Status.BAD_REQUEST).entity(g.toJson("entity_is_volunteer")).build();
        }

        Entity entity = reply.getUser();

        String dico = (String) entity.getProperty(User.dico);

        if (dico == null) {
            LOG.warning("Entity does not have an area defined");
            return Response.status(Response.Status.BAD_REQUEST).build();
        }

        Query.Filter propertyFilter = new Query.FilterPredicate(Occurrences.dico, Query.FilterOperator.EQUAL, dico);

        Query ctrQuery = new Query(Occurrences.kind)
                .addSort(Occurrences.creation_time, Query.SortDirection.DESCENDING)
                .setFilter(propertyFilter);

        List<Entity> results = datastore.prepare(ctrQuery).asList(FetchOptions.Builder.withDefaults());

        LOG.info("Returning " + results.size() + " occurrences");

        return Response.ok().entity(g.toJson(OccurrenceValues.getListWithUserVote(results, null))).build();
    }

    @GET
    @Path("/privateEntityStats")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    public Response privateEntityStats(@PathParam("device") String device, @HeaderParam("Authorization") String tokenID, @HeaderParam("SessionID") String sessionID, @QueryParam("worker") String worker) {
        Reply reply = RequestDataVerification.validateTokenPermission(tokenID, sessionID, device, Actions.list_self_workers);
        if (!reply.ok())
            return Response.status(reply.getStatus()).entity(reply.getResponse()).build();

        Boolean volunteer = (Boolean) reply.getUser().getProperty(User.volunteer);
        if (volunteer != null && volunteer) {
            LOG.warning("Entity is volunteer");
            return Response.status(Response.Status.BAD_REQUEST).entity(g.toJson("entity_is_volunteer")).build();
        }

        if (worker != null) {
            Reply reply2 = WorkerValues.isWorkerFromEntity(worker, reply.getUser().getKey());
            if (!reply2.ok())
                return Response.status(reply2.getStatus()).entity(reply2.getResponse()).build();

            try {
                Key workerKey = KeyFactory.createKey(User.kind, worker);
                Entity workerE = datastore.get(workerKey);

                return Response.ok(EntityValues.getWorkerStats(workerE)).build();
            } catch (EntityNotFoundException e) {
                return Response.status(Response.Status.BAD_REQUEST).entity(g.toJson("worker not found")).build();
            }
        } else {
            return Response.ok(g.toJson(EntityValues.getPrivateStats(reply.getUser()))).build();
        }
    }

    @GET
    @Path("/publicEntityStats")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    public Response publicEntityStats(@PathParam("device") String device, @HeaderParam("Authorization") String tokenID, @HeaderParam("SessionID") String sessionID, @QueryParam("entity") String entity) {

        if (entity == null || entity.isEmpty()) {
            LOG.warning("Entity name is invalid: " + entity);
            return Response.status(Response.Status.BAD_REQUEST).entity("invalid_name").build();
        }

        Key entityKey = KeyFactory.createKey(User.kind, entity);

        try {
            Entity entityE = datastore.get(entityKey);
            String role = (String) entityE.getProperty(User.role);
            if (!role.equals(UserPermissions.ROLE.u_entity.name().toLowerCase())) {
                LOG.warning("Who's a bad boy?");
                return Response.status(Response.Status.FORBIDDEN).entity(g.toJson("user is not a entity!")).build();
            }

            return Response.ok().entity(g.toJson(EntityValues.getEntityStats(entityE))).build();
        } catch (EntityNotFoundException e) {
            return Response.status(Response.Status.BAD_REQUEST).entity(g.toJson("entity not found")).build();
        }
    }

    @GET
    @Path("/listAllEntities")
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    public Response listAllEntities(@PathParam("device") String device, @HeaderParam("Authorization") String tokenID, @HeaderParam("SessionID") String sessionID, @QueryParam("entity") String entity) {

        Query.Filter propertyFilter = new Query.FilterPredicate(User.role, Query.FilterOperator.EQUAL, UserPermissions.ROLE.u_entity.name());
        Query ctrQuery = new Query(User.kind).setFilter(propertyFilter);
        List<Entity> results = datastore.prepare(ctrQuery).asList(FetchOptions.Builder.withDefaults());

        return Response.ok(g.toJson(EntityValues.getEntitiesInfo(results))).build();
    }

    @SuppressWarnings("unchecked")
    @GET
    @Path("/fix")
    public Response fix() {
        Query ctrQuery = new Query(Occurrences.kind);
        List<Entity> results = datastore.prepare(ctrQuery).asList(FetchOptions.Builder.withDefaults());

        List<Entity> fixed = new LinkedList<>();
        long count = 0L;
        for (Entity e : results) {
            int id = Math.abs(DigestUtils.sha512Hex(UUID.randomUUID().toString()).hashCode());
            Entity occurrence = new Entity(Occurrences.kind, id, e.getParent());
            occurrence.setUnindexedProperty(Occurrences.title, e.getProperty(Occurrences.title));
            occurrence.setUnindexedProperty(Occurrences.description, e.getProperty(Occurrences.description));
            occurrence.setProperty(Occurrences.lat, e.getProperty(Occurrences.lat));
            occurrence.setProperty(Occurrences.lng, e.getProperty(Occurrences.lng));
            occurrence.setProperty(Occurrences.geoHash, e.getProperty(Occurrences.geoHash));
            List<Double> lats = (List<Double>) e.getProperty(Occurrences.lats);
            List<Double> lngs = (List<Double>) e.getProperty(Occurrences.lngs);
            if (lats != null && lngs != null) {
                occurrence.setProperty(Occurrences.lats, lats);
                occurrence.setProperty(Occurrences.lngs, lngs);
            }
            occurrence.setProperty(Occurrences.status, e.getProperty(Occurrences.status));
            occurrence.setProperty(Occurrences.type, e.getProperty(Occurrences.type));
            occurrence.setProperty(Occurrences.validated, e.getProperty(Occurrences.validated));
            String imageID = (String) e.getProperty(Occurrences.imageID);
            if (imageID != null) {
                occurrence.setUnindexedProperty(Occurrences.imageID, imageID);
            }
            occurrence.setProperty(Occurrences.creation_time, e.getProperty(Occurrences.creation_time));
            occurrence.setProperty(Occurrences.last_edit_time, e.getProperty(Occurrences.last_edit_time));
            occurrence.setProperty(Occurrences.archived, e.getProperty(Occurrences.archived));
            occurrence.setProperty(Occurrences.dico, e.getProperty(Occurrences.dico));
            occurrence.setProperty(Occurrences.county, e.getProperty(Occurrences.county));
            occurrence.setProperty(Occurrences.district, e.getProperty(Occurrences.district));
            occurrence.setProperty(Occurrences.checked, true);
            fixed.add(occurrence);
            count++;
        }

        datastore.put(fixed);

        return Response.ok(g.toJson(count)).build();
    }


}
