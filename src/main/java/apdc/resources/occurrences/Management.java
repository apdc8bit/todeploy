package apdc.resources.occurrences;

import apdc.dico.DicoData;
import apdc.gamification.PointSystem;
import apdc.input.data.occurrences.CreateOccurrenceData;
import apdc.input.data.occurrences.EditOccurrenceData;
import apdc.input.data.occurrences.OccurrenceIDData;
import apdc.input.verification.utils.Reply;
import apdc.input.verification.utils.RequestDataVerification;
import apdc.input.verification.values.OccurrenceValues;
import apdc.output.messages.OutputStatus;
import apdc.permissions.Actions;
import apdc.resources.api.OccurrenceResources;
import apdc.tables.occurrence.Occurrences;
import com.github.davidmoten.geo.GeoHash;
import com.google.appengine.api.datastore.*;
import com.google.appengine.api.taskqueue.QueueFactory;
import com.google.appengine.api.taskqueue.TaskOptions;
import com.google.gson.JsonObject;
import org.apache.commons.codec.digest.DigestUtils;

import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import java.util.Date;
import java.util.UUID;

import static apdc.resources.api.Base.*;

@Path("/{device}/occurrence")
@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
public class Management implements OccurrenceResources.ManagementI {

    public Management() {
    }

    @Override
    public Response checkNewOccurrence(CreateOccurrenceData data, String device, String tokenID, String sessionID) {
        Reply reply = RequestDataVerification.validateDataTokenPermission(data, tokenID, sessionID, device, Actions.create_new_occurrence);
        if (!reply.ok())
            return Response.status(reply.getStatus()).entity(reply.getResponse()).build();

        //Check if in water
        if (OccurrenceValues.isInWater(data.lat, data.lng))
            return Response.status(Status.BAD_REQUEST).entity(g.toJson(OutputStatus.Messages.IN_WATER)).build();

        OccurrenceValues.Result result = OccurrenceValues.checkOccurrence(GeoHash.encodeHash(data.lat, data.lng, 8));
        int value = result.r;

        if (value == 1) //close but no cigar
            return Response.accepted(g.toJson(OccurrenceValues.getOccurrenceInfo(result.e, reply.getUser().getKey(), null))).build();
        else if (value == 2) //too close for comfort
            return Response.status(Status.CONFLICT).entity(g.toJson(OccurrenceValues.getOccurrenceInfo(result.e, reply.getUser().getKey(), null))).build();
        else //I'll allow it
            return Response.ok().build();
    }


    @Override
    public Response addOccurrence(CreateOccurrenceData data, String device, String tokenID, String sessionID) {
        Reply reply = RequestDataVerification.validateDataTokenPermission(data, tokenID, sessionID, device, Actions.create_new_occurrence);
        if (!reply.ok())
            return Response.status(reply.getStatus()).entity(reply.getResponse()).build();

        LOG.info("Attempting to add new occurrence: " + data.title);

        Transaction txn = null;

        Key userKey = reply.getUser().getKey();


        try {
            //Calculate geohash
            String geoHash = GeoHash.encodeHash(data.lat, data.lng);
            LOG.info("GEO: " + geoHash);
            Date date = new Date();

            //Check if occurrence exists
            if (OccurrenceValues.checkOccurrence(geoHash).r == 2) {
                LOG.warning("Checking Occurrence");
                return Response.status(Status.CONFLICT).entity(g.toJson(OutputStatus.Messages.OCCURRENCE_EXISTS)).build();
            }

            txn = datastore.beginTransaction(TransactionOptions.Builder.withXG(true));

            int id = Math.abs(DigestUtils.sha512Hex(UUID.randomUUID().toString()).hashCode());
            Entity occurrence = new Entity(Occurrences.kind, id, userKey);

            //Entity occurrence = new Entity(Occurrences.kind, userKey);
            //occurrence.setProperty(Occurrences.address, geo.address);

            //Set Properties
            occurrence.setUnindexedProperty(Occurrences.title, data.title);
            occurrence.setUnindexedProperty(Occurrences.description, data.description);

            occurrence.setProperty(Occurrences.lat, data.lat);
            occurrence.setProperty(Occurrences.lng, data.lng);


            occurrence.setProperty(Occurrences.geoHash, geoHash);

            if (!data.lats.isEmpty() && !data.lngs.isEmpty()) {
                occurrence.setProperty(Occurrences.lats, data.lats);
                occurrence.setProperty(Occurrences.lngs, data.lngs);
            }

            occurrence.setProperty(Occurrences.status, OccurrenceValues.STATUS.on_hold.name());
            occurrence.setProperty(Occurrences.type, data.type);
            occurrence.setProperty(Occurrences.validated, false);

            if (data.imageID != null && !data.imageID.isEmpty()) {
                occurrence.setUnindexedProperty(Occurrences.imageID, data.imageID);
            }

            occurrence.setProperty(Occurrences.creation_time, date);
            occurrence.setProperty(Occurrences.last_edit_time, date);
            occurrence.setProperty(Occurrences.archived, false);

            DicoData dicoData = OccurrenceValues.getDico(data.lat, data.lng);
            if (dicoData != null) {
                occurrence.setProperty(Occurrences.dico, dicoData.dico);
                occurrence.setProperty(Occurrences.county, dicoData.county);
                occurrence.setProperty(Occurrences.district, dicoData.district);
            } else {
                LOG.warning("DICO is Null");
            }

            occurrence.setProperty(Occurrences.checked, false);

            datastore.put(txn, occurrence);

            txn.commit();

            LOG.info("Occurrence created: " + occurrence.getKey().getId());

            //Add points for creating occurrence
            boolean levelUP = PointSystem.addPoints(PointSystem.ACTIONS.create_occurrence, userKey.getName());

            QueueFactory.getDefaultQueue().add(
                    TaskOptions.Builder
                            .withUrl("/rest/cron/checkOccurrences/" + userKey.getName() + "/" + occurrence.getKey().getId()));

            //Creating return json
            JsonObject json = new JsonObject();
            json.add("lat", g.toJsonTree(data.lat));
            json.add("lng", g.toJsonTree(data.lng));
            json.add("id", g.toJsonTree(occurrence.getKey().getId()));
            json.add("username", g.toJsonTree(userKey.getName()));
            json.addProperty("levelUP", levelUP);

            return Response.ok(g.toJson(json)).build();
        } finally {
            if (txn != null && txn.isActive()) {
                txn.rollback();
                LOG.severe("SERVER ERROR");
            }
        }
    }

    @Override
    public Response editOccurrence(EditOccurrenceData data, String device, String tokenID, String sessionID) {
        boolean altered = false, coords_altered = false;
        Reply reply = RequestDataVerification.validateDataTokenPermission(data, tokenID, sessionID, device, Actions.edit_self_occurrence);
        if (!reply.ok())
            return Response.status(reply.getStatus()).entity(reply.getResponse()).build();

        LOG.info("Attempting to edit occurrence" + data.title);

        Transaction txn = datastore.beginTransaction(TransactionOptions.Builder.withXG(true));

        Key userKey = reply.getUser().getKey();

        try {

            //Get Occurrence Entity
            long id = data.id;
            Key occurrenceKey = KeyFactory.createKey(userKey, Occurrences.kind, id);
            Entity occurrence = datastore.get(occurrenceKey);

            //Check if the occurrence status is on_hold
            String status = (String) occurrence.getProperty(Occurrences.status);
            if (!OccurrenceValues.STATUS.valueOf(status).equals(OccurrenceValues.STATUS.on_hold)) {
                LOG.warning("Occurrence status has change, cannot be removed");
                txn.rollback();
                return Response.status(Status.FORBIDDEN).entity(g.toJson(OutputStatus.Messages.OCCURRENCE_STATUS_HAS_CHANGED)).build();
            }

            if (!data.title.isEmpty()) {
                occurrence.setUnindexedProperty(Occurrences.title, data.title);
                altered = true;
            }
            if (!data.description.isEmpty()) {
                occurrence.setUnindexedProperty(Occurrences.description, data.description);
                altered = true;
            }
            if (!data.type.isEmpty()) {
                occurrence.setProperty(Occurrences.type, data.type);
                altered = true;
            }
            if (!data.imageID.isEmpty()) {
                //GcsServlet.deleteFile((String) occurrence.getProperty(Occurrences.imageID), GcsServlet.FILE.occurrence_image);
                occurrence.setUnindexedProperty(Occurrences.imageID, data.imageID);
                altered = true;
            }
            if (!data.lats.isEmpty()) {
                occurrence.setProperty(Occurrences.lats, data.lats);
                altered = true;
                coords_altered = true;
            }
            if (!data.lngs.isEmpty()) {
                occurrence.setProperty(Occurrences.lngs, data.lngs);
                altered = true;
                coords_altered = true;
            }
            if (altered)
                occurrence.setProperty(Occurrences.last_edit_time, new Date()); //guarda a data da ultima edição.

            datastore.put(txn, occurrence);
            txn.commit();

            JsonObject json = new JsonObject();
            json.add("lat", g.toJsonTree(data.lat));
            json.add("lng", g.toJsonTree(data.lng));

            return Response.ok(g.toJson(json)).build();
        } catch (EntityNotFoundException e) {
            LOG.warning("Occurrence doesn't exist.");
            txn.rollback();
            return Response.status(Status.BAD_REQUEST).entity(g.toJson(OutputStatus.Messages.OCCURRENCE_DOES_NOT_EXIST)).build();
        } finally {
            if (txn != null && txn.isActive()) {
                txn.rollback();
                LOG.severe("SERVER ERROR");
            }
        }

    }

    @Override
    public Response deleteOccurrence(OccurrenceIDData data, String device, String tokenID, String sessionID) {
        Reply reply = RequestDataVerification.validateDataTokenPermission(data, tokenID, sessionID, device, Actions.delete_self_occurrence);
        if (!reply.ok())
            return Response.status(reply.getStatus()).entity(reply.getResponse()).build();

        LOG.info("Attempting to delete occurrence " + data.id);

        Transaction txn = datastore.beginTransaction(TransactionOptions.Builder.withXG(true));

        try {
            //Get Occurrence
            long id = data.id;
            Key ownerKey = reply.getUser().getKey();
            Key occurrenceKey = KeyFactory.createKey(ownerKey, Occurrences.kind, id);
            Entity occ = datastore.get(occurrenceKey);

            //Check if the occurrence status is on_hold
            String status = (String) occ.getProperty(Occurrences.status);
            if (!OccurrenceValues.STATUS.valueOf(status).equals(OccurrenceValues.STATUS.on_hold)) {
                LOG.warning("Occurrence status has change, cannot be removed");
                txn.rollback();
                return Response.status(Status.FORBIDDEN).entity(g.toJson(OutputStatus.Messages.OCCURRENCE_STATUS_HAS_CHANGED)).build();
            }

            OccurrenceValues.deleteOccurrence(occurrenceKey);
            datastore.delete(txn, occurrenceKey);
            PointSystem.removePoints(PointSystem.ACTIONS.create_occurrence, reply.getUser().getKey().getName());

            txn.commit();
            LOG.info("occurrence deleted successfully");
            return Response.ok(g.toJson(OutputStatus.Messages.OCCURRENCE_REMOVED)).build();
        } catch (EntityNotFoundException e) {
            txn.rollback();
            LOG.warning("occurrence doesn't exist.");
            return Response.status(Response.Status.BAD_REQUEST).entity(g.toJson(OutputStatus.Messages.OCCURRENCE_DOES_NOT_EXIST)).build();
        } finally {
            if (txn != null && txn.isActive()) {
                txn.rollback();
                LOG.severe("SERVER ERROR");
            }
        }

    }
}