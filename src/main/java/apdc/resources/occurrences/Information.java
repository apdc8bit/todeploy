package apdc.resources.occurrences;

import apdc.input.data.basic.MapViewAreaData;
import apdc.input.data.occurrences.OccurrenceIDData;
import apdc.input.verification.utils.Device;
import apdc.input.verification.utils.Reply;
import apdc.input.verification.utils.RequestDataVerification;
import apdc.input.verification.values.OccurrenceValues;
import apdc.output.info.BigAreaInfo;
import apdc.output.info.CommentInfo;
import apdc.output.info.OccurrenceInfo;
import apdc.output.messages.OutputStatus;
import apdc.permissions.Actions;
import apdc.resources.api.OccurrenceResources;
import apdc.tables.occurrence.Comments;
import apdc.tables.occurrence.Occurrences;
import apdc.tables.user.User;
import com.github.davidmoten.geo.Coverage;
import com.github.davidmoten.geo.GeoHash;
import com.github.davidmoten.geo.LatLong;
import com.google.appengine.api.datastore.*;
import com.google.gson.JsonObject;

import javax.ws.rs.Path;
import javax.ws.rs.core.Response;
import java.util.*;

import static apdc.resources.api.Base.*;

@Path("/{device}/occurrenceInfo")
public class Information implements OccurrenceResources.InformationI {

    public Information() {
    }

    @SuppressWarnings("unchecked")
    public Response listMyOccurrences(String device, String cursor, int size, String tokenID, String sessionID, String filter) {
        Reply reply = RequestDataVerification.validateTokenPermission(tokenID, sessionID, device, Actions.list_my_occurrences);
        if (!reply.ok())
            return Response.status(reply.getStatus()).entity(reply.getResponse()).build();

        LOG.info("Attempting to list user's occurrences");

        //Limit number of entities
        int numbOccs = RESPONSE_SIZE;
        if (size > 0 && size < RESPONSE_SIZE)
            numbOccs = size;

        FetchOptions fo = FetchOptions.Builder.withLimit(numbOccs);

        Key userKey = reply.getUser().getKey();

        long countE = -1;
        //If cursor is not null use it
        if (cursor != null && !cursor.isEmpty())
            try {
                fo.startCursor(Cursor.fromWebSafeString(cursor));
            } catch (Exception e) {
                LOG.warning("Invalid Cursor");
                fo = FetchOptions.Builder.withLimit(numbOccs);
            }
        else {
            FetchOptions foa = FetchOptions.Builder.withDefaults();
            Query count = new Query(Occurrences.kind).setAncestor(userKey);
            countE = datastore.prepare(count).countEntities(foa);
        }


        //Query by User Key on Occurrences, sort by new
        Query query = new Query(Occurrences.kind)
                .setAncestor(userKey)
                .addSort(Occurrences.creation_time, Query.SortDirection.DESCENDING);
        QueryResultList<Entity> result = datastore.prepare(query).asQueryResultList(fo);
        List<OccurrenceInfo> occurrences = OccurrenceValues.getListWithUserVote(result, reply.getUser().getKey());

        LOG.info("Sending " + occurrences.size() + " occurrences");

        //Combine occurrence list and cursor
        JsonObject json = new JsonObject();
        json.add("data", g.toJsonTree(occurrences));
        json.addProperty("cursor", result.getCursor().toWebSafeString());
        if (countE != -1)
            json.addProperty("number", countE);

        return Response.ok(g.toJson(json)).build();
    }

    @Deprecated
    @SuppressWarnings("unchecked")
    public Response listOccurrenceByArea(MapViewAreaData data, boolean cluster, String device, String tokenID, String sessionID) {
        Reply reply = RequestDataVerification.validateData(data);
        if (!reply.ok())
            return Response.status(reply.getStatus()).entity(reply.getResponse()).build();

        LOG.info("Attempting to list all occurrences by area");

        //https://github.com/davidmoten/geo
        Coverage box = GeoHash.coverBoundingBox(data.top_left_lat, data.top_left_lng, data.bottom_right_lat, data.bottom_right_lng, 4);

        //Get all the hashes contained within the bounds
        List<String> areas = new ArrayList<>(box.getHashes());

        //Filter Occurrences archived
        Query.Filter archived_filter = new Query.FilterPredicate(Occurrences.archived, Query.FilterOperator.EQUAL, false);

        //If the are more than 4 return empty list
        if (areas.size() > 4) {
            LOG.warning("Map area is too big");
            List<BigAreaInfo> numMarkers = new LinkedList<>();
            for (String area : areas) {
                char[] c = area.toCharArray();
                c[c.length - 1]++;
                String geohash_bigger = new String(c);
                //Filter Occurrences with equal hash
                Query.Filter hash_bigger_filter = new Query.FilterPredicate(Occurrences.geoHash, Query.FilterOperator.GREATER_THAN_OR_EQUAL, area);
                Query.Filter hash_smaller_filter = new Query.FilterPredicate(Occurrences.geoHash, Query.FilterOperator.LESS_THAN, geohash_bigger);

                //And filter
                Query.CompositeFilter and_filter = Query.CompositeFilterOperator.and(hash_bigger_filter, hash_smaller_filter, archived_filter);

                //Query Occurrences, sort by new
                Query ctrQuery = new Query(Occurrences.kind)
                        //.addSort(Occurrences.creation_time, Query.SortDirection.DESCENDING)
                        .setFilter(and_filter);

                //Get only keys
                int numbOcc = datastore.prepare(ctrQuery.setKeysOnly()).countEntities(FetchOptions.Builder.withDefaults());

                LatLong point = GeoHash.decodeHash(area);
                double lat = point.getLat();
                double lng = point.getLon();
                numMarkers.add(new BigAreaInfo(numbOcc, lat, lng));
            }

            return Response.status(Response.Status.ACCEPTED).entity(g.toJson(numMarkers)).build();
        }

        //List<Entity> results = new LinkedList<>();
        Set<Entity> results = new HashSet<>();
        for (String area : areas) {
            char[] c = area.toCharArray();
            c[c.length - 1]++;
            String geohash_bigger = new String(c);
            //Filter Occurrences with equal hash
            Query.Filter hash_bigger_filter = new Query.FilterPredicate(Occurrences.geoHash, Query.FilterOperator.GREATER_THAN_OR_EQUAL, area);
            Query.Filter hash_smaller_filter = new Query.FilterPredicate(Occurrences.geoHash, Query.FilterOperator.LESS_THAN, geohash_bigger);
            //And filter
            Query.CompositeFilter and_filter = Query.CompositeFilterOperator.and(hash_bigger_filter, hash_smaller_filter, archived_filter);

            //Query Occurrences, sort by new
            Query ctrQuery = new Query(Occurrences.kind)
                    //.addSort(Occurrences.creation_time, Query.SortDirection.DESCENDING)
                    .setFilter(and_filter);

            //Get only keys
            results.addAll(datastore.prepare(ctrQuery).asList(FetchOptions.Builder.withDefaults()));
        }
/*
        if (!all && results.size() > 1001) {
            LOG.warning("Too many markers: " + results.size());
            double lat = 0;
            double lng = 0;
            return Response.status(Response.Status.ACCEPTED).entity(g.toJson(new BigAreaInfo(results.size(), lat, lng))).build();
        }
*/
        List<Entity> res = new LinkedList<>();
        res.addAll(results);

        //Check if user is login
        Reply reply2 = RequestDataVerification.validateToken(tokenID, sessionID, device);
        List<OccurrenceInfo> occurrences;

        if (cluster) {
            if (reply2.ok()) {
                occurrences = OccurrenceValues.getListWithUserVote(res, reply2.getUser().getKey());
                LOG.info("Returning with user votes");
            } else
                occurrences = OccurrenceValues.getListWithUserVote(res, null);
        } else {
            if (reply2.ok()) {
                occurrences = OccurrenceValues.getListWithUserVote(res, reply2.getUser().getKey());
                LOG.info("Returning with user votes");
            } else
                occurrences = OccurrenceValues.getListWithUserVote(res, null);
        }

/*
        Map<Long, String> test = new HashMap<>();
        for (OccurrenceInfo occ : occurrences) {
            if (test.containsKey(occ.id)) {
                LOG.severe("Duplicated: " + occ.id);
            }
            test.put(occ.id, "s");
        }
*/

        LOG.info("Sending " + occurrences.size() + " occurrences");
        return Response.ok(g.toJson(occurrences)).build();
    }

    @Override
    public Response getMarkers(String geohash, Boolean cluster, String device, String tokenID, String sessionID) {
        int maxMarkers = 30;
        if (Device.confirmMobile(device))
            maxMarkers = 15;

        cluster = cluster == null ? true : cluster;
        cluster = geohash.length() < 4 ? true : cluster;
        LOG.info("Cluster: " + cluster);

        //Calculate next geo hash
        if (geohash.length() > 4)
            geohash = geohash.substring(0, 4);

        char[] c = geohash.toCharArray();
        c[c.length - 1]++;
        String geohash_bigger = new String(c);

        //Filter Occurrences with equal hash
        Query.Filter hash_bigger_filter = new Query.FilterPredicate(Occurrences.geoHash, Query.FilterOperator.GREATER_THAN_OR_EQUAL, geohash);
        Query.Filter hash_smaller_filter = new Query.FilterPredicate(Occurrences.geoHash, Query.FilterOperator.LESS_THAN, geohash_bigger);
        //Filter Occurrences archived
        Query.Filter archived_filter = new Query.FilterPredicate(Occurrences.archived, Query.FilterOperator.EQUAL, false);
        //And filter
        Query.CompositeFilter and_filter = Query.CompositeFilterOperator.and(hash_bigger_filter, hash_smaller_filter, archived_filter);

        //Query Occurrences
        Query ctrQuery = new Query(Occurrences.kind)
                .setFilter(and_filter)
                .addSort(Occurrences.geoHash)
                .setKeysOnly();


        //Get only keys
        List<Entity> keys_results = datastore.prepare(ctrQuery).asList(FetchOptions.Builder.withDefaults());

        //Check number of markers in area
        int numb_markers = keys_results.size();
        if (numb_markers > maxMarkers || cluster) {
            LOG.warning("Markers: " + numb_markers);

            LatLong point = GeoHash.decodeHash(geohash);
            double lat = point.getLat();
            double lng = point.getLon();

            JsonObject json = new JsonObject();
            json.addProperty("is_markers", false);
            json.add("data", g.toJsonTree(new BigAreaInfo(numb_markers, lat, lng)));
            return Response.ok(g.toJson(json)).build();
        }

        //Get entities
        List<Entity> entity_results = datastore.prepare(ctrQuery.clearKeysOnly()).asList(FetchOptions.Builder.withDefaults());

        //Check if user is login
        Reply reply2 = RequestDataVerification.validateToken(tokenID, sessionID, device);
        List<OccurrenceInfo> occurrences;

        //If user is login generate occurrence list with user votes
        if (reply2.ok()) {
            occurrences = OccurrenceValues.getListWithUserVote(entity_results, reply2.getUser().getKey());
            LOG.info("Returning with user votes");
        } else {
            LOG.info("Returning without user votes");
            occurrences = OccurrenceValues.getListWithUserVote(entity_results, null);
        }

        LOG.info("Sending " + numb_markers + " occurrences");
        JsonObject json = new JsonObject();
        json.addProperty("is_markers", true);
        json.add("data", g.toJsonTree(occurrences));

        return Response.ok(g.toJson(json)).build();
    }


    @Override
    @SuppressWarnings("unchecked")
    public Response listOccurrenceComments(OccurrenceIDData data, String device, String cursor, int size) {
        Reply reply = RequestDataVerification.validateData(data);
        if (!reply.ok())
            return Response.status(reply.getStatus()).entity(reply.getResponse()).build();

        LOG.info("Attempting to list occurrence comments");
        Key ownerKey = KeyFactory.createKey(User.kind, data.username);
        Key occKey = KeyFactory.createKey(ownerKey, Occurrences.kind, data.id);

        int numbComs = COMMENT_RESPONSE_SIZE;
        if (size > 0 && size < COMMENT_RESPONSE_SIZE)
            numbComs = size;

        //Limit number of entities
        FetchOptions fo = FetchOptions.Builder.withLimit(numbComs);

        //If cursor is not null use it
        if (cursor != null && !cursor.isEmpty())

            try {
                fo.startCursor(Cursor.fromWebSafeString(cursor));
            } catch (Exception e) {
                LOG.warning("Invalid cursor");
                fo = FetchOptions.Builder.withLimit(numbComs);
            }


        try {
            datastore.get(occKey);

            //Query Comments by Occurrence Key, sort by new
            Query.Filter propertyFilter = new Query.FilterPredicate(Comments.occurrenceKey, Query.FilterOperator.EQUAL, occKey);
            Query ctrQuery = new Query(Comments.kind)
                    .setFilter(propertyFilter)
                    .addSort(Comments.creation_time, Query.SortDirection.DESCENDING);

            //List<Entity> results = datastore.prepare(ctrQuery).asList(FetchOptions.Builder.withDefaults());
            QueryResultList<Entity> results = datastore.prepare(ctrQuery).asQueryResultList(fo);
            List<CommentInfo> comments = OccurrenceValues.listComments(results);

            //Combine occurrence list and cursor
            JsonObject json = new JsonObject();
            json.add("data", g.toJsonTree(comments));
            json.addProperty("cursor", results.getCursor().toWebSafeString());

            LOG.info("Sending " + results.size() + " comments");
            return Response.ok(g.toJson(json)).build();

        } catch (EntityNotFoundException e) {
            LOG.warning("occurrence doesn't exist.");
            return Response.status(Response.Status.BAD_REQUEST).entity(g.toJson(OutputStatus.Messages.OCCURRENCE_DOES_NOT_EXIST)).build();
        }
    }

    @Override
    public Response getOccurrenceInfo(OccurrenceIDData data, String device, String tokenID, String sessionID) {
        Reply reply = RequestDataVerification.validateData(data);
        if (!reply.ok())
            return Response.status(reply.getStatus()).entity(reply.getResponse()).build();

        Key ownerKey = KeyFactory.createKey(User.kind, data.username);
        Key occKey = KeyFactory.createKey(ownerKey, Occurrences.kind, data.id);

        try {
            Entity occ = datastore.get(occKey);

            List<Entity> tmp = new LinkedList<>();
            tmp.add(occ);

            reply = RequestDataVerification.validateToken(tokenID, sessionID, device);
            List<OccurrenceInfo> out;
            if (reply.ok())
                out = OccurrenceValues.getListWithUserVote(tmp, reply.getUser().getKey());
            else
                out = OccurrenceValues.getListWithUserVote(tmp, null);

            return Response.ok(g.toJson(out.get(0))).build();
        } catch (EntityNotFoundException e) {
            LOG.warning("Occurrence not found");
            return Response.status(Response.Status.BAD_REQUEST).entity(g.toJson(OutputStatus.Messages.OCCURRENCE_DOES_NOT_EXIST)).build();
        }
    }

    @Override
    public Response listOccurrencesByDico(String dico, String device, String tokenID, String sessionID) {
        if (dico == null || dico.isEmpty()) {
            LOG.warning("Invalid Dico: " + dico);
            return Response.status(Response.Status.BAD_REQUEST).entity(g.toJson("invalid_dico")).build();
        }

        Query.Filter propertyFilter = new Query.FilterPredicate(Occurrences.dico, Query.FilterOperator.EQUAL, dico);

        Query ctrQuery = new Query(Occurrences.kind)
                .addSort(Occurrences.creation_time, Query.SortDirection.DESCENDING)
                .setFilter(propertyFilter);

        List<Entity> results = datastore.prepare(ctrQuery).asList(FetchOptions.Builder.withDefaults());

        LOG.info("Returning " + results.size() + " occurrences");

        return Response.ok().entity(g.toJson(OccurrenceValues.getListWithUserVote(results, null))).build();
    }

}
