package apdc.resources.api;

import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.gson.Gson;

import java.util.logging.Logger;

public interface Base {
    Logger LOG = Logger.getLogger(Base.class.getName());
    DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
    Gson g = new Gson();
}
