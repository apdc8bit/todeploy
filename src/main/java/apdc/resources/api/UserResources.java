package apdc.resources.api;

import apdc.input.data.SettingsData;
import apdc.input.data.admin.UserIDData;
import apdc.input.data.basic.*;
import apdc.input.data.entity.DeleteUserData;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

public interface UserResources extends Base {

    @Path("/{device}/user/")
    interface AccountI {

        /*
            Check if username already exists
         */
        @GET
        @Path("confirmUsername")
        @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
        Response confirmUsername(@QueryParam("username") String username);

        /*
            Check if email already exists
         */
        @GET
        @Path("confirmEmail")
        @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
        Response confirmEmail(@QueryParam("email") String email);

        /*
            Register a new basic user
         */
        @POST
        @Path("register")
        @Consumes(MediaType.APPLICATION_JSON)
        @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
        Response userRegister(InitialRegistrationData data, @PathParam("device") String device);

        /*
            Change User's Email
         */
        @PUT
        @Path("changeEmail")
        @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
        Response changeEmail(EmailData data, @PathParam("device") String device, @HeaderParam("Authorization") String tokenID, @HeaderParam("SessionID") String sessionID);

        /*
            Add and Edit Information to User's Account
         */
        @PUT
        @Path("add_info")
        @Consumes(MediaType.APPLICATION_JSON)
        @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
        Response userAddEditInfo(AdditionalRegistrationData data, @PathParam("device") String device, @HeaderParam("Authorization") String tokenID, @HeaderParam("SessionID") String sessionID);

        /*
            Set Users Settings
         */
        @PUT
        @Path("setSettings")
        @Consumes(MediaType.APPLICATION_JSON)
        @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
        Response setSettings(SettingsData data, @PathParam("device") String device, @HeaderParam("Authorization") String tokenID, @HeaderParam("SessionID") String sessionID);

        /*
            Delete Information from User's Account
         */
        @PUT
        @Path("delete_info")
        @Consumes(MediaType.APPLICATION_JSON)
        @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
        Response deleteInfo(DeleteInfoData data, @PathParam("device") String device, @HeaderParam("Authorization") String tokenID, @HeaderParam("SessionID") String sessionID);

        /*
            Set User's Account for deletion, sends confirmation email
         */
        @PUT
        @Path("delete")
        @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
        Response userDelete(DeleteUserData data, @HeaderParam("Authorization") String tokenID, @PathParam("device") String device, @HeaderParam("SessionID") String sessionID);

        /*
            Validate User's email and his Account
         */
        @GET
        @Path("validateEmail/{key}")
        @Produces(MediaType.TEXT_HTML + ";charset=utf-8")
        Response userValidateEmail(@PathParam("key") String key);

        /*
            Resend validation email
         */
        @PUT
        @Path("resendEmail")
        @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
        Response resendEmailKey(@QueryParam("key") String key);

        /*
            Confirms User's Account deletion, Account is still in database for x days
         */
        @GET
        @Path("confirm_delete/{key}")
        @Produces(MediaType.TEXT_HTML)
        Response userConfirmDelete(@PathParam("device") String device, @PathParam("key") String key);
    }


    @Path("/{device}/user/")
    interface SessionI {
        /*
            Check is token is still valid and not expired
         */
        @GET
        @Path("/ping")
        @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
        Response ping(@PathParam("device") String device, @HeaderParam("Authorization") String tokenID, @HeaderParam("SessionID") String sessionID);

        /*
            Perform Login for User's Account, creates new Token and SessionID
         */
        @POST
        @Path("/login")
        @Consumes(MediaType.APPLICATION_JSON)
        @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
        Response userLogin(LoginData data, @Context HttpServletRequest request, @Context HttpHeaders headers, @PathParam("device") String device);

        /*
            Invalidates User's Session Token
         */
        @DELETE
        @Path("/logout")
        @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
        Response userLogout(@HeaderParam("Authorization") String tokenID, @PathParam("device") String device, @HeaderParam("SessionID") String sessionID);

        /*
            Android Only, Refreshes Supplied Token
         */
        @PUT
        @Path("/refresh")
        @Consumes(MediaType.APPLICATION_JSON)
        @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
        Response userRefresh(@HeaderParam("Authorization") String tokenID, @PathParam("device") String device, @HeaderParam("SessionID") String sessionID);
    }

    @Path("/{device}/user/")
    interface PasswordI {
        /*
            Change User's current password
         */
        @PUT
        @Path("/password_change")
        @Consumes(MediaType.APPLICATION_JSON)
        @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
        Response userChangePassword(PasswordChangeData data, @HeaderParam("Authorization") String tokenID, @PathParam("device") String device, @HeaderParam("SessionID") String sessionID);

        /*
            Confirm user password
        */
        @PUT
        @Path("/confirm_password")
        @Consumes(MediaType.APPLICATION_JSON)
        @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
        Response userConfirmPassword(ConfirmPasswordData data, @HeaderParam("Authorization") String tokenID, @PathParam("device") String device, @HeaderParam("SessionID") String sessionID);

        /*
            In case of forgotten password, sends an email to create a new password
         */
        @PUT
        @Path("/requestPasswordRecovery")
        @Consumes(MediaType.APPLICATION_JSON)
        @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
        Response userPasswordRecovery(RequestPasswordRecoveryData data, @PathParam("device") String device);

        /*
            Only for forgotten passwords, changes the current password
         */
        @PUT
        @Path("/recoverPassword")
        @Consumes(MediaType.APPLICATION_JSON)
        @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
        Response userRecover(PasswordRecoveryData data, @PathParam("device") String device);
    }

    @Path("/{device}/userInfo/")
    interface InformationI {
        /*
            Returns the users information based on a filter
            filter=amount of information to return
         */
        @GET
        @Path("/getInfo")
        @Consumes(MediaType.APPLICATION_JSON)
        @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
        Response getUserInfo(@PathParam("device") String device, @HeaderParam("Authorization") String tokenID, @HeaderParam("SessionID") String sessionID, @QueryParam("filter") String filter);

        /*
            Returns basic info about another user
         */
        @POST
        @Path("/getBasicInfo")
        @Consumes(MediaType.APPLICATION_JSON)
        @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
        Response getBasicInfo(UserIDData data, @PathParam("device") String device, @HeaderParam("Authorization") String tokenID, @HeaderParam("SessionID") String sessionID);

        /*
            Return a list of the users comments sorted by:
            type= type of value to sort (creation date/alphabetical)
            order= ascending/descending
         */
        @Path("/myComments")
        @Consumes(MediaType.APPLICATION_JSON)
        @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
        Response getUserComments(@PathParam("device") String device, @HeaderParam("Authorization") String tokenID, @HeaderParam("SessionID") String sessionID);

        /*
            Return a list of the users votes sorted by:
            type= type of value to sort (creation date/occurrence title)
            order= ascending/descending
         */
        @GET
        @Path("/myVotes")
        @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
        Response listMyVotes(@PathParam("device") String device, @QueryParam("cursor") String cursor, @HeaderParam("Authorization") String tokenID, @HeaderParam("SessionID") String sessionID);

        /*
            Return the users points and levels
         */
        @GET
        @Path("/myPoints")
        @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
        Response listMyPoints(@PathParam("device") String device, @HeaderParam("Authorization") String tokenID, @HeaderParam("SessionID") String sessionID);

        /*
            Get Users Settings
        */
        @GET
        @Path("getSettings")
        @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
        Response getSettings(@PathParam("device") String device, @HeaderParam("Authorization") String tokenID, @HeaderParam("SessionID") String sessionID);
    }
}
