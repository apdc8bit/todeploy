package apdc.resources.api;

import apdc.input.data.basic.MapViewAreaData;
import apdc.input.data.occurrences.CreateOccurrenceData;
import apdc.input.data.occurrences.EditOccurrenceData;
import apdc.input.data.occurrences.OccurrenceIDData;
import apdc.input.data.occurrences.ReportOccurrenceData;
import apdc.input.data.occurrences.comments.CommentIDData;
import apdc.input.data.occurrences.comments.CommentOccurrenceData;
import apdc.input.data.occurrences.comments.EditCommentData;
import apdc.input.data.occurrences.votes.VoteOccurrenceData;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

public interface OccurrenceResources extends Base {

    @Path("/{device}/occurrence")
    interface ManagementI {

        @POST
        @Path("/check")
        @Consumes(MediaType.APPLICATION_JSON)
        @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
        Response checkNewOccurrence(CreateOccurrenceData data, @PathParam("device") String device, @HeaderParam("Authorization") String tokenID, @HeaderParam("SessionID") String sessionID);

        @POST
        @Path("/add")
        @Consumes(MediaType.APPLICATION_JSON)
        @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
        Response addOccurrence(CreateOccurrenceData data, @PathParam("device") String device, @HeaderParam("Authorization") String tokenID, @HeaderParam("SessionID") String sessionID);

        @PUT
        @Path("/edit")
        @Consumes(MediaType.APPLICATION_JSON)
        @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
        Response editOccurrence(EditOccurrenceData data, @PathParam("device") String device, @HeaderParam("Authorization") String tokenID, @HeaderParam("SessionID") String sessionID);

        @PUT
        @Path("/delete")
        @Consumes(MediaType.APPLICATION_JSON)
        @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
        Response deleteOccurrence(OccurrenceIDData data, @PathParam("device") String device, @HeaderParam("Authorization") String tokenID, @HeaderParam("SessionID") String sessionID);
    }

    @Path("/{device}/occurrence")
    interface InteractionI {
        @POST
        @Path("/vote")
        @Consumes(MediaType.APPLICATION_JSON)
        @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
        Response voteOccurrence(VoteOccurrenceData data, @PathParam("device") String device, @HeaderParam("Authorization") String tokenID, @HeaderParam("SessionID") String sessionID);

        @PUT
        @Path("/deleteVote")
        @Consumes(MediaType.APPLICATION_JSON)
        @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
        Response deleteLikeOccurrence(VoteOccurrenceData data, @PathParam("device") String device, @HeaderParam("Authorization") String tokenID, @HeaderParam("SessionID") String sessionID);

        @POST
        @Path("/comment")
        @Consumes(MediaType.APPLICATION_JSON)
        @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
        Response commentOccurrence(CommentOccurrenceData data, @PathParam("device") String device, @HeaderParam("Authorization") String tokenID, @HeaderParam("SessionID") String sessionID);

        @PUT
        @Path("/deleteComment")
        @Consumes(MediaType.APPLICATION_JSON)
        @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
        Response deleteCommentOccurrence(CommentIDData data, @PathParam("device") String device, @HeaderParam("Authorization") String tokenID, @HeaderParam("SessionID") String sessionID);

        @PUT
        @Path("/editComment")
        @Consumes(MediaType.APPLICATION_JSON)
        @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
        Response editCommentOccurrence(EditCommentData data, @PathParam("device") String device, @HeaderParam("Authorization") String tokenID, @HeaderParam("SessionID") String sessionID);

        @POST
        @Path("/report")
        @Consumes(MediaType.APPLICATION_JSON)
        @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
        Response reportOccurrence(ReportOccurrenceData data, @PathParam("device") String device, @HeaderParam("Authorization") String tokenID, @HeaderParam("SessionID") String sessionID);
    }

    @Path("/{device}/occurrenceInfo")
    interface InformationI {
        int RESPONSE_SIZE = 10;
        int COMMENT_RESPONSE_SIZE = 10;

        @GET
        @Path("/myOccurrences")
        @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
        Response listMyOccurrences(@PathParam("device") String device, @QueryParam("cursor") String cursor, @QueryParam("size") int size, @HeaderParam("Authorization") String tokenID, @HeaderParam("SessionID") String sessionID, @QueryParam("filter") String filter);

        @POST
        @Path("/listByArea")
        @Consumes(MediaType.APPLICATION_JSON)
        @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
        Response listOccurrenceByArea(MapViewAreaData data, @QueryParam("cluster") boolean cluster, @PathParam("device") String device, @HeaderParam("Authorization") String tokenID, @HeaderParam("SessionID") String sessionID);

        @GET
        @Path("/getMarkers/{geohash}/{cluster}")
        @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
        Response getMarkers(@PathParam("geohash") String geohash, @PathParam("cluster") Boolean cluster, @PathParam("device") String device, @HeaderParam("Authorization") String tokenID, @HeaderParam("SessionID") String sessionID);

        @POST
        @Path("/getComments")
        @Consumes(MediaType.APPLICATION_JSON)
        @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
        Response listOccurrenceComments(OccurrenceIDData data, @PathParam("device") String device, @QueryParam("cursor") String cursor, @QueryParam("size") int size);

        @POST
        @Path("/info")
        @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
        Response getOccurrenceInfo(OccurrenceIDData data, @PathParam("device") String device, @HeaderParam("Authorization") String tokenID, @HeaderParam("SessionID") String sessionID);

        @GET
        @Path("/listOccurrencesByDico/{dico}")
        @Consumes(MediaType.APPLICATION_JSON)
        @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
        Response listOccurrencesByDico(@PathParam("dico") String dico, @PathParam("device") String device, @HeaderParam("Authorization") String tokenID, @HeaderParam("SessionID") String sessionID);
    }
}
