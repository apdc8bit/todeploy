package apdc.resources.users;

import apdc.exceptions.ExpiredEmailVerificationException;
import apdc.exceptions.InvalidEmailKeyException;
import apdc.input.data.basic.ConfirmPasswordData;
import apdc.input.data.basic.PasswordChangeData;
import apdc.input.data.basic.PasswordRecoveryData;
import apdc.input.data.basic.RequestPasswordRecoveryData;
import apdc.input.verification.utils.EmailValidationKey;
import apdc.input.verification.utils.Reply;
import apdc.input.verification.utils.RequestDataVerification;
import apdc.input.verification.values.UserValues;
import apdc.output.messages.OutputStatus;
import apdc.permissions.Actions;
import apdc.permissions.UserPermissions;
import apdc.resources.api.UserResources;
import apdc.tables.user.User;
import apdc.util.email.EmailResource;
import apdc.util.keys.KeyGenerator;
import com.google.appengine.api.datastore.*;
import com.google.appengine.repackaged.org.apache.commons.codec.digest.DigestUtils;

import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.Date;
import java.util.UUID;

import static apdc.resources.api.UserResources.LOG;
import static apdc.resources.api.UserResources.datastore;

@Path("/{device}/user")
@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
public class Password implements UserResources.PasswordI {

    public Password() {
    }

    @Override
    public Response userChangePassword(PasswordChangeData data, String tokenID, String device, String sessionID) {
        Reply reply = RequestDataVerification.validateDataTokenPermission(data, tokenID, sessionID, device, Actions.change_self_password);
        if (!reply.ok())
            return Response.status(reply.getStatus()).entity(reply.getResponse()).build();

        LOG.info("Changing user password.");

        TransactionOptions options = TransactionOptions.Builder.withXG(true);
        Transaction txn = datastore.beginTransaction(options);

        try {
            Entity user = reply.getUser();

            String hashedPWD = (String) user.getProperty(User.password);

            //Check Passwords
            reply = UserValues.comparePasswords(hashedPWD, data.password_old);
            if (!reply.ok()) {
                LOG.info("User " + user.getKey().getName() + ": wrong password");
                txn.rollback();
                return Response.status(reply.getStatus()).entity(reply.getResponse()).build();
            }

            String oldPass = (String) user.getProperty(User.old_password);
            String newPass = DigestUtils.sha512Hex(data.password_new);

            //Check if new password is not equal to old password changed
            if (oldPass != null && oldPass.equals(newPass)) {
                LOG.info("Password match new one");
                txn.rollback();
                return Response.status(Response.Status.BAD_REQUEST).entity(OutputStatus.PASSWORD_MATCHES_OLD).build();
            }

            user.setUnindexedProperty(User.old_password, hashedPWD);
            user.setUnindexedProperty(User.password, DigestUtils.sha512Hex(data.password_new));
            user.setProperty(User.last_edit_time, new Date());

            //Invalidate token
            KeyGenerator.invalidateToken(tokenID, sessionID, KeyGenerator.getTypeFromDevice(device));

            datastore.put(txn, user);
            txn.commit();

            LOG.info(user.getKey().getName() + "'s password has been changed.");
            return Response.ok(OutputStatus.PASSWORD_CHANGED).build();
        } finally {
            if (txn != null && txn.isActive()) {
                txn.rollback();
                LOG.severe("SERVER ERROR");
            }
        }
    }

    @Override
    public Response userConfirmPassword(ConfirmPasswordData data, String tokenID, String device, String sessionID) {
        Reply reply = RequestDataVerification.validateDataTokenPermission(data, tokenID, sessionID, device, Actions.change_self_password);
        if (!reply.ok())
            return Response.status(reply.getStatus()).entity(reply.getResponse()).build();

        LOG.info("Confirming user password.");

        Entity user = reply.getUser();

        String hashedPWD = (String) user.getProperty(User.password);

        //Check Passwords
        reply = UserValues.comparePasswords(hashedPWD, data.password);

        return Response.status(reply.getStatus()).entity(reply.getResponse()).build();
    }

    @Override
    public Response userPasswordRecovery(RequestPasswordRecoveryData data, String device) {
        Reply reply = RequestDataVerification.validateData(data);
        if (!reply.ok())
            return Response.status(reply.getStatus()).entity(reply.getResponse()).build();

        LOG.info("Recovering " + data.username + "'s password.");

        Transaction txn = datastore.beginTransaction(TransactionOptions.Builder.withXG(true));

        Entity user;
        try {
            Key userKey = KeyFactory.createKey(User.kind, data.username);
            user = datastore.get(userKey);

            Reply reply2 = UserPermissions.isAllowed(user, Actions.change_self_password, device);
            if (!reply2.ok()) {
                txn.rollback();
                return Response.status(reply2.getStatus()).entity(reply2.getResponse()).build();
            }

            //Compare Emails
            if (user.getProperty(User.email).equals(data.email)) {
                String key = EmailValidationKey.getKey(user.getKey().getName());

                //Invalidate Current Pass
                String currPass = (String) user.getProperty(User.password);
                user.setUnindexedProperty(User.password, UUID.randomUUID().toString());
                user.setUnindexedProperty(User.old_password, currPass);

                EmailResource.sendPasswordResetEmail(data.username, data.email, key);

                datastore.put(txn, user);
                txn.commit();
                return Response.ok(OutputStatus.PASSWORD_RECOVERED).build();
            } else {
                LOG.warning("Username does not match Email.");
                txn.rollback();
                return Response.status(Response.Status.FORBIDDEN).entity(OutputStatus.USERNAME_EMAIL_NO_MATCH).build();
            }

        } catch (EntityNotFoundException e) {
            LOG.warning("User does not exist.");
            txn.rollback();
            return Response.status(Response.Status.BAD_REQUEST).entity(OutputStatus.NO_USER_FOUND).build();
        } finally {
            if (txn != null && txn.isActive()) {
                txn.rollback();
                LOG.severe("SERVER ERROR");
            }
        }
    }

    @Override
    public Response userRecover(PasswordRecoveryData data, String device) {
        Reply reply = RequestDataVerification.validateData(data);
        if (!reply.ok())
            return Response.status(reply.getStatus()).entity(reply.getResponse()).build();

        //Validate key
        Entity user;
        try {
            user = EmailValidationKey.getUserValid(data.key);
        } catch (ExpiredEmailVerificationException e) {
            return Response.status(Response.Status.FORBIDDEN).entity(OutputStatus.EXPIRED_KEY).build();
        } catch (InvalidEmailKeyException e) {
            return Response.status(Response.Status.BAD_REQUEST).entity(OutputStatus.INVALID_KEY).build();
        }

        TransactionOptions options = TransactionOptions.Builder.withXG(true);
        Transaction txn = datastore.beginTransaction(options);
        try {
            String oldPass = (String) user.getProperty(User.old_password);
            String newPass = DigestUtils.sha512Hex(data.new_password);

            //Check if new password is not equal to old password changed
            if ((oldPass != null && oldPass.equals(newPass))) {
                return Response.status(Response.Status.BAD_REQUEST).entity(OutputStatus.PASSWORD_ALREADY_USED).build();
            }

            user.setUnindexedProperty(User.password, DigestUtils.sha512Hex(data.new_password));
            user.setProperty(User.last_edit_time, new Date());
            datastore.put(txn, user);
            txn.commit();

            LOG.info(user.getKey().getName() + "'s password has been updated.");
            return Response.ok(OutputStatus.PASSWORD_CHANGED).build();
        } finally {
            if (txn != null && txn.isActive()) {
                txn.rollback();
                LOG.severe("SERVER ERROR");
            }
        }
    }
}
