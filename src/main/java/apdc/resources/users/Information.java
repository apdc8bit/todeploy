package apdc.resources.users;

import apdc.gamification.PointSystem;
import apdc.gamification.Values;
import apdc.input.data.admin.UserIDData;
import apdc.input.verification.utils.Reply;
import apdc.input.verification.utils.RequestDataVerification;
import apdc.input.verification.values.OccurrenceValues;
import apdc.input.verification.values.UserValues;
import apdc.output.info.AllUserInfo;
import apdc.output.info.SettingsInfo;
import apdc.output.info.UserPointsInfo;
import apdc.output.messages.OutputStatus;
import apdc.permissions.Actions;
import apdc.permissions.UserPermissions;
import apdc.resources.api.UserResources;
import apdc.tables.occurrence.Comments;
import apdc.tables.occurrence.Votes;
import apdc.tables.user.Settings;
import apdc.tables.user.User;
import com.google.appengine.api.datastore.*;

import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import static apdc.resources.api.UserResources.*;


@Path("/{device}/userInfo")
@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
public class Information implements UserResources.InformationI {

    public Information() {
    }

    @Override
    public Response getUserInfo(String device, String tokenID, String sessionID, String filter) {
        Reply reply = RequestDataVerification.validateTokenPermission(tokenID, sessionID, device, Actions.list_self_info);
        if (!reply.ok())
            return Response.status(reply.getStatus()).entity(reply.getResponse()).build();

        LOG.info("Returning user info");

        UserValues.INFO infoE = UserValues.getInfo(filter);
        Entity user = reply.getUser();

        String imageID = null, email = null, username = null, role = null, zipcode = null, name = null, address = null;
        Date lastEdit = null, register_date = null;
        Number phoneNumber = null, nif = null, points = null, level = null, next_level_points = null, curr_level_points = null;
        boolean valid = false;
        Boolean isVolunteer = false;

        switch (infoE) {
            case ALL:
                name = (String) user.getProperty(User.name);
                phoneNumber = (Number) user.getProperty(User.phone_number);
                address = (String) user.getProperty(User.address);
                nif = (Number) user.getProperty(User.nif);
                zipcode = (String) user.getProperty(User.zip_code);
            case BASIC:
                username = user.getKey().getName();
                email = (String) user.getProperty(User.email);
                imageID = (String) user.getProperty(User.imageID);
                lastEdit = (Date) user.getProperty(User.last_edit_time);
                register_date = (Date) user.getProperty(User.creation_time);
                role = (String) user.getProperty(User.role);
                valid = (boolean) user.getProperty(User.valid);
                Values values = PointSystem.getPointsAndLevel(username);
                points = values.getPoints();
                level = values.getLevel();
                isVolunteer = (Boolean) user.getProperty(User.volunteer);
                isVolunteer = isVolunteer == null ? false : isVolunteer;
                next_level_points = PointSystem.pointsForLevelUp(((long) level) + 1);
                curr_level_points = PointSystem.pointsForLevelUp(((long) level));
            case NONE:
                break;
        }

        AllUserInfo out = new AllUserInfo(username, email, role, imageID, zipcode, name, phoneNumber, address, nif, lastEdit, register_date, valid, points, level, isVolunteer, next_level_points, curr_level_points);

        return Response.ok(g.toJson(out)).build();
    }

    @Override
    public Response getBasicInfo(UserIDData data, String device, String tokenID, String sessionID) {
        Reply reply = RequestDataVerification.validateDataTokenPermission(data, tokenID, sessionID, device, Actions.list_other_basic_info);
        if (!reply.ok())
            return Response.status(reply.getStatus()).entity(reply.getResponse()).build();

        LOG.info("Returning user info");

        try {
            Key userKey = KeyFactory.createKey(User.kind, data.username);
            Entity user = datastore.get(userKey);

            String role = (String) user.getProperty(User.role);
            if (!role.equals(UserPermissions.ROLE.u_basic.name())) {
                LOG.warning("User role is not basic");
                return Response.status(Response.Status.FORBIDDEN).entity("user_role_not_basic").build();
            }

            return Response.ok(g.toJson(UserValues.getBasicUserInfo(user))).build();
        } catch (EntityNotFoundException e) {
            LOG.warning("User not found");
            return Response.status(Response.Status.BAD_REQUEST).entity(OutputStatus.NO_USER_FOUND).build();
        }
    }

    @Override
    public Response getUserComments(String device, String tokenID, String sessionID) {
        Reply reply = RequestDataVerification.validateTokenPermission(tokenID, sessionID, device, Actions.list_self_comments);
        if (!reply.ok())
            return Response.status(reply.getStatus()).entity(reply.getResponse()).build();

        LOG.info("Returning user comments");

        Entity user = reply.getUser();

        //Filter Comments by user key
        Query.Filter propertyFilter = new Query.FilterPredicate(Comments.userKey, Query.FilterOperator.EQUAL, user.getKey());
        //Order by most recent
        Query ctrQuery = new Query(Comments.kind)
                .addSort(Comments.creation_time, Query.SortDirection.DESCENDING)
                .setFilter(propertyFilter);
        List<Entity> results = datastore.prepare(ctrQuery).asList(FetchOptions.Builder.withDefaults());

        LOG.info("Information returned to: " + user.getKey().getName());

        return Response.ok(g.toJson(OccurrenceValues.listComments(results))).build();
    }


    @Override
    @SuppressWarnings("unchecked")
    public Response listMyVotes(String device, String cursor, String tokenID, String sessionID) {
        Reply reply = RequestDataVerification.validateTokenPermission(tokenID, sessionID, device, Actions.list_self_votes);
        if (!reply.ok())
            return Response.status(reply.getStatus()).entity(reply.getResponse()).build();

        LOG.info("Attempting to list user's occurrences");


        Key userKey = reply.getUser().getKey();

        //Filter Votes by user key
        Query.Filter propertyFilter = new Query.FilterPredicate(Votes.userKey, Query.FilterOperator.EQUAL, userKey);
        //Order by most recent
        Query ctrQuery = new Query(Votes.kind)
                .addSort(Votes.added_date, Query.SortDirection.DESCENDING)
                .setFilter(propertyFilter);

        List<Entity> results = datastore.prepare(ctrQuery).asList(FetchOptions.Builder.withDefaults());

        List<Key> occKeys = new LinkedList<>();

        //Get the occurrences keys
        for (Entity e : results) {
            occKeys.add((Key) e.getProperty(Votes.occurrenceKey));
        }

        List<Entity> occs = new LinkedList<>();

        //Get the occurrences entities
        for (Key k : occKeys) {
            try {
                occs.add(datastore.get(k));
            } catch (EntityNotFoundException e) {
                LOG.warning("Occurrence Not Found");
            }
        }

        return Response.ok(g.toJson(OccurrenceValues.getListWithUserVote(occs, reply.getUser().getKey()))).build();
    }

    @SuppressWarnings("unchecked")
    public Response listMyPoints(String device, String tokenID, String sessionID) {
        Reply reply = RequestDataVerification.validateTokenPermission(tokenID, sessionID, device, Actions.list_self_points);
        if (!reply.ok())
            return Response.status(reply.getStatus()).entity(reply.getResponse()).build();

        LOG.info("Returning user points, level and next level");

        Values values = PointSystem.getPointsAndLevel(reply.getUser().getKey().getName());
        long next_level_up = PointSystem.pointsForLevelUp(values.getLevel() + 1);
        long curr_level_points = PointSystem.pointsForLevelUp(values.getLevel());

        UserPointsInfo out = new UserPointsInfo(values.getPoints(), values.getLevel(), next_level_up, curr_level_points);

        return Response.ok(out).build();
    }

    @Override
    public Response getSettings(String device, String tokenID, String sessionID) {
        Reply reply = RequestDataVerification.validateTokenPermission(tokenID, sessionID, device, Actions.change_settings);
        if (!reply.ok())
            return Response.status(reply.getStatus()).entity(reply.getResponse()).build();

        String key = KeyFactory.keyToString(reply.getUser().getKey());
        try {
            Key settingsKey = KeyFactory.createKey(Settings.kind, key);
            Entity settings = datastore.get(settingsKey);
            Boolean isColorBlind = (Boolean) settings.getProperty(Settings.colorBind);


            return Response.ok(g.toJson(new SettingsInfo(isColorBlind))).build();
        } catch (EntityNotFoundException e) {
            return Response.ok().build();
        }
    }
}

