package apdc.resources.users;

import apdc.input.data.basic.LoginData;
import apdc.input.verification.utils.Device;
import apdc.input.verification.utils.Reply;
import apdc.input.verification.utils.RequestDataVerification;
import apdc.input.verification.values.UserValues;
import apdc.output.info.TokenSessionInfo;
import apdc.output.messages.OutputStatus;
import apdc.resources.api.UserResources;
import apdc.tables.user.User;
import apdc.util.keys.KeyGenerator;
import apdc.util.keys.KeyInterface;
import com.google.gson.JsonObject;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Path;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;

import static apdc.resources.api.UserResources.LOG;
import static apdc.resources.api.UserResources.g;

@Path("/{device}/user")
public class Session implements UserResources.SessionI {

    public Session() {
    }

    @Override
    public Response ping(String device, String tokenID, String sessionID) {
        //Validate token
        Reply reply = RequestDataVerification.validateToken(tokenID, sessionID, device);

        if (reply.ok()) {
            String role = (String) reply.getUser().getProperty(User.role);
            boolean valid = (boolean) reply.getUser().getProperty(User.valid);

            JsonObject json = new JsonObject();
            json.addProperty("role", role);

            if (valid) {
                json.addProperty("message", OutputStatus.Messages.OK.name());
                json.addProperty("code", 2323);
            } else {
                json.addProperty("message", OutputStatus.Messages.USER_NOT_VALID.name());
                json.addProperty("code", 4545);
            }

            return Response.ok(g.toJson(json)).build();
        }

        return Response.status(reply.getStatus()).entity(reply.getResponse()).build();
    }

    @Override
    public Response userLogin(LoginData data, HttpServletRequest request, HttpHeaders headers, String device) {
        Reply reply = RequestDataVerification.validateData(data);
        if (!reply.ok())
            return Response.status(reply.getStatus()).entity(reply.getResponse()).build();

        LOG.info("Attempting basic login: " + data.username);

        reply = UserValues.loginBasic(data, request, headers, device);
        return Response.status(reply.getStatus()).entity(reply.getResponse()).build();
    }

    @Override
    public Response userLogout(String tokenID, String device, String sessionID) {
        LOG.info("Logging out user.");

        //Adds token to InvalidTokens table
        KeyGenerator.invalidateToken(tokenID, sessionID, KeyGenerator.getTypeFromDevice(device));

        return Response.ok(g.toJson(OutputStatus.LOGOUT_OK)).build();
    }

    @Override
    public Response userRefresh(String tokenID, String device, String sessionID) {
        if (!device.equals(Device.MOBILE))
            return Response.status(Response.Status.FORBIDDEN).entity(OutputStatus.WRONG_DEVICE).build();

        Reply reply = RequestDataVerification.validateToken(tokenID, sessionID, device);
        if (!reply.ok())
            return Response.status(reply.getStatus()).entity(reply.getResponse()).build();

        String token = KeyGenerator.getCypher(reply.getUser().getKey().getName(), sessionID, KeyInterface.Type.MOBILE);
        LOG.info("Session: " + sessionID + ", Token: " + token);

        TokenSessionInfo tokenInfo = new TokenSessionInfo(token, sessionID, null, null, null, null, null, null);
        return Response.ok(g.toJson(tokenInfo)).build();
    }
}
