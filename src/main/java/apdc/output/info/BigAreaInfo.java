package apdc.output.info;

public class BigAreaInfo {
    public int count;
    public double lat;
    public final double lng;
    public boolean isCluster;

    public BigAreaInfo(int count, double lat, double lng) {
        this.count = count;
        this.lat = lat;
        this.lng = lng;
        this.isCluster = true;
    }
}
