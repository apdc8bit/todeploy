package apdc.output.info;

import java.util.Date;

public class WorkersInfo {
    public final String worker_username;
    public final Date date_added;

    public WorkersInfo(String worker_username, Date date_added) {
        this.worker_username = worker_username;
        this.date_added = date_added;
    }
}
