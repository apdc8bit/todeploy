package apdc.output.info;

import java.util.Date;

public class EntityInfo {

    public final String imageID;
    public final String district;
    public final String county;
    public final String address;
    public final String name;
    public final String dico;
    public final Date creation_time;
    public final Double lat;
    public final Double lng;
    public final Long totalStarted;
    public final Long totalCompleted;
    public final Long avgResponseTime;
    public final Long avgCompletionTime;

    public EntityInfo(String imageID, String district, String county, String address, String name, Date creation_time, Double lat, Double lng, Long totalStarted, Long totalCompleted, Long avgResponseTime, Long avgCompletionTime, String dico) {
        this.imageID = imageID;
        this.district = district;
        this.county = county;
        this.address = address;
        this.name = name;
        this.creation_time = creation_time;
        this.lat = lat;
        this.lng = lng;
        this.totalStarted = totalStarted;
        this.totalCompleted = totalCompleted;
        this.avgResponseTime = avgResponseTime;
        this.avgCompletionTime = avgCompletionTime;
        this.dico = dico;
    }
}
