package apdc.output.info;

import java.util.Date;
import java.util.List;

public class EventInfo {
    public final String title;
    public final String description;
    public final String worker;
    public final String state;
    public final String address;
    public final String dico;
    public final String district;
    public final String county;
    public final EntityInfo entity;
    public final Date eventTime;
    public final double lat;
    public final double lng;
    public final long id;
    public final List<BasicUserInfo> usersJoined;
    public final boolean isGoing;

    public EventInfo(String title, String description, String worker, Date eventTime,
                     double lat, double lng, long id, List<BasicUserInfo> usersJoined, EntityInfo entity, String state,
                     String address, String dico, String district, String county, boolean isGoing) {

        this.title = title;
        this.description = description;
        this.worker = worker;
        this.eventTime = eventTime;
        this.lat = lat;
        this.lng = lng;
        this.id = id;
        this.usersJoined = usersJoined;
        this.entity = entity;
        this.state = state;
        this.address = address;
        this.dico = dico;
        this.district = district;
        this.county = county;
        this.isGoing = isGoing;
    }
}
