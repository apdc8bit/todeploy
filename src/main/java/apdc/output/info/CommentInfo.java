package apdc.output.info;

import java.util.Date;

public class CommentInfo {
    public final long comment_id;
    public final String comment;
    public final String username;
    public final String imageID;
    public final Date creating_time;
    public final Date last_edit_time;

    public CommentInfo(long comment_id, String comment, Date creating_time, Date last_edit_time, String username, String imageID) {
        this.comment_id = comment_id;
        this.comment = comment;
        this.creating_time = creating_time;
        this.last_edit_time = last_edit_time;
        this.username = username;
        this.imageID = imageID;
    }
}
