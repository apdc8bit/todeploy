package apdc.output.info;

import java.util.Date;
import java.util.List;

public class OccurrenceInfo {
    public long id, comment_count, vote_count;
    public String username, title, description, type, status, imageID, geoHash, completedImageID, county, district, dico, address;
    public final double lat;
    public final double lng;
    public List<Double> lats, lngs;
    public Date creationDate;
    public boolean has_vote;
    public int priority;


    public OccurrenceInfo(long id, String username, String geoHash, String title, String description, String type, String status,
                          String imageID, double lat, double lng, List<Double> lats, List<Double> lngs, Date creationDate,
                          long comment_count, long vote_count, boolean has_vote, String completedImageID,
                          String county, String district, String dico, int priority, String address) {
        this.id = id;
        this.username = username;
        this.title = title;
        this.description = description;
        this.type = type;
        this.status = status;
        this.imageID = imageID;
        this.lat = lat;
        this.lng = lng;
        this.lats = lats;
        this.lngs = lngs;
        this.creationDate = creationDate;
        this.comment_count = comment_count;
        this.vote_count = vote_count;
        this.has_vote = has_vote;
        this.geoHash = geoHash;
        this.completedImageID = completedImageID;
        this.county = county;
        this.district = district;
        this.dico = dico;
        this.priority = priority;
        this.address = address;
    }

    public int count;
    public boolean isCluster;
    public List<OccurrenceInfo> markers;

    public OccurrenceInfo(double lat, double lng, int count, List<OccurrenceInfo> markers) {

        this.lat = lat;
        this.lng = lng;
        this.count = count;
        this.isCluster = true;
        this.markers = markers;
    }
}
