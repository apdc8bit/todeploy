package apdc.output.info;

import java.util.Date;

public class AllUserInfo {
    public final String imageID;
    public String email;
    public String username;
    public String role;
    public String zipcode;
    public String name;
    public String address;
    public Number phoneNumber, nif, points, level, next_level_points, curr_level_points;
    public Date register_date, lastEdit;
    public boolean valid, isVolunteer;

    public AllUserInfo(String username, String email, String role, String imageID, String zipcode, String name, Number phoneNumber, String address, Number nif, Date lastEdit, Date register_date, boolean valid, Number points, Number level, boolean isVolunteer, Number next_level_points, Number curr_level_points) {
        this.email = email;
        this.username = username;
        this.role = role;
        this.imageID = imageID;
        this.name = name;
        this.phoneNumber = phoneNumber;
        this.address = address;
        this.nif = nif;
        this.zipcode = zipcode;
        this.lastEdit = lastEdit;
        this.register_date = register_date;
        this.points = points;
        this.level = level;
        this.valid = valid;
        this.isVolunteer = isVolunteer;
        this.next_level_points = next_level_points;
        this.curr_level_points = curr_level_points;
    }
}
