package apdc.permissions;

import apdc.util.CalendarUtils;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.google.gson.stream.JsonReader;

import java.lang.reflect.Type;
import java.util.Date;
import java.util.Map;
import java.util.logging.Logger;

public class PermissionTableSingleton {
    private final Logger LOG = Logger.getLogger(PermissionTableSingleton.class.getName());
    private final int DAYS = 1;

    private static PermissionTableSingleton instance = null;

    private Map<String, Map<String, Boolean>> table;
    private Date load_date;

    private PermissionTableSingleton() {

    }

    public String getTableJson() {
        return new Gson().toJson(table);
    }

    public boolean setTableJson(String jsonTable) {
        Type type = new TypeToken<Map<String, Map<String, Boolean>>>() {
        }.getType();

        Map<String, Map<String, Boolean>> tableTMP = new Gson().fromJson(jsonTable, type);

        if (checkIntegrity(tableTMP)) {
            table = tableTMP;
            return true;
        }

        return false;
    }

    public static PermissionTableSingleton getInstance() {
        if (instance == null) {
            instance = new PermissionTableSingleton();
        }
        return instance;
    }

    public boolean hasPermission(String role, String action) {
        checkTables();

        try {
            return table.get(action).get(role);
        } catch (NullPointerException n) {
            LOG.severe("Permission not found");
            return false;
        }
    }

    private void checkTables() {
        if (table == null || load_date == null || isExpired(load_date)) {
            loadPermissions();
        }
    }

    private boolean isExpired(Date loadDate) {
        return CalendarUtils.addDays(loadDate, DAYS).before(new Date());
    }

    @SuppressWarnings("unchecked")
    private void loadPermissions() {
        Type type = new TypeToken<Map<String, Map<String, Boolean>>>() {
        }.getType();

        Gson gson = new Gson();
        JsonReader jsonReader = apdc.util.FileReader.read("permissions");
        if (jsonReader != null)
            table = gson.fromJson(jsonReader, type);

        load_date = CalendarUtils.addDays(new Date(), DAYS);

        checkIntegrity(table);
    }


    private boolean checkIntegrity(Map<String, Map<String, Boolean>> table) {
        if (table == null) {
            LOG.severe("Error loading table");
            return false;
        }

        try {
            for (Actions action : Actions.values())
                table.get(action.name());
        } catch (NullPointerException n) {
            LOG.severe("Table is incomplete!!!");
            return false;
        }

        return true;
    }
}


