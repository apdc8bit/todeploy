package apdc.permissions;

public enum Actions {
    //basic user
    change_self_email,
    edit_self_profile,
    delete_self_account,
    change_self_password,
    list_self_info,
    list_other_basic_info,
    list_self_comments,
    list_self_votes,
    add_image,
    list_self_points,
    change_settings,
    join_event,

    //occurrence
    create_new_occurrence,
    edit_self_occurrence,
    delete_self_occurrence,
    vote_occurrence,
    delete_vote,
    comment_occurrence,
    delete_comment,
    edit_comment,
    report_occurrence,
    list_my_occurrences,
    get_info_all_occurrences,

    //worker
    list_self_worker_jobs,
    change_other_occurrence_type,
    mark_occurrence_complete,
    manage_event,

    //entity
    create_worker,
    edit_self_worker,
    delete_self_worker,
    associate_self_occurrence_worker,
    disassociate_self_occurrence_worker,
    confirm_self_job_completion,
    list_self_workers,
    list_self_jobs,
    list_occurrences_in_area,
    create_new_event,

    //admin
    edit_all_occurrences,
    list_all_occurrences,
    delete_all_occurrences,
    delete_all_comments,
    delete_all_users,
    list_all_users,
    verify_role_change,
    create_new_admin,
    list_reports,
    confirm_report
}
