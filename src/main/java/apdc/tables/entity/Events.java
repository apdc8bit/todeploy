package apdc.tables.entity;

public class Events {
    public static final String kind = "Events";
    public static final String workerKey = "worker_key";
    public static final String entityKey = "entity_key";
    public static final String title = "title";
    public static final String description = "description";
    public static final String eventTime = "event_time";
    public static final String lat = "event_lat";
    public static final String lng = "event_lng";
    public static final String state = "event_state";

    public static final String dico = "event_dico";
    public static final String county = "event_county";
    public static final String district = "event_district";
    public static final String address = "event_address";
}
