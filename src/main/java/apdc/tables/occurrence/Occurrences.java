package apdc.tables.occurrence;

public class Occurrences {
    public static final String kind = "Occurrence"; //key = user key + random id
    public static final String title = "occurrence_title";
    public static final String description = "occurrence_description";
    public static final String lat = "occurrence_latitude";
    public static final String lng = "occurrence_longitude";
    public static final String imageID = "occurrence_image_id";
    public static final String status = "occurrence_status";
    public static final String type = "occurrence_type";
    public static final String creation_time = "occurrence_creation_time";
    public static final String initiation_time = "occurrence_initiation_time";
    public static final String completion_time = "occurrence_completion_time";
    public static final String last_edit_time = "occurrence_last_edit_time";
    public static final String lats = "occurrence_latitudes_array";
    public static final String lngs = "occurrence_longitude_array";
    public static final String geoHash = "occurrence_geo_hash";
    public static final String archived = "occurrence_archived";
    public static final String imageIDCompleted = "occurrence_image_id_completed";
    public static final String dico = "occurrence_dico";
    public static final String county = "occurrence_county";
    public static final String district = "occurrence_district";
    public static final String address = "occurrence_address";
    public static final String validated = "occurrence_validated";
    public static final String checked = "occurrence_checked";
}
