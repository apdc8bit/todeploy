package apdc.tables.occurrence;

public class Reports {
    public static final String kind = "OccurrenceReports";//key = occurrence id
    public static final String count = "occurrence_count";
    public static final String type = "report_type";
    public static final String occurrenceKey = "occurrence_key";
    public static final String usersList = "occurrence_users_list";
    public static final String first_report_date = "occurrence_first_report_date";
    public static final String last_report_date = "occurrence_last_report_date";
}
