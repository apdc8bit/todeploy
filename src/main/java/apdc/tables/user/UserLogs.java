package apdc.tables.user;

public class UserLogs {

    public static final String kind = "UserLog";
    public static final String loginIP = "user_login_ip";
    public static final String loginHost = "user_login_host";
    public static final String userLatLong = "user_login_latlon";
    public static final String userCity = "user_login_city";
    public static final String userCountry = "user_login_country";
    public static final String userLoginTime = "user_login_time";
    public static final String userLoginStats = "user_stats_logins";
    public static final String userStatsFailed = "user_stats_failed";
    public static final String userStatsLast = "user_stats_last";

}
