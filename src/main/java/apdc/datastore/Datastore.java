package apdc.datastore;

import com.google.appengine.api.datastore.*;
import com.google.gson.Gson;

import java.util.Date;
import java.util.logging.Logger;

public class Datastore {
    private static final Logger LOG = Logger.getLogger(Datastore.class.getName());
    private static final DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
    private final Gson g = new Gson();

    public static EntityValues getFrom(Entity entity) {
        return new EntityValues(entity);
    }

    public static EntityValues getFrom(String kind, String key) {
        return new EntityValues(kind, key);
    }

    public static EntityValues getFrom(Key key) {
        return new EntityValues(key);
    }

    public static class EntityValues {
        public final Entity entity;

        private EntityValues(Entity entity) {
            this.entity = entity;
        }

        private EntityValues(String kind, String key) {
            this.entity = getEntity(kind, key);
        }

        private EntityValues(Key key) {
            this.entity = getEntity(key);
        }

        public Entity aSelf() {
            return this.entity;
        }

        public long aLong(String valueKey) {
            if (isNull(this.entity, valueKey))
                return -1L;

            try {
                return (long) this.entity.getProperty(valueKey);
            } catch (Exception e) {
                LOG.warning("Error getting value: " + valueKey);
                return -1L;
            }
        }

        public String aString(String valueKey) {
            if (isNull(this.entity, valueKey))
                return "";

            try {
                String tmp = (String) entity.getProperty(valueKey);
                if (isNull(tmp))
                    return "";
                return tmp;
            } catch (Exception e) {
                LOG.warning("Error getting value: " + valueKey);
                return "";
            }
        }

        public boolean aBoolean(String valueKey) {
            if (isNull(this.entity, valueKey))
                return false;

            try {
                return (boolean) this.entity.getProperty(valueKey);
            } catch (Exception e) {
                LOG.warning("Error getting value: " + valueKey);
                return false;
            }
        }

        public double aDouble(String valueKey) {
            if (isNull(this.entity, valueKey))
                return -1.0;

            try {
                return (double) this.entity.getProperty(valueKey);
            } catch (Exception e) {
                LOG.warning("Error getting value: " + valueKey);
                return -1.0;
            }
        }

        public Date aDate(String valueKey) {
            if (isNull(this.entity) || isNull(valueKey))
                return new Date();

            try {
                return (Date) this.entity.getProperty(valueKey);
            } catch (Exception e) {
                LOG.warning("Error getting value: " + valueKey);
                return new Date();
            }
        }
    }

    private static boolean isNull(Object... objs) {
        for (Object obj : objs) {
            if (obj == null) {
                LOG.warning("Null Values");
                return true;
            }
        }
        return false;
    }

    private static Entity getEntity(String kind, String key) {
        if (isNull(kind) || isNull(key)) {
            LOG.warning("Null Values: kind: " + kind + ", key: " + key);
            return null;
        }

        try {
            return datastore.get(KeyFactory.createKey(kind, key));
        } catch (EntityNotFoundException e) {
            LOG.info("Did not find '" + kind + "' " + key + "'");
            return null;
        }
    }

    private static Entity getEntity(Key key) {
        if (isNull(key)) {
            LOG.warning("Null Value: key: " + key);
            return null;
        }

        try {
            return datastore.get(key);
        } catch (EntityNotFoundException e) {
            LOG.info("Did not find '" + key.getNamespace() + "' " + key.getName() + "'");
            return null;
        }
    }

}
