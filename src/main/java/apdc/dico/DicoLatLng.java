package apdc.dico;

public class DicoLatLng {
    public final double lat;
    public final double lng;

    public DicoLatLng(double lat, double lng) {
        this.lat = lat;
        this.lng = lng;
    }
}
