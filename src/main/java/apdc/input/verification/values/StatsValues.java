package apdc.input.verification.values;

import apdc.firerisk.FireRiskStats;
import apdc.firerisk.FireRiskTableSingleton;
import apdc.output.info.BasicStatsInfo;
import apdc.output.info.BasicStatsInfo.Stats;
import apdc.output.info.CountyInfo;
import apdc.permissions.UserPermissions;
import apdc.resources.occurrences.Management;
import apdc.tables.entity.EntityStats;
import apdc.tables.occurrence.Occurrences;
import apdc.tables.occurrence.VotesCommentsCount;
import apdc.tables.user.Points;
import apdc.tables.user.User;
import apdc.tables.user.UserLogs;
import apdc.tables.worker.WorkerJobs;
import apdc.util.CalendarUtils;
import com.google.appengine.api.datastore.*;
import com.google.gson.Gson;

import java.util.*;
import java.util.logging.Logger;


public class StatsValues {
    private static final DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
    private static final Logger LOG = Logger.getLogger(Management.class.getName());
    private static final FetchOptions fo = FetchOptions.Builder.withDefaults();
    private static final Gson g = new Gson();

    public static void loadBasicStats() {
        BasicStatsInfo stats = new BasicStatsInfo();

        userStats(stats);

        occurrenceStats(stats);

        fireStats(stats);

        votesCommentsStats(stats);

        loginStats(stats);

        stats.submit();
    }

    private static void fireStats(BasicStatsInfo stats) {
        FireRiskTableSingleton frt = FireRiskTableSingleton.getInstance();
        FireRiskStats frs = frt.getFireRiskStats();

        stats.setStat(Stats.maxFireRisk, frs.maxF);
        stats.setStat(Stats.minFireRisk, frs.minF);
        stats.setStat(Stats.avgFireRisk, Math.round(frs.avgF));

        List<CountyInfo> atRisk = frt.getAtRiskCounties();

        List<EmbeddedEntity> svList = new LinkedList<>();
        for (CountyInfo info : atRisk) {
            EmbeddedEntity sv = new EmbeddedEntity();
            sv.setIndexedProperty("county", info.county);
            sv.setIndexedProperty("dico", info.dico);
            svList.add(sv);
            LOG.warning("AAA " + info.county);
        }

        LOG.warning("C " + atRisk.size());

        stats.setStatEE(Stats.atRiskCounties, svList);
    }

    private static void occurrenceStats(BasicStatsInfo stats) {
        Query totalOccs = new Query(Occurrences.kind).setKeysOnly();
        stats.setStat(Stats.totalNumberOccurrences, datastore.prepare(totalOccs).countEntities(fo));

        totalOccs = new Query(Occurrences.kind).setKeysOnly();
        Query.Filter occsOnHold = new Query.FilterPredicate(Occurrences.status, Query.FilterOperator.EQUAL, OccurrenceValues.STATUS.on_hold.name());
        stats.setStat(Stats.occurrencesOnHoldTotal, datastore.prepare(totalOccs.setFilter(occsOnHold)).countEntities(fo));

        totalOccs = new Query(Occurrences.kind).setKeysOnly();
        Query.Filter occsInProgress = new Query.FilterPredicate(Occurrences.status, Query.FilterOperator.EQUAL, OccurrenceValues.STATUS.in_progress.name());
        stats.setStat(Stats.occurrencesInProgressTotal, datastore.prepare(totalOccs.setFilter(occsInProgress)).countEntities(fo));

        totalOccs = new Query(Occurrences.kind).setKeysOnly();
        Query.Filter occsCompleted = new Query.FilterPredicate(Occurrences.status, Query.FilterOperator.EQUAL, OccurrenceValues.STATUS.completed.name());
        stats.setStat(Stats.occurrencesCompletedTotal, datastore.prepare(totalOccs.setFilter(occsCompleted)).countEntities(fo));

        totalOccs = new Query(Occurrences.kind).setKeysOnly();
        Query.Filter occsLastWeek = new Query.FilterPredicate(Occurrences.creation_time, Query.FilterOperator.GREATER_THAN_OR_EQUAL, CalendarUtils.addDays(new Date(), -7));
        stats.setStat(Stats.occurrencesCreatedLastWeek, datastore.prepare(totalOccs.setFilter(occsLastWeek)).countEntities(fo));

        totalOccs = new Query(Occurrences.kind).setKeysOnly();
        Query.Filter occsYesterday = new Query.FilterPredicate(Occurrences.creation_time, Query.FilterOperator.GREATER_THAN_OR_EQUAL, CalendarUtils.addDays(new Date(), -1));
        stats.setStat(Stats.occurrencesCreatedYesterday, datastore.prepare(totalOccs.setFilter(occsYesterday)).countEntities(fo));

        totalOccs = new Query(WorkerJobs.kind).setKeysOnly();
        Query.Filter openedLastWeek = new Query.FilterPredicate(WorkerJobs.added_time, Query.FilterOperator.GREATER_THAN_OR_EQUAL, CalendarUtils.addDays(new Date(), -7));
        stats.setStat(Stats.occurrencesStartedLastWeek, datastore.prepare(totalOccs.setFilter(openedLastWeek)).countEntities(fo));

        totalOccs = new Query(WorkerJobs.kind).setKeysOnly();
        Query.Filter resolvedLastWeek = new Query.FilterPredicate(WorkerJobs.finish_time, Query.FilterOperator.GREATER_THAN_OR_EQUAL, CalendarUtils.addDays(new Date(), -7));
        stats.setStat(Stats.occurrencesCompletedLastWeek, datastore.prepare(totalOccs.setFilter(resolvedLastWeek)).countEntities(fo));

        //[START TOP 5 OCCS LAST WEEK]
        totalOccs = new Query(VotesCommentsCount.kind);
        Query.Filter lastWeekFilter = new Query.FilterPredicate(VotesCommentsCount.occurrenceCreationTime, Query.FilterOperator.GREATER_THAN_OR_EQUAL, CalendarUtils.addDays(new Date(), -7));
        List<Entity> lastWeek = datastore.prepare(totalOccs
                .setFilter(lastWeekFilter))
                .asList(FetchOptions.Builder.withDefaults());


        LOG.info("Occ Votes Week: " + lastWeek.size());
        Collections.sort(lastWeek, new VotesCommentsComparator());

        List<Key> infoOcc = new LinkedList<>();
        int counter = 0;
        for (Entity v : lastWeek) {
            if (counter == 5)
                break;
            counter++;
            infoOcc.add((Key) v.getProperty(VotesCommentsCount.occurrenceKey));
        }
        stats.setStat(Stats.topOccurrencesLast5Days, infoOcc);
        //[END TOP 5 OCCS LAST WEEK]

        //[START AVG JOB COMPLETION TIME]
        long avgTotalTime = 0L, avgResponseTime = 0L;
        totalOccs = new Query(EntityStats.kind);
        int counterJ = 0;
        List<Entity> entities = datastore.prepare(totalOccs).asList(FetchOptions.Builder.withDefaults());
        for (Entity e : entities) {
            Long start = (Long) e.getProperty(EntityStats.avgCompletionTime);
            Long resp = (Long) e.getProperty(EntityStats.avgResponseTime);
            counterJ++;

            if (start != null) {
                if (avgTotalTime == 0)
                    avgTotalTime = start;
                else {
                    avgTotalTime = getAvgFromAvg(avgTotalTime, start, counterJ - 1);
                }
            }
            if (resp != null) {
                if (avgResponseTime == 0)
                    avgResponseTime = resp;
                else {
                    avgResponseTime = getAvgFromAvg(avgResponseTime, resp, counterJ - 1);
                }
            }
        }

        stats.setStat(Stats.averageOccurrenceCompletionTime, avgTotalTime);
        stats.setStat(Stats.averageOccurrenceResponseTime, avgResponseTime);
        //[STOP AVG JOB COMPLETION TIME]
    }

    private static long getAvgFromAvg(long avg, long newValue, long numbBefore) {
        return ((avg * numbBefore) + newValue) / (numbBefore + 1);
    }

    static class VotesCommentsComparator implements Comparator<Entity> {
        @Override
        public int compare(Entity a, Entity b) {
            long votesDif = (long) a.getProperty(VotesCommentsCount.likeCount) - (long) b.getProperty(VotesCommentsCount.likeCount);
            long commentDif = (long) a.getProperty(VotesCommentsCount.commentCount) - (long) b.getProperty(VotesCommentsCount.commentCount);

            if (votesDif == 0) {
                if (commentDif == 0)
                    return 0;
                else if (commentDif > 0)
                    return 1;
                else
                    return -1;
            } else if (votesDif > 0)
                return 1;
            else
                return -1;
        }
    }

    private static void userStats(BasicStatsInfo stats) {
        Query totalUser = new Query(User.kind).setKeysOnly();
        stats.setStat(Stats.totalNumberUsers, datastore.prepare(totalUser).countEntities(fo));

        totalUser = new Query(User.kind).setKeysOnly();
        Query.Filter basicUserFilter = new Query.FilterPredicate(User.role, Query.FilterOperator.EQUAL, UserPermissions.ROLE.u_basic.name());
        stats.setStat(Stats.numberBasicUsers, datastore.prepare(totalUser.setFilter(basicUserFilter)).countEntities(fo));

        totalUser = new Query(User.kind).setKeysOnly();
        Query.Filter workerUserFilter = new Query.FilterPredicate(User.role, Query.FilterOperator.EQUAL, UserPermissions.ROLE.u_worker.name());
        stats.setStat(Stats.numberWorkersUsers, datastore.prepare(totalUser.setFilter(workerUserFilter)).countEntities(fo));

        totalUser = new Query(User.kind).setKeysOnly();
        Query.Filter entityUserFilter = new Query.FilterPredicate(User.role, Query.FilterOperator.EQUAL, UserPermissions.ROLE.u_entity.name());
        stats.setStat(Stats.numberEntitiesUsers, datastore.prepare(totalUser.setFilter(entityUserFilter)).countEntities(fo));

        //[START : USER MOST OCCURRENCES]
        long topNumbOccs = Long.MIN_VALUE;
        Key topUser = null;
        totalUser = new Query(User.kind).setFilter(new Query.FilterPredicate(User.role, Query.FilterOperator.EQUAL, UserPermissions.ROLE.u_basic.name())).setKeysOnly();
        List<Entity> users = datastore.prepare(totalUser).asList(fo);
        for (Entity user : users) {
            Query occCount = new Query(Occurrences.kind).setKeysOnly().setAncestor(user.getKey());
            long tmp = datastore.prepare(occCount).countEntities(fo);
            if (tmp > topNumbOccs) {
                topNumbOccs = tmp;
                topUser = user.getKey();
            }
        }
        stats.setStat(Stats.userMostOccurrences, topUser);
        //[END : USER MOST OCCURRENCES]

        //[START : 5 USERS HIGHEST LEVEL]
        List<Key> topUsers = new LinkedList<>();
        FetchOptions foa = FetchOptions.Builder.withLimit(5);
        totalUser = new Query(Points.kind);
        List<Entity> points = datastore.prepare(totalUser
                .addSort(Points.level, Query.SortDirection.DESCENDING)
                .addSort(Points.points, Query.SortDirection.DESCENDING)).asList(foa);
        for (Entity point : points) {
            topUsers.add(KeyFactory.createKey(User.kind, point.getKey().getName()));

        }
        stats.setStat(Stats.top5UsersByLevel, topUsers);
        //[END : 5 USERS HIGHEST LEVEL]

        totalUser = new Query(User.kind)
                .addProjection(new PropertyProjection(User.creation_time, Date.class));
        List<Entity> registered = datastore.prepare(totalUser).asList(fo);
        Date today = new Date();
        long usersRegisteredYesterday = 0L, userRegisteredLastWeek = 0L, getUsersRegisteredLastMonth = 0L;
        for (Entity r : registered) {
            Date loginTime = (Date) r.getProperty(User.creation_time);

            if (CalendarUtils.addDays(today, -1).before(loginTime)) {
                //last Day
                usersRegisteredYesterday++;
            }
            if (CalendarUtils.addDays(today, -7).before(loginTime)) {
                //last Week
                userRegisteredLastWeek++;
            }
            if (CalendarUtils.addDays(today, -30).before(loginTime)) {
                //last Month
                getUsersRegisteredLastMonth++;
            }

        }

        stats.setStat(Stats.usersRegisteredYesterday, usersRegisteredYesterday);
        stats.setStat(Stats.userRegisteredLastWeek, userRegisteredLastWeek);
        stats.setStat(Stats.getUsersRegisteredLastMonth, getUsersRegisteredLastMonth);
    }

    private static void votesCommentsStats(BasicStatsInfo stats) {
        Query totalStats = new Query(VotesCommentsCount.kind);
        List<Entity> counts = datastore.prepare(totalStats).asList(fo);
        Date today = new Date();
        long totalComments = 0L, totalVotes = 0L,
                weekComments = 0L, weekVotes = 0L;
        for (Entity count : counts) {
            long v = (long) count.getProperty(VotesCommentsCount.likeCount);
            long c = (long) count.getProperty(VotesCommentsCount.commentCount);

            totalComments += c;
            totalVotes += v;
            Date lastI = (Date) count.getProperty(VotesCommentsCount.lastInteractionTime);

            if (CalendarUtils.addDays(lastI, 7).after(today)) {
                //Was last week
                weekComments += c;
                weekVotes += v;
            }
        }
        stats.setStat(Stats.totalVotes, totalVotes);
        stats.setStat(Stats.totalComments, totalComments);
        stats.setStat(Stats.votesLastWeek, weekVotes);
        stats.setStat(Stats.commentsLasWeek, weekComments);
    }

    private static void loginStats(BasicStatsInfo stats) {
        Query totalStats = new Query(UserLogs.kind)
                .setFilter(new Query.FilterPredicate(UserLogs.userLoginTime, Query.FilterOperator.GREATER_THAN_OR_EQUAL, CalendarUtils.addDays(new Date(), -7)));

        FetchOptions fo = FetchOptions.Builder.withDefaults();
        long loginsLastWeek = datastore.prepare(totalStats).countEntities(fo);

        totalStats = new Query(UserLogs.kind)
                .setFilter(new Query.FilterPredicate(UserLogs.userLoginTime, Query.FilterOperator.GREATER_THAN_OR_EQUAL, CalendarUtils.addDays(new Date(), -1)));

        long loginsLastDay = datastore.prepare(totalStats).countEntities(fo);

        stats.setStat(Stats.loginsLastDay, loginsLastDay);
        stats.setStat(Stats.loginsLastWeek, loginsLastWeek);
    }
}
