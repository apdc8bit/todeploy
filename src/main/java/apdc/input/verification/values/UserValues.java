package apdc.input.verification.values;

import apdc.datastore.Datastore;
import apdc.dico.DicoData;
import apdc.dico.DicoLatLng;
import apdc.dico.DicoTableSingleton;
import apdc.gamification.PointSystem;
import apdc.gamification.Values;
import apdc.input.data.admin.RegisterBasicData;
import apdc.input.data.basic.AdditionalRegistrationData;
import apdc.input.data.basic.DeleteInfoData;
import apdc.input.data.basic.InitialRegistrationData;
import apdc.input.data.basic.LoginData;
import apdc.input.data.entity.RegisterWorkerData;
import apdc.input.verification.utils.Device;
import apdc.input.verification.utils.Reply;
import apdc.output.info.BasicUserInfo;
import apdc.output.info.TokenSessionInfo;
import apdc.output.messages.OutputStatus;
import apdc.permissions.UserPermissions;
import apdc.tables.user.Stats;
import apdc.tables.user.User;
import apdc.tables.user.UserLogs;
import apdc.util.CalendarUtils;
import apdc.util.email.EmailResource;
import apdc.util.keys.KeyGenerator;
import apdc.util.keys.KeyInterface;
import com.google.appengine.api.datastore.*;
import com.google.appengine.repackaged.org.apache.commons.codec.digest.DigestUtils;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.UUID;
import java.util.logging.Logger;

import static apdc.resources.api.Base.g;

public class UserValues {
    private static final Logger LOG = Logger.getLogger(UserValues.class.getName());
    private static final DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();

    public enum INFO {
        ALL, BASIC, NONE
    }

    public static BasicUserInfo getBasicUserInfo(Entity user) {
        String imageID, username;
        Number points = null, level = null, next_level_points = null, curr_level_points = null;

        username = user.getKey().getName();
        imageID = (String) user.getProperty(User.imageID);

        if (Datastore.getFrom(user).aString(User.role).equals(UserPermissions.ROLE.u_basic.name())) {
            Values values = PointSystem.getPointsAndLevel(username);
            points = values.getPoints();
            level = values.getLevel();
            next_level_points = PointSystem.pointsForLevelUp(((long) level) + 1);
            curr_level_points = PointSystem.pointsForLevelUp((long) level);
        }

        return new BasicUserInfo(username, imageID, points, level, next_level_points, curr_level_points);
    }

    public static INFO getInfo(String value) {
        try {
            return INFO.valueOf(value.toUpperCase());
        } catch (IllegalArgumentException | NullPointerException e) {
            return INFO.NONE;
        }
    }

    //Creates a new basic user entity
    private static Entity addUser(InitialRegistrationData data) {
        //Entity user = new Entity(User.kind, data.username.toLowerCase());
        Entity user = new Entity(User.kind, data.username);

        user.setProperty(User.email, data.email);
        //user.setProperty(User.email, data.email.toLowerCase());
        user.setUnindexedProperty(User.password, DigestUtils.sha512Hex(data.password));
        user.setProperty(User.for_removal, false);
        Date date = new Date();
        user.setProperty(User.creation_time, date);
        user.setProperty(User.last_edit_time, date);

        return user;
    }

    //Creates a new complex user entity
    private static Entity addUserFull(RegisterWorkerData data) {
        Entity user = addUser(data);

        user.setProperty(User.name, data.name);
        user.setProperty(User.phone_number, data.phoneNumber);
        user.setProperty(User.zip_code, data.zipcode);
        user.setProperty(User.nif, data.nif);
        if (!data.imageID.isEmpty())
            user.setProperty(User.imageID, data.imageID);
        user.setProperty(User.address, data.address);

        return user;
    }

    //Creates a new user entity and sets the properties
    public static Entity addUserBasic(InitialRegistrationData data) {
        Entity user = addUser(data);

        user.setProperty(User.role, UserPermissions.ROLE.u_basic.name());
        user.setProperty(User.valid, false);

        return user;
    }

    //Creates a new User Worker and sets the properties
    public static Entity addUserWorker(RegisterWorkerData data) {
        Entity user = addUserFull(data);

        user.setProperty(User.role, UserPermissions.ROLE.u_worker.name());
        user.setProperty(User.valid, true);

        return user;
    }

    //Creates a new User Entity and sets the properties
    public static Entity addUserEntity(RegisterWorkerData data) {
        Entity user = addUserFull(data);

        user.setProperty(User.role, UserPermissions.ROLE.u_entity.name());//u_basic
        user.setProperty(User.valid, false);

        DicoLatLng latLong = EntityValues.getDicoFromAddress(data.address, data.zipcode);
        if (latLong != null) {
            DicoTableSingleton dt = DicoTableSingleton.getInstance();
            DicoData dicoData = dt.getDicoFromCoords(latLong.lat, latLong.lng);
            user.setProperty(User.dico, dicoData.dico);
            user.setProperty(User.county, dicoData.county);
            user.setProperty(User.district, dicoData.district);
            user.setProperty(User.lat, latLong.lat);
            user.setProperty(User.lng, latLong.lng);
            user.setProperty(User.volunteer, data.isVolunteer);
        }
        return user;
    }

    //Creates a new User Admin and sets the properties
    public static Entity addUserAdmin(InitialRegistrationData data) {
        Entity user = addUserBasic(data);

        user.setProperty(User.role, UserPermissions.ROLE.u_admin.name());
        user.setProperty(User.valid, true);

        return user;
    }

    public static Entity editUser(Entity user, RegisterBasicData data) {

        //Grade A+++ code
        AdditionalRegistrationData newData = new AdditionalRegistrationData();
        newData.address = data.address;
        newData.imageID = data.imageID;
        newData.name = data.name;
        newData.zipcode = data.zipcode;
        newData.phoneNumber = data.phoneNumber;

        return editUser(user, newData);
    }

    //Edits the properties of the given user
    public static Entity editUser(Entity user, AdditionalRegistrationData data) {

        if (!data.name.isEmpty()) {
            user.setProperty(User.name, data.name);
        }
        if (data.phoneNumber != 0) {
            user.setProperty(User.phone_number, data.phoneNumber);
        }
        if (!data.zipcode.isEmpty()) {
            user.setProperty(User.zip_code, data.zipcode);
        }
        if (data.nif != 0) {
            user.setProperty(User.nif, data.nif);
        }
        if (!data.imageID.isEmpty()) {
            String image = (String) user.getProperty(User.imageID);
            // if (image != null)
            //GcsServlet.deleteFile(image, GcsServlet.FILE.user_image);

            user.setProperty(User.imageID, data.imageID);
        }
        if (!data.address.isEmpty()) {
            user.setProperty(User.address, data.address);
        }

        user.setProperty(User.last_edit_time, new Date());

        return user;
    }

    //Deletes the properties of a given user
    public static void deleteInfoUser(Entity user, DeleteInfoData data) {

        if (data.name) {
            user.removeProperty(User.name);
        }
        if (data.phoneNumber) {
            user.removeProperty(User.phone_number);
        }
        if (data.zipcode) {
            user.removeProperty(User.zip_code);
        }
        if (data.nif) {
            user.removeProperty(User.nif);
        }
        if (data.imageID) {
            user.removeProperty(User.imageID);
        }
        if (data.address) {
            user.removeProperty(User.address);
        }

        user.setProperty(User.last_edit_time, new Date());
    }

    //Check if email exists
    public static Reply emailAvailable(String email) {
        //email = email.toLowerCase();

        Query.Filter propertyFilter = new Query.FilterPredicate(User.email, Query.FilterOperator.EQUAL, email);
        Query ctrQuery = new Query(User.kind).setFilter(propertyFilter);
        List<Entity> results = datastore.prepare(ctrQuery).asList(FetchOptions.Builder.withDefaults());

        if (!results.isEmpty()) {
            LOG.info("Email exists");
            return new Reply("", null, OutputStatus.EMAIL_EXISTS, Response.Status.CONFLICT);
        }

        LOG.info("Email does not exist");
        return new Reply("", null, OutputStatus.EMAIL_NOT_EXIST, Response.Status.OK);
    }

    //Check if username exists
    public static Reply usernameAvailable(String username) {
        try {
            Key userKey = KeyFactory.createKey(User.kind, username);
            datastore.get(userKey);

            LOG.info("Username exists");
            return new Reply("", null, OutputStatus.USERNAME_EXISTS, Response.Status.CONFLICT);
        } catch (EntityNotFoundException e) {
            LOG.info("Username does not exist");
            return new Reply("", null, OutputStatus.USERNAME_NOT_EXIST, Response.Status.OK);
        }
    }


    public static Reply comparePasswords(String user_password, String other_password) {
        if (user_password.equals(DigestUtils.sha512Hex(other_password))) {
            LOG.info("Passwords match");
            return new Reply("", null, OutputStatus.PASSWORDS_MATCH, Response.Status.OK);
        }

        LOG.warning("Passwords do not match!");
        return new Reply("", null, OutputStatus.PASSWORDS_DO_NOT_MATCH, Response.Status.FORBIDDEN);
    }

    public static Entity executeLoginUpdates(Entity user, Date lastLogin, String latLon, String deviceLogin, String deviceToken, String loginIP, String loginCity, String email) {
        boolean userChanged = false;

        //Check Login IP
        if (UserPermissions.isLoginFromAnotherLocation(user.getKey(), loginIP)) {
            LOG.warning("User login from a new ip");
            EmailResource.sendLoginNewIP(user.getKey().getName(), email, loginCity);
        }

        //Check if login is in a new day
        if (UserPermissions.isLoginInNewDay(lastLogin)) {
            LOG.info("Login in new day");
            PointSystem.addPoints(PointSystem.ACTIONS.login, user.getKey().getName());
        }

        //If user was marked for removal, cancel removal
        boolean for_removal = Datastore.getFrom(user).aBoolean(User.for_removal);//(boolean) user.getProperty(User.for_removal);
        if (for_removal) {
            user.setProperty(User.last_edit_time, new Date());
            user.setProperty(User.for_removal, false);
            userChanged = true;
        }

        //Set user dico
        String role = (String) user.getProperty(User.role);
        if (role.equals(UserPermissions.ROLE.u_basic.name())) {
            try {
                double lat = Double.parseDouble(latLon.split(",")[0]);
                double lng = Double.parseDouble(latLon.split(",")[1]);
                DicoData dico = OccurrenceValues.getDico(lat, lng);
                String oldDico = Datastore.getFrom(user).aString(User.dico);//(String) user.getProperty(User.dico);
                if (!oldDico.equals(dico.dico)) {
                    user.setProperty(User.dico, dico.dico);
                    user.setProperty(User.county, dico.county);
                    user.setProperty(User.district, dico.district);
                    userChanged = true;
                }
            } catch (Exception e) {
                //Catch all the things
                LOG.warning("Error setting user dico " + e.getMessage());
            }
        }

        //Update device ID
        Entity userTMP = NotificationValues.checkDeviceID(deviceLogin, user, deviceToken);
        if (userTMP != null) {
            user = userTMP;
            userChanged = true;
        }

        if (userChanged)
            return user;
        else
            return null;
    }

    private static Reply login(LoginData data, HttpServletRequest request, HttpHeaders headers, String device, boolean admin) {
        //Check Login Country
        String country = headers.getHeaderString("X-AppEngine-Country");
        if (!UserPermissions.isLoginFromAuthorizedCountry(country)) {
            return new Reply("", null, OutputStatus.UNAUTHORIZED_COUNTRY, Response.Status.FORBIDDEN);
        }

        TransactionOptions options = TransactionOptions.Builder.withXG(true);
        Transaction txn = datastore.beginTransaction(options);

        try {
            //Check if username exists
            Key userKey = KeyFactory.createKey(User.kind, data.username);
            Entity user = datastore.get(userKey);

            String role = Datastore.getFrom(user).aString(User.role);
            String email = Datastore.getFrom(user).aString(User.email);
            boolean valid = Datastore.getFrom(user).aBoolean(User.valid);

            if (!valid && role.equals(UserPermissions.ROLE.u_entity.name())) {
                return new Reply("", null, OutputStatus.ENTITY_NOT_VALID, Response.Status.FORBIDDEN);
            }

            if (admin == UserPermissions.ROLE.u_admin.name().equals(role)) {
                LOG.warning("Roles do not match");
                txn.rollback();
                return new Reply("", null, OutputStatus.USER_ROLE_NOT_ALLOWED, Response.Status.FORBIDDEN);
            }
            // else if (admin ) return forbiden for entity

            //Check User Timeout
            if (UserPermissions.isUserInTimeout(user)) {
                txn.rollback();
                return new Reply("", null, OutputStatus.USER_TIMEOUT, Response.Status.FORBIDDEN);
            }

            //Stats
            Query ctrQuery = new Query(Stats.kind).setAncestor(user.getKey());
            List<Entity> results = datastore.prepare(ctrQuery).asList(FetchOptions.Builder.withDefaults());
            Entity user_stats;
            if (results.isEmpty()) {
                user_stats = new Entity(Stats.kind, user.getKey());
                user_stats.setProperty(Stats.login, 0L);
                user_stats.setProperty(Stats.failed, 0L);
            } else {
                user_stats = results.get(0);
            }

            //Additional Checks for Admin
            if (role.equals(UserPermissions.ROLE.u_admin.name())) {
                long numb_failed_login = Datastore.getFrom(user_stats).aLong(Stats.failed); //(long) user_stats.getProperty(Stats.failed);
                if (!UserPermissions.verifyAdminLogin(numb_failed_login)) {
                    LOG.warning("Admin login is unauthorized");
                    //Send email
                    EmailResource.sendAdminAccountCompromised(user.getKey().getName(), email);
                    //Set timeout
                    user_stats.setProperty(Stats.failed, 0);
                    user.setProperty(User.timeout, CalendarUtils.addDays(new Date(), 1));
                    datastore.put(txn, Arrays.asList(user_stats, user));
                    txn.commit();
                    return new Reply("", null, OutputStatus.ADMIN_FAILED, Response.Status.FORBIDDEN);
                }
            }

            //Compare passwords
            String hashedPWD = Datastore.getFrom(user).aString(User.password);//(String) user.getProperty(User.password);
            if (hashedPWD.equals(DigestUtils.sha512Hex(data.password))) {
                //Passwords match
                Date last_login;

                // Construct the logs
                Entity user_logs = new Entity(UserLogs.kind, user.getKey());
                user_logs.setProperty(UserLogs.loginIP, request.getRemoteAddr());
                user_logs.setProperty(UserLogs.loginHost, request.getRemoteHost());
                String latLon = headers.getHeaderString("X-AppEngine-CityLatLong");
                user_logs.setProperty(UserLogs.userLatLong, latLon);
                user_logs.setProperty(UserLogs.userCity, headers.getHeaderString("X-AppEngine-City"));
                user_logs.setProperty(UserLogs.userCountry, country);
                last_login = Datastore.getFrom(user_logs).aDate(UserLogs.userLoginTime);//(Date) user_logs.getProperty(UserLogs.userLoginTime);
                user_logs.setProperty(UserLogs.userLoginTime, new Date());

                // Get the user statistics and updates it
                long numbLogin = Datastore.getFrom(user_stats).aLong(Stats.login);
                user_stats.setProperty(UserLogs.userLoginStats, 1L + numbLogin);
                user_stats.setProperty(UserLogs.userStatsFailed, 0L);
                user_stats.setProperty(UserLogs.userStatsLast, new Date());

                // Batch operation
                List<Entity> logs = Arrays.asList(user_logs, user_stats);
                datastore.put(txn, logs);

                //Generate token
                String session = UUID.randomUUID().toString();
                String token;
                if (Device.confirmMobile(device))
                    token = KeyGenerator.getCypher(data.username, session, KeyInterface.Type.MOBILE);
                else
                    token = KeyGenerator.getCypher(data.username, session, KeyInterface.Type.TOKEN);
                TokenSessionInfo tokeInfo = new TokenSessionInfo(user, token, session);

                //Only save user info if changes happened
                String loginIP = request.getRemoteAddr();
                String loginCity = headers.getHeaderString("X-AppEngine-City");
                user = executeLoginUpdates(user, last_login, latLon, device, data.deviceID, loginIP, loginCity, email);
                if (user != null)
                    datastore.put(txn, user);

                LOG.info(data.username + " was logged in successfully");

                txn.commit();
                return new Reply(role, null, g.toJson(tokeInfo), Response.Status.OK);
            } else {
                // Incorrect password
                long numbFailed = Datastore.getFrom(user_stats).aLong(Stats.failed);
                user_stats.setProperty(Stats.failed, 1L + numbFailed);
                datastore.put(txn, user_stats);

                LOG.warning("Wrong password for username: " + data.username);

                txn.commit();
                return new Reply("", null, OutputStatus.USER_PASS_INC, Response.Status.FORBIDDEN);
            }
        } catch (EntityNotFoundException e) {
            //username does not exist
            LOG.warning("Username does not exist: " + data.username);

            txn.rollback();
            return new Reply("", null, OutputStatus.USER_PASS_INC, Response.Status.FORBIDDEN);
        } finally {
            if (txn != null && txn.isActive()) {
                txn.rollback();
                LOG.severe("SERVER ERROR");
            }
        }
    }

    public static Reply loginAdmin(LoginData data, HttpServletRequest request, HttpHeaders headers, String device) {
        Reply reply = login(data, request, headers, device, false);

        if (!reply.ok())
            return reply;

        return reply;
    }

    public static Reply loginBasic(LoginData data, HttpServletRequest request, HttpHeaders headers, String device) {
        Reply reply = login(data, request, headers, device, true);

        if (!reply.ok())
            return reply;

        return reply;
    }
}
