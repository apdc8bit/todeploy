package apdc.input.verification.values;

import apdc.dico.DicoInfo;
import apdc.dico.DicoTableSingleton;
import apdc.input.verification.utils.Device;
import apdc.tables.entity.Events;
import apdc.tables.occurrence.Occurrences;
import apdc.tables.user.User;
import apdc.util.Notifications;
import com.google.appengine.api.datastore.*;

import java.util.LinkedList;
import java.util.List;
import java.util.logging.Logger;

public class NotificationValues {
    private static final Logger LOG = Logger.getLogger(NotificationValues.class.getName());
    private static final DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();


    public static Entity checkDeviceID(String device, Entity user, String deviceID) {
        boolean changed = false;
        if (deviceID != null && !deviceID.isEmpty()) {
            if (Device.confirmWeb(device)) {
                String oldDeviceID = (String) user.getProperty(User.webDeviceID);

                if (oldDeviceID == null || !oldDeviceID.equals(deviceID)) {
                    LOG.info("New Web Device ID: " + deviceID);
                    user.setProperty(User.webDeviceID, deviceID);
                    changed = true;
                }

            } else if (Device.confirmMobile(device)) {
                String oldDeviceID = (String) user.getProperty(User.mobileDeviceID);

                if (oldDeviceID == null || !oldDeviceID.equals(deviceID)) {
                    LOG.info("New Mobile Device ID: " + deviceID);
                    user.setProperty(User.mobileDeviceID, deviceID);
                    changed = true;
                }
            }
        }
        if (changed)
            return user;
        else
            return null;
    }

    private static void sendToUser(String username, String title, String body) {
        try {
            Entity user = datastore.get(KeyFactory.createKey(User.kind, username));
            String mobileID = (String) user.getProperty(User.mobileDeviceID);
            String webID = (String) user.getProperty(User.webDeviceID);

            if (mobileID != null) {
                List<String> userIDs = new LinkedList<>();
                userIDs.add(mobileID);
                Notifications.sendtoUserID(userIDs, title, body);
            } else
                LOG.info("No mobile device id found");
            if (webID != null) {
                List<String> userIDs = new LinkedList<>();
                userIDs.add(webID);
                Notifications.sendtoUserID(userIDs, title, body);
            } else
                LOG.info("No web device id found");

        } catch (EntityNotFoundException e) {
            LOG.warning("User not found");
        }
    }

    public static void occurrenceStateNotification(Entity occ) {
        String tag = "" + occ.getKey().getId();
        String occTitle = (String) occ.getProperty(Occurrences.title);
        String occStatus = (String) occ.getProperty(Occurrences.status);
        String status = OccurrenceValues.getStatus(occStatus);
        String statusName = "";
        String username = occ.getKey().getParent().getName();

        if (status.equals(OccurrenceValues.STATUS.in_progress.name()))
            statusName = "em tratamento";
        else if (status.equals(OccurrenceValues.STATUS.completed.name()))
            statusName = "terminada!";

        Notifications.sendToTag("occID", tag, "Ocorrência Atualizada", "A tua Ocorrência '" + occTitle + "' está agora " + statusName);

        sendToUser(username, "Ocorrência Atualizada", "A Ocorrẽncia '" + occTitle + "' está agora " + statusName);
    }

    public static void occurrenceCommentNotification(Entity occ, String owner, String userCom, String comment) {
        String occTitle = (String) occ.getProperty(Occurrences.title);

        sendToUser(owner, "Novo comentário", "A tua Ocorrência '" + occTitle + "' recebeu um novo comentário de " + userCom + "\n'" + comment + "'");
    }

    public static void workerNewJob(Entity occ, String workerUser) {
        String occTitle = (String) occ.getProperty(Occurrences.title);

        sendToUser(workerUser, "Tens um novo trabalho", "Novo trabalho na '" + occTitle + "'");
    }

    public static void workerConfirmedJob(Entity occ, String workerUser) {
        String occTitle = (String) occ.getProperty(Occurrences.title);

        sendToUser(workerUser, "O Trabalho foi aceite", "O Trabalho na ocorrencia '" + occTitle + "' foi aceite");
    }

    public static void entityWorkerJobFinished(Entity occ, String entityName) {
        String occTitle = (String) occ.getProperty(Occurrences.title);

        sendToUser(entityName, "A ocorrencia está completa", "A ocorrencia '" + occTitle + "' esta completa");
    }

    public static void userNearEvent(Entity event, String workername) {
        String occTitle = (String) event.getProperty(Events.title);
        double lat = (double) event.getProperty(Events.lat);
        double lng = (double) event.getProperty(Events.lng);

        Notifications.sendtoLocation(lat, lng, 25000, "Novo evento perto de ti",
                "O evento '" + occTitle + "' foi criado perto de ti, com o trabalhador " + workername, false);
    }

    public static void userNearRisk(int risk, String dico) {
        DicoInfo info = DicoTableSingleton.getInstance().getInfoFromDico(dico);
        if (info == null)
            return;


        double lat = info.latitude;
        double lng = info.latitude;

        Notifications.sendtoLocation(lat, lng, 25000, "Alerta de Incêndio",
                "O risco de incêndio em " + info.local + " está a " + risk, true);
    }

    public static void userLevelUP(String username, String title, String body) {
        sendToUser(username, title, body);
    }

}
