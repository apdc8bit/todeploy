package apdc.input.verification.utils;

import com.google.appengine.api.datastore.Entity;

import javax.ws.rs.core.Response;

public class Reply {
    private final boolean ok;
    private final String username;
    private final Entity user;
    private final String jsonResponse;
    private final int statusCode;

    public Reply(String username, Entity user, String jsonResponse, Response.StatusType statusCode) {
        this.ok = statusCode.getStatusCode() == 200;
        this.username = username;
        this.user = user;
        this.jsonResponse = jsonResponse;
        this.statusCode = statusCode.getStatusCode();
    }

    public boolean ok() {
        return ok;
    }

    public String getUsername() {
        return username;
    }

    public Entity getUser() {
        return user;
    }

    public String getResponse() {
        return jsonResponse;
    }

    public int getStatus() {
        return statusCode;
    }


}
