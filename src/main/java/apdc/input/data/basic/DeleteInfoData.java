package apdc.input.data.basic;

import apdc.input.data.InputDataInterface;

public class DeleteInfoData implements InputDataInterface {
    public boolean address;
    public boolean name;
    public boolean nif;
    public boolean zipcode;
    public boolean phoneNumber;
    public boolean imageID;

    @Override
    public boolean validateData() {
        return true;
    }
}
