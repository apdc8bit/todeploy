package apdc.input.data.basic;

import apdc.input.data.InputDataInterface;
import apdc.input.verification.utils.InputDataVerification;

public class PasswordRecoveryData implements InputDataInterface {
    public String key;
    public String new_password;
    public String new_password_conf;

    public PasswordRecoveryData() {
    }

    @Override
    public boolean validateData() {
        key = key == null ? "" : key;
        new_password = new_password == null ? "" : new_password;
        new_password_conf = new_password_conf == null ? "" : new_password_conf;

        return !key.isEmpty() && new_password.equals(new_password_conf) &&
                InputDataVerification.validateText(new_password, InputDataVerification.TYPE.PASSWORD) &&
                InputDataVerification.validateText(new_password_conf, InputDataVerification.TYPE.PASSWORD);
    }

}
