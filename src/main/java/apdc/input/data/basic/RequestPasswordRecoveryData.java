package apdc.input.data.basic;

import apdc.input.data.InputDataInterface;
import apdc.input.verification.utils.InputDataVerification;

public class RequestPasswordRecoveryData implements InputDataInterface {
    public String username;
    public String email;

    public RequestPasswordRecoveryData() {
    }

    @Override
    public boolean validateData() {
        username = username == null ? "" : username.toLowerCase();
        email = email == null ? "" : email;

        return InputDataVerification.validateText(email, InputDataVerification.TYPE.EMAIL) &&
                InputDataVerification.validateText(username, InputDataVerification.TYPE.USERNAME);
    }

}
