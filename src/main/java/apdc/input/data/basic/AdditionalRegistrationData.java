package apdc.input.data.basic;

import apdc.input.data.InputDataInterface;
import apdc.input.verification.utils.InputDataVerification;

public class AdditionalRegistrationData implements InputDataInterface {
    public String address;
    public String name;
    public String zipcode;
    public int phoneNumber;
    public int nif;
    public String imageID;

    public AdditionalRegistrationData() {
    }

    @Override
    public boolean validateData() {
        boolean valid = true;

        name = name == null ? "" : InputDataVerification.cleanText(name);
        zipcode = zipcode == null ? "" : zipcode;
        imageID = imageID == null ? "" : imageID;
        address = address == null ? "" : InputDataVerification.cleanText(address);

        if (!name.isEmpty())
            valid &= InputDataVerification.validateText(name, InputDataVerification.TYPE.NAME);

        if (phoneNumber != 0)
            valid &= InputDataVerification.validateNumber(phoneNumber, InputDataVerification.TYPE.PHONE_NUMBER);

        if (!zipcode.isEmpty())
            valid &= InputDataVerification.validateText(zipcode, InputDataVerification.TYPE.ZIPCODE);

        if (nif != 0)
            valid &= InputDataVerification.validateNumber(nif, InputDataVerification.TYPE.NIF);

        if (!imageID.isEmpty())
            valid &= InputDataVerification.validateText(imageID, InputDataVerification.TYPE.IMAGE);

        if (!address.isEmpty())
            valid &= InputDataVerification.validateText(address, InputDataVerification.TYPE.ADDRESS);

        return valid;
    }
}