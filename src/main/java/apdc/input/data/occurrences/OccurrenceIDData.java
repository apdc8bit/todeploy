package apdc.input.data.occurrences;

import apdc.input.data.InputDataInterface;
import apdc.input.verification.utils.InputDataVerification;

public class OccurrenceIDData implements InputDataInterface {
    public long id;
    public String username;

    public OccurrenceIDData() {
    }

    @Override
    public boolean validateData() {
        username = username == null ? "" : username.toLowerCase();

        if (id == 0L || username.isEmpty())
            return false;

        return InputDataVerification.validateText(username, InputDataVerification.TYPE.USERNAME) &&
                InputDataVerification.validateNumber(id, InputDataVerification.TYPE.ID);
    }
}
