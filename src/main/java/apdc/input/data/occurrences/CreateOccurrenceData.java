package apdc.input.data.occurrences;

import apdc.input.data.InputDataInterface;
import apdc.input.verification.utils.InputDataVerification;
import apdc.input.verification.values.OccurrenceValues;

import java.util.ArrayList;
import java.util.List;

public class CreateOccurrenceData implements InputDataInterface {
    public String title;
    public String description;
    public String type;
    public double lat;
    public double lng;
    public String imageID;
    public List<Double> lats;
    public List<Double> lngs;

    public CreateOccurrenceData() {
    }

    public boolean validateData() {
        imageID = imageID == null ? "" : InputDataVerification.cleanText(imageID);
        title = title == null ? "" : InputDataVerification.cleanText(title);
        description = description == null ? "" : InputDataVerification.cleanText(description);
        type = type == null ? "" : OccurrenceValues.getType(type);
        lats = lats == null ? new ArrayList<Double>() : lats;
        lngs = lngs == null ? new ArrayList<Double>() : lngs;

        boolean valid;

        if (lats.isEmpty() || lngs.isEmpty()) {
            valid = InputDataVerification.validateNumber(lat, InputDataVerification.TYPE.LATITUDE) &&
                    InputDataVerification.validateNumber(lng, InputDataVerification.TYPE.LONGITUDE);

        } else if (valid = InputDataVerification.validateCoordinates(lats, lngs)) {
            double[] points = InputDataVerification.generateCenter(lats, lngs);
            lat = points[0];
            lng = points[1];
        } else {
            return false;
        }

        valid = valid && (imageID.isEmpty() || InputDataVerification.validateText(imageID, InputDataVerification.TYPE.IMAGE));

        valid = valid && InputDataVerification.validateText(title, InputDataVerification.TYPE.TITLE) &&
                InputDataVerification.validateText(description, InputDataVerification.TYPE.DESCRIPTION);

        return valid;
    }

}