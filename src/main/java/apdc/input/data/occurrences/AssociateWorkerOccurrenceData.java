package apdc.input.data.occurrences;

import apdc.input.data.InputDataInterface;
import apdc.input.verification.utils.InputDataVerification;

public class AssociateWorkerOccurrenceData implements InputDataInterface {
    public String username_occurrence;
    public long id_occurrence;

    public String username_worker;

    public AssociateWorkerOccurrenceData() {

    }

    @Override
    public boolean validateData() {
        username_worker = username_worker == null ? "" : username_worker.toLowerCase();

        if (username_worker.isEmpty() || id_occurrence == 0L)
            return false;

        return InputDataVerification.validateText(username_worker, InputDataVerification.TYPE.USERNAME) &&
                InputDataVerification.validateNumber(id_occurrence, InputDataVerification.TYPE.ID);
    }
}
