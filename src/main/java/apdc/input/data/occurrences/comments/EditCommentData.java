package apdc.input.data.occurrences.comments;

import apdc.input.data.InputDataInterface;
import apdc.input.verification.utils.InputDataVerification;

public class EditCommentData extends CommentIDData implements InputDataInterface {
    public String comment;

    public EditCommentData() {
    }

    @Override
    public boolean validateData() {
        if (!super.validateData())
            return false;

        comment = comment == null ? "" : InputDataVerification.cleanText(comment);

        if (comment.isEmpty())
            return false;

        return InputDataVerification.validateText(comment, InputDataVerification.TYPE.COMMENT);
    }


}

