package apdc.input.data.occurrences.comments;

import apdc.input.data.InputDataInterface;
import apdc.input.verification.utils.InputDataVerification;

public class CommentIDData implements InputDataInterface {
    public long comment_id;

    public CommentIDData() {
    }

    @Override
    public boolean validateData() {
        return InputDataVerification.validateNumber(comment_id, InputDataVerification.TYPE.ID);
    }
}
