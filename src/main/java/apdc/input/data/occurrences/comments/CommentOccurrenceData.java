package apdc.input.data.occurrences.comments;

import apdc.input.data.InputDataInterface;
import apdc.input.verification.utils.InputDataVerification;

public class CommentOccurrenceData implements InputDataInterface {
    public String comment;
    public long id;
    public String username;

    public CommentOccurrenceData() {
    }

    @Override
    public boolean validateData() {

        comment = comment == null ? "" : InputDataVerification.cleanText(comment);
        username = username == null ? "" : username.toLowerCase();

        if (username.isEmpty() || comment.isEmpty())
            return false;

        return InputDataVerification.validateText(comment, InputDataVerification.TYPE.COMMENT) &&
                InputDataVerification.validateText(username, InputDataVerification.TYPE.USERNAME);
    }


}
