package apdc.input.data.occurrences;

import apdc.input.data.InputDataInterface;

public class ReportOccurrenceData extends OccurrenceIDData implements InputDataInterface {
    public String report;

    @Override
    public boolean validateData() {
        report = report == null ? "" : report.toLowerCase();
        return super.validateData();
    }
}
