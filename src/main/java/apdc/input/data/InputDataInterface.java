package apdc.input.data;

public interface InputDataInterface {
    //Verifies input data using regex or sanitation and word limits
    boolean validateData();
}