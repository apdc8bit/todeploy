package apdc.input.data.admin;

import apdc.input.data.InputDataInterface;
import apdc.input.verification.utils.InputDataVerification;
import apdc.input.verification.values.OccurrenceValues;

public class ChangeStatusOccurrenceData implements InputDataInterface {
    public final long id;
    public String username;
    public String status;

    public ChangeStatusOccurrenceData(long id, String username, String status) {
        this.id = id;
        this.username = username;
        this.status = status;
    }

    @Override
    public boolean validateData() {
        status = status == null ? "" : OccurrenceValues.getStatus(status);
        username = username == null ? "" : username.toLowerCase();

        if (status.isEmpty() || username.isEmpty() || id == 0L)
            return false;


        return InputDataVerification.validateText(username, InputDataVerification.TYPE.USERNAME) &&
                InputDataVerification.validateNumber(id, InputDataVerification.TYPE.ID);
    }
}
