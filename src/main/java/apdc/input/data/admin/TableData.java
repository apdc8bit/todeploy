package apdc.input.data.admin;

import apdc.input.data.InputDataInterface;

public class TableData extends AdminPassData implements InputDataInterface {

    public String table;

    @Override
    public boolean validateData() {
        table = table == null ? "" : table;

        return super.validateData();
    }

}
