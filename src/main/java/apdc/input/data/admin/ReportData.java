package apdc.input.data.admin;

import apdc.input.data.InputDataInterface;
import apdc.input.verification.utils.InputDataVerification;

public class ReportData extends AdminPassData implements InputDataInterface {
    public long occ_id;
    public boolean confirmation;

    public ReportData() {
    }

    @Override
    public boolean validateData() {
        return super.validateData() && InputDataVerification.validateNumber(occ_id, InputDataVerification.TYPE.ID);
    }
}
