package apdc.input.data.entity;

import apdc.input.data.InputDataInterface;
import apdc.input.data.basic.DeleteInfoData;
import apdc.input.verification.utils.InputDataVerification;

public class DeleteWorkerInfoData extends DeleteInfoData implements InputDataInterface {
    public String username;

    public DeleteWorkerInfoData() {
    }

    @Override
    public boolean validateData() {
        username = username == null ? "" : username.toLowerCase();

        return InputDataVerification.validateText(username, InputDataVerification.TYPE.USERNAME);
    }
}
