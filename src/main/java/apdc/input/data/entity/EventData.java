package apdc.input.data.entity;

import apdc.input.data.InputDataInterface;
import apdc.input.verification.utils.InputDataVerification;

public class EventData implements InputDataInterface {
    public String workerUsername;
    public String title;
    public String description;
    public String eventTime;
    public double lat;
    public double lng;

    @Override
    public boolean validateData() {
        workerUsername = workerUsername == null ? "" : workerUsername.toLowerCase();
        title = title == null ? "" : InputDataVerification.cleanText(title);
        description = description == null ? "" : InputDataVerification.cleanText(description);

        if (eventTime == null)
            return false;

        return InputDataVerification.validateText(workerUsername, InputDataVerification.TYPE.USERNAME)
                && InputDataVerification.validateText(title, InputDataVerification.TYPE.TITLE)
                && InputDataVerification.validateText(description, InputDataVerification.TYPE.DESCRIPTION)
                && InputDataVerification.validateNumber(lat, InputDataVerification.TYPE.LATITUDE)
                && InputDataVerification.validateNumber(lng, InputDataVerification.TYPE.LONGITUDE);
    }
}
