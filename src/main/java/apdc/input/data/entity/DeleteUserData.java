package apdc.input.data.entity;

import apdc.input.data.InputDataInterface;
import apdc.input.verification.utils.InputDataVerification;

public class DeleteUserData implements InputDataInterface {
    public String password;

    public DeleteUserData() {
    }

    @Override
    public boolean validateData() {
        password = password == null ? "" : password;
        return InputDataVerification.validateText(password, InputDataVerification.TYPE.PASSWORD);
    }
}
