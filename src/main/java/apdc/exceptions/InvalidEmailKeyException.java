package apdc.exceptions;

public class InvalidEmailKeyException extends RuntimeException {
    static final long serialVersionUID = 0L;

    public InvalidEmailKeyException() {
        super();
    }

    public InvalidEmailKeyException(String message) {
        super(message);
    }

}