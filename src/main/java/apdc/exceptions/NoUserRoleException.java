package apdc.exceptions;

public class NoUserRoleException extends RuntimeException {
    static final long serialVersionUID = 0L;

    public NoUserRoleException() {
        super();
    }

    public NoUserRoleException(String message) {
        super(message);
    }
}
