package apdc.exceptions;

public class AlreadyValidatedException extends RuntimeException {

    static final long serialVersionUID = 0L;

    public AlreadyValidatedException() {
        super();
    }

    public AlreadyValidatedException(String message) {
        super(message);
    }
}

