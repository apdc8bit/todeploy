package apdc.exceptions;

public class UserIsNotConfirmedException extends RuntimeException {

    static final long serialVersionUID = 0L;

    public UserIsNotConfirmedException() {
        super();
    }

    public UserIsNotConfirmedException(String message) {
        super(message);
    }
}
