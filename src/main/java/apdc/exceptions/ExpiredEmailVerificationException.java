package apdc.exceptions;

public class ExpiredEmailVerificationException extends RuntimeException {

    static final long serialVersionUID = 0L;

    public ExpiredEmailVerificationException() {
        super();
    }

    public ExpiredEmailVerificationException(String message) {
        super(message);
    }
}
