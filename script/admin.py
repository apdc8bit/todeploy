import json
import pprint
import requests
import script.urls as actions
from script.urls import URLS

from script.storage import Storage

urls = URLS()

pp = pprint.PrettyPrinter(indent=4)

st = Storage()


def create(username, password, email, admin_password):
    payload = json.dumps(dict(
        username=username,
        password=password,
        confirmation=password,
        email=email,
        admin_password=admin_password))

    r = requests.post(
        urls.get_url(actions.CREATE_ADMIN),
        data=payload,
        headers=st.get_headers())

    print(f'Create Admin: {r.status_code}')

    return r.ok, None


def first_admin():
    r = requests.get(
        urls.get_url(actions.FIRST_ADMIN),
        headers=st.get_headers())

    print(f'Adding First Admin: {r.status_code}')

    return r.ok, None


def admin_login(username, password):
    payload = json.dumps(dict(
        username=username,
        password=password))

    r = requests.post(
        urls.get_url(actions.ADMIN_LOGIN),
        data=payload,
        headers=st.get_headers())

    print(f'Login Admin: {r.status_code}')

    if r.ok:
        reply = r.json()
        token = reply['token']
        session = reply['session']
        st.set_headers(token, session)

    return r.ok, None
