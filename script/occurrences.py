import json
import pprint

import requests

import script.urls as actions
from script.urls import URLS

from script.storage import Storage

urls = URLS()

pp = pprint.PrettyPrinter(indent=4)

st = Storage()


def create(title, description, type_occ, lat, lng, image_id, lats, lngs):
    print('Create Occ:')
    payload = json.dumps(dict(
        title=title,
        description=description,
        type=type_occ,
        lat=lat,
        lng=lng,
        imageID=image_id,
        lats=lats,
        lngs=lngs))

    r = requests.post(
        urls.get_url(actions.NEW_OCC),
        data=payload,
        headers=st.get_headers())

    pp.pprint(r.status_code)
    reply = r.json()
    # pp.pprint(reply)

    if r.ok:
        id_occ = reply['id']
        username = reply['username']
        st.set_occ(id_occ, username)

    return r.ok, reply


def list_hash(occ_hash, cluster):
    print('List Occ Hash:')

    r = requests.get(
        urls.get_url(f'{urls.OCC_BY_HASH}/{occ_hash}/{cluster}'),
        headers=st.get_headers())

    pp.pprint(r.status_code)
    reply = r.json()
    pp.pprint(reply)

    return r.ok, r.json()


def report(id, username, report):
    print('Report Occ:')
    payload = json.dumps(dict(
        id=id,
        username=username,
        report=report))

    r = requests.post(
        urls.get_url(urls.REPORT),
        data=payload,
        headers=st.get_headers())

    pp.pprint(r.status_code)
    reply = r.json()
    pp.pprint(reply)

    return r.ok, reply