import json
import pprint
import requests

import script.urls as actions
from script.urls import URLS

from script.storage import Storage

urls = URLS()

pp = pprint.PrettyPrinter(indent=4)

st = Storage()


def register(username, email, password):
    print('Register:')
    payload = json.dumps(dict(
        username=username,
        password=password,
        confirmation=password,
        email=email))

    r = requests.post(
        urls.get_url(actions.REGISTER),
        data=payload,
        headers=st.get_headers())

    pp.pprint(r.status_code)
    reply = r.json()
    pp.pprint(reply)

    return r.ok, r.json()


def login(username, password):
    print('Login:')
    payload = json.dumps(dict(
        username=username,
        password=password))

    r = requests.post(
        urls.get_url(actions.LOGIN),
        data=payload,
        headers=st.get_headers())

    pp.pprint(r.status_code)
    reply = r.json()
    pp.pprint(reply)

    if r.ok:
        token = reply['token']
        session = reply['session']
        st.set_headers(token, session)

    return r.ok, r.json()


def change_pass(password_old, password_new):
    print('Changing Password:')
    payload = json.dumps(dict(
        password_old=password_old,
        password_new=password_new,
        password_new_conf=password_new))

    r = requests.put(
        urls.get_url(actions.CHANGE_PASS),
        data=payload,
        headers=st.get_headers())

    pp.pprint(r.status_code)
    reply = r.json()
    pp.pprint(reply)

    return r.ok, r.json()


def get_info(param):
    print('Get Info:')
    r = requests.get(
        urls.get_url(actions.MY_INFO),
        params={'filter': param},
        headers=st.get_headers())

    pp.pprint(r.status_code)
    reply = r.json()
    pp.pprint(reply)

    return r.ok, r.json()


def ping():
    url = urls.get_url(actions.PING)
    print(url)
    try:
        r = requests.get(
            url,
            headers=st.get_headers())
    except:
        return False, 404, 'malformed url'

    # pp.pprint(r.status_code)
    # reply = r.json()
    # pp.pprint(reply)

    ok = True if r.status_code == 401 else False

    return ok, r.status_code, 'not found'


def stats_basic(days):
    print('Basic Stats:')
    r = requests.get(
        urls.get_url(f'{actions.BASIC_STATS}?days={days}'),
        headers=st.get_headers())

    pp.pprint(r.status_code)
    reply = r.json()

    with open('stats.json', 'w') as outfile:
        json.dump(reply, outfile)
    # pp.pprint( type(reply) )
    # pp.pprint(json.dumps(reply))

    return r.ok, r.json()


def req_pass_req(username, email):
    print('Request Password Recovery:')
    payload = json.dumps(dict(
        username=username,
        email=email))

    r = requests.put(
        urls.get_url(actions.REQ_RECOVER_PASS),
        data=payload,
        headers=st.get_headers())

    pp.pprint(r.status_code)
    reply = r.json()
    pp.pprint(reply)

    return r.ok, r.json()


def list_my():
    print('List My:')
    r = requests.get(
        urls.get_url(actions.FULL_STATS),
        headers=st.get_headers())

    pp.pprint(r.status_code)
    reply = r.json()
    pp.pprint(reply)

    return r.ok, r.json()


def list_reports(password):
    print('List Reports:')
    payload = json.dumps(dict(
        password=password))

    r = requests.post(
        urls.get_url(actions.LIST_REPORTS),
        data=payload,
        headers=st.get_headers())

    pp.pprint(r.status_code)
    reply = r.json()
    pp.pprint(reply)

    return r.ok, r.json()


