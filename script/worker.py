import json
import pprint
import requests

import script.urls as urls

from script.storage import Storage

pp = pprint.PrettyPrinter(indent=4)

st = Storage()


def list_jobs():
    print('List Worker Jobs:')

    r = requests.get(
        urls.get_url(urls.GET_JOBS),
        headers=st.get_headers())

    pp.pprint(r.status_code)
    reply = r.json()
    pp.pprint(reply)

    return r.ok, r.json()


def change_type(id, value):
    print('Changing Type:')
    payload = json.dumps(dict(
        id=id,
        value=value))

    r = requests.put(
        urls.get_url(urls.CHANGE_TYPE),
        data=payload,
        headers=st.get_headers())

    pp.pprint(r.status_code)
    reply = r.json()
    pp.pprint(reply)

    return r.ok, r.json()

def mark_comp(occurrence_id, imageID):
    print('Marking Completed:')
    payload = json.dumps(dict(
        occurrence_id=occurrence_id,
        imageID=imageID))

    r = requests.put(
        urls.get_url(urls.MARK_COMP),
        data=payload,
        headers=st.get_headers())

    pp.pprint(r.status_code)
    reply = r.json()
    pp.pprint(reply)

    return r.ok, r.json()
