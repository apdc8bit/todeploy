class Storage:

    __token = ''
    __session = ''

    __markers = []

    @staticmethod
    def get_headers():
        return {
            'Content-type': 'application/json',
            'Authorization': Storage.__token,
            'SessionID': Storage.__session
        }

    @staticmethod
    def set_headers(token, session):
        Storage.__token = token
        Storage.__session = session

    @staticmethod
    def set_occ(occ_id, occ_username):
        marker = dict(
            username=occ_username,
            id=occ_id
        )
        Storage.__markers.append(marker.copy())

    @staticmethod
    def get_occ(pos):
        return Storage.__markers[pos]

    @staticmethod
    def get_occs():
        return Storage.__markers

