def get_url_back(action):
    if IN_PRODUCTION:
        return BASE_URL_PRODUCTION + action
    else:
        return BASE_URL_TESTING + action


class URLS:
    url = None

    @staticmethod
    def set_url(url):
        URLS.url = url + '/rest/w/'

    @staticmethod
    def get_url(action):
        if not URLS.url:
            return get_url_back(action)
        return URLS.url + action


IN_PRODUCTION = False
BASE_URL_PRODUCTION = 'https://gaea-8bit.appspot.com/rest/w/'
BASE_URL_TESTING = 'http://localhost:8080/rest/w/'

# BASIC USER REQUESTS
# setting info
REGISTER = 'user/register'
LOGIN = 'user/login'
ADD_INFO = 'user/add_info'
LOGOUT = 'user/logout'
PING = 'user/ping'
CHANGE_PASS = 'user/password_change'
REQ_RECOVER_PASS = 'user/requestPasswordRecovery'
# getting info
MY_INFO = 'userInfo/getInfo'
BASIC_STATS = 'globalStats/getBasicStats'
FULL_STATS = 'globalStats/getFullStats'

# OCCURRENCES RELATED REQUESTS
MY_VOTES = 'userInfo/myVotes'
MY_COMMENTS = 'userInfo/myComments'
OCC_BY_HASH = 'occurrenceInfo/getMarkers'

NEW_OCC = 'occurrence/add'
VOTE_OCC = 'occurrence/vote'
DEL_VOTE_OCC = 'occurrence/deleteVote'
COMMENT_OCC = 'occurrence/comment'
DEL_COMMENT_OCC = 'occurrence/deleteComment'
EDIT_COMMENT_OCC = 'occurrence/editComment'

MY_OCC = 'occurrenceInfo/myOccurrences'
LIST_USERS = 'special/listUsers'
DELETE_USERS = 'special/deleteUser'
REPORT = 'occurrence/report'

# Entity
REGISTER_ENTITY = 'entity/register'
CREATE_WORKER = 'entity/create_worker'
LIST_WORKERS = 'entity/list_workers'
ASS_OCC = 'entity/associate_occurrence'
REMOVE_ASS_OCC = 'entity/remove_association'
LIST_JOBS = 'entity/list_jobs'
LIST_OCC_AREA = 'entity/listOccurrencesInArea'
CONF_JOB = 'entity/confirm_job'
LIST_ALL = 'entity/listAllEntities'
FIX = 'special/testing'
FIX_E = 'entity/fix'

# Worker
GET_JOBS = 'worker/getJobs'
CHANGE_TYPE = 'worker/changeType'
MARK_COMP = 'worker/markCompleted'

# Admin
CREATE_ADMIN = 'special/createAdmin'
LIST_REPORTS = 'special/listReports'
ADMIN_LOGIN = 'special/login'
FIRST_ADMIN = 'special/initializeApplication'

# Volunteer
CREATE_EVENT = 'event/create'
LIST_MY_EVENTS = 'event/listMy'
LIST_ALL_EVENTS = 'event/listAll?filter=false'
JOIN_EVENT = 'event/join'
ABSTAIN_EVENT = 'event/abstain'
REMOVE_USER_EVENT = 'event/removeUser'
ARCHIVE_EVENT = 'event/archive'
GET_EVENT = 'event/get'
