import json
import pprint
import requests
import sys

import script.urls as actions
from script.urls import URLS

from script.storage import Storage

urls = URLS()

pp = pprint.PrettyPrinter(indent=4)

st = Storage()


def register(username, password, email, name, phoneNumber, zipcode, nif, address):
    print('Register Entity:')
    payload = json.dumps(dict(
        username=username,
        password=password,
        confirmation=password,
        email=email,
        name=name,
        phoneNumber=phoneNumber,
        zipcode=zipcode,
        nif=nif,
        address=address,
        isVolunteer=True))

    r = requests.post(
        urls.get_url(actions.REGISTER_ENTITY),
        data=payload,
        headers=st.get_headers())

    pp.pprint(r.status_code)
    reply = r.json()
    pp.pprint(reply)

    return r.ok, r.json()


def new_worker(username, password, email, name, phoneNumber, zipcode, nif, address):
    print('New Worker:')
    payload = json.dumps(dict(
        username=username,
        password=password,
        confirmation=password,
        email=email,
        name=name,
        phoneNumber=phoneNumber,
        zipcode=zipcode,
        nif=nif,
        address=address))

    r = requests.post(
        urls.get_url(actions.CREATE_WORKER),
        data=payload,
        headers=st.get_headers())

    pp.pprint(r.status_code)
    reply = r.json()
    pp.pprint(reply)

    return r.ok, r.json()


def list_workers():
    print('List Workers:')

    r = requests.get(
        urls.get_url(actions.LIST_WORKERS),
        headers=st.get_headers())

    pp.pprint(r.status_code)
    reply = r.json()
    pp.pprint(reply)

    return r.ok, r.json()


def create_event(title, description, workerUsername, eventTime, lat, lng):
    print('Create Event:')
    payload = json.dumps(dict(
        title=title,
        description=description,
        workerUsername=workerUsername,
        eventTime=eventTime,
        lat=lat,
        lng=lng))

    r = requests.post(
        urls.get_url(actions.CREATE_EVENT),
        data=payload,
        headers=st.get_headers())

    pp.pprint(r.status_code)
    reply = r.json()
    pp.pprint(reply)

    return r.ok, r.json()


def list_my():
    print('List My Events:')

    r = requests.get(
        f'{urls.get_url(actions.LIST_MY_EVENTS)}?filter=archived',
        headers=st.get_headers())

    pp.pprint(r.status_code)
    reply = r.json()
    pp.pprint(reply)

    return r.ok, r.json()


def list_all():
    print('List All Events:')

    r = requests.get(
        urls.get_url(actions.LIST_ALL_EVENTS),
        headers=st.get_headers())

    pp.pprint(r.status_code)
    reply = r.json()
    pp.pprint(reply)

    return r.ok, r.json()

def get_event(id):
    print('Get Event:')

    r = requests.get(
        f'{urls.get_url(actions.GET_EVENT)}/{id}',
        headers=st.get_headers())

    pp.pprint(r.status_code)
    reply = r.json()
    pp.pprint(reply)

    return r.ok, r.json()


def join_event(id):
    print('Join Event:')

    r = requests.post(
        f'{urls.get_url(actions.JOIN_EVENT)}/{id}',
        headers=st.get_headers())

    pp.pprint(r.status_code)
    reply = r.json()
    pp.pprint(reply)

    return r.ok, r.json()

def abstain_event(id):
    print('Abstain Event:')

    r = requests.delete(
        f'{urls.get_url(actions.ABSTAIN_EVENT)}/{id}',
        headers=st.get_headers())

    pp.pprint(r.status_code)
    reply = r.json()
    pp.pprint(reply)

    return r.ok, r.json()

def remove_user(id, username):
    print('Remove User Event:')
    try:
        r = requests.delete(
            f'{urls.get_url(actions.REMOVE_USER_EVENT)}/{id}/{username}',
            headers=st.get_headers())
    except requests.HTTPError as e:
        print(e)
        sys.exit(1)
    
    pp.pprint(r.status_code)
    reply = r.json()
    pp.pprint(reply)

    return r.ok, r.json()


def archive_event(id):
    print('Archive Event:')
    try:
        r = requests.put(
            f'{urls.get_url(actions.ARCHIVE_EVENT)}/{id}',
            headers=st.get_headers())
    except requests.HTTPError as e:
        print(e)
        sys.exit(1)
    
    pp.pprint(r.status_code)
    reply = r.json()
    pp.pprint(reply)

    return r.ok, r.json()

