import sys
import script.user as user
import script.occurrences as occ
import script.entity as entity
import script.worker as worker
import script.admin as admin
import script.volunteer
import os
import random
import json
from script.storage import Storage

PASSWORD_ADMIN = 'WoWS05eCuRE'
PASSWORD = 'Password1'

USERS = dict(
    admins=['admin1', 'admin2'],
    basics=['davidmiguel', 'anasofia', 'pedroalmeida', 'migueldavid', 'faridaly'],
    entities=['almadaconsigo', 'camaralisboa', 'melhorarportugal'],
    workers=['joaorelvas', 'antonioareu']
)

os.system('cls' if os.name == 'nt' else 'clear')

st = Storage()

markers = [dict(

)]


class Run:

    def __init__(self, url, fill_b):
        self.url = url
        self.__fill = fill_b
        self.start()

    def start(self):
        print('Starting...')

        if self.__fill:
            fill()
        else:
            initialize()

        print('Done')
        print_users()
        sys.exit()


def fill():
    """Initializes and Fills the database with 'random' data"""
    initialize()


def initialize():
    """Initializes the database"""
    admin.first_admin()
    admin.admin_login(USERS['admins'][0], PASSWORD_ADMIN)
    admin.create(USERS['admins'][1], PASSWORD_ADMIN, f'{USERS["admins"][1]}@admin.com', PASSWORD_ADMIN)
    del USERS['basics']
    del USERS['entities']
    del USERS['workers']


def print_users():
    print('\nAVAILABLE USERS:\n')
    for role in USERS.keys():
        print('-Role: ' + role)
        for username in USERS[role]:
            print('===Username: ' + username)
        print('\n')
    print('-Passwords: ')
    print('===Admins: ' + PASSWORD_ADMIN)
    print('===Other: ' + PASSWORD)


def new_basic_users(count):
    for x in range(1, count):
        user.register(f'basicuser{x}', f'basicuser{x}@gmail.com', 'aaAA11')

    user.register('admin1', 'admin1@gmail.com', 'aaAA11')
    user.register('admin2', 'admin2@gmail.com', 'aaAA11')


def confirm(ok):
    if not ok:
        sys.exit()


def create_occurrences(count):
    status, _ = user.login('basicuser12', 'aaAA11')
    confirm(status)
    for x in range(0, count):
        lat = random.uniform(36.9, 42.0)
        lng = random.uniform(-9.6, -6.1)
        # lat = random.uniform(38.787141, 38.733197)
        # lng = random.uniform(-9.163971, -9.281902)
        status, _ = occ.create(f'Novas Occs {x}', f'lixo, limpa {x}', 'tree_cutting', lat, lng, 'vUGsa8bdae6', [], [])

    print(st.get_occs())


def fill_back_office():
    address = [
        {"ad": "Largo Forte São João de Deus", "zip": "5300-263"},
        {"ad": "Praça 8 de Maio", "zip": "3000-300"},
        {"ad": "Cais da Fonte Nova", "zip": "3811-904"},
        {"ad": "Praça do Sertório", "zip": "7004-506"}
    ]

    for x in range(1, 5):
        status, _ = entity.register(f'New_Age_Entity_{x}', 'aaAA11', f'new_entity{x}@entity.com', f'Entity Name {x}',
                                    987654321, address[x - 1]["zip"], 123456789, address[x - 1]["ad"])
        confirm(status)

    for x in range(1, 5):
        status, _ = user.login(f'New_Age_Entity_{x}', 'aaAA11')
        confirm(status)
        for y in range(1, 7):
            status, _ = entity.new_worker(f'new_age_worker_{x*10+y}', 'aaAA11', f'new_worker{x*10+y}@worker.com',
                                          f'Worker Name {x*10+y}', 987654321, "1250-096", 123456789, "Av. da Liberdade")
            confirm(status)


def associate_occurrences(workers, jobs):
    status, _ = user.login('New_Age_Entity_1', 'aaAA11')
    confirm(status)
    for x in range(1, workers):
        for y in range(x * jobs, x * jobs + jobs):
            entity.ass_occ(st.get_occ(y)['username'], st.get_occ(y)['id'], f'new_age_worker_1{x}')


def worker_finalize_jobs(workers, jobs):
    for x in range(1, workers):
        user.login(f'new_age_worker_1{x+1}', 'aaAA11')
        for y in range(x * jobs, x * jobs + jobs):
            print(st.get_occ(y)['id'])
            worker.mark_comp(st.get_occ(y)['id'], 'dc5o9tf6L9')


def entity_confirm_jobs(workers, jobs):
    status, _ = user.login('New_Age_Entity_1', 'aaAA11')
    confirm(status)
    for x in range(1, workers):
        for y in range(x * jobs, x * jobs + jobs):
            entity.confirm_job(st.get_occ(y)['username'], st.get_occ(y)['id'], f'new_age_worker_1{x+1}', True)

# reports comments votes


'''
new_users(20)
entity.register('entity99', 'aaAA11', 'new_entity99@gmail.com', 'Entity Name99', 987654321, 'Rua Dona Ines', 123456789, '2605-455')
create_occ(1000)
fill_back_office()
ass_occ(7, 6)
worke_fin(7, 6)
conf_job(7, 6)
entity.register('entity1', 'aaAA11', 'entity1@entity.com', 'Entity 1', 987654321, "2605-243", 123456789, "Rua D. Inês de Castro")
user.login('entity1', 'aaAA11')
entity.new_worker('worker1', 'aaAA11', 'worker1@worker.com', 'Worker 1', 987654321, "1250-096", 123456789, "Av. da Liberdade")
entity.list_all()
entity.register('Ealmada', 'aaAA11', 'entityalmada@entity.com', 'Camara de Almada', 987654321, "2800-158", 123456789, "Largo Luís de Camões")
entity.register('Esintra', 'aaAA11', 'entitysintra@entity.com', 'Camara de Sintra', 987654321, "2714-501", 123456789, "Largo Dr. Virgílio Horta")

fill_back_office()
working_workers()
ass_occ()
rm_ass_occ()
user.login('entity_1', 'aaAA11')
user.ping()
entity.list_jobs('WAITING_CONFIRMATION')
entity.list_occ_area()
entity.confirm_job('userBasic1', 4660622021099520, 'worker_11', True)

user.login('userBasic2', 'aaAA11')
admin.create('admin1', 'aaAA11', 'admin1@gmail.com', 'aaAA11')

entity.list_all()
user.register('basicUser1', 'aaAA11', 'admin@admin1.com')
user.login('basicUser1', 'aaAA11')
occ.create(f'AAA Test', f'testing new hashes', 'other', 38.713911, -9.133759, '', [], [])
confirm(status)
user.get_info('all')
confirm(status)
user.ping()
confirm(status)
occ.list_hash('eyc7aaaaaa', False)


user.register('emailtest', 'd.carpinteiro@campus.fct.unl.pt', 'aaAA11')
user.login('emailtest', 'aaAA11')
user.req_pass_req('emailtest', 'd.carpinteiro@campus.fct.unl.pt')
user.stats_basic(5)

user.stats_full()

user.login('userBasic8', 'aaAA11')
user.change_pass('aaAA11', 'aaAA22')

user.login('New_Age_Entity_1', 'aaAA11')
entity.ass_occ('basicuser1', 1129788880, 'new_age_worker_11')
entity.ass_occ('basicuser1', 1227900393 , 'new_age_worker_11')
entity.ass_occ('basicuser1', 1394036114 , 'new_age_worker_11')


user.login('volunteerentity', 'aaAA11')
occ.report(1129788880, 'basicuser1', 'sexual')
occ.report(1227900393, 'basicuser1', 'offensive')
occ.report(1394036114, 'basicuser1', 'violent')


volunteer.register('volunteerentity', 'aaAA11', 'volunteerentity@gmail.com', 'Voluntariados', 987654321, '1234-567', 123456789, 'A Tua Mae')

volunteer.new_worker('volunteerworker', 'aaAA11', 'volenteerworker@gmail.com','Trabalhador Voluntario', 987654321, '1234-567', 123456789, 'A Tua Mae')

volunteer.list_workers()

user.login('volunteerentity', 'aaAA11')
volunteer.create_event('Novo asdasd', 'Temos asdasd', 'volunteerworker', '12/08/2018 13:40', 38.7, -9.1)
sys.exit()
volunteer.list_my()

user.login('basicuser10', 'aaAA11')
volunteer.list_my()
volunteer.join_event(5731568492478464)

user.login('basicuser11', 'aaAA11')

volunteer.join_event(5686147871145984)

volunteer.list_my()

user.login('volunteerworker', 'aaAA11')

volunteer.list_my()

#volunteer.abstain_event(5686147871145984)

volunteer.list_all()
volunteer.archive_event(5686147871145984)
sys.exit()
user.login('volunteerworker', 'aaAA11')

volunteer.remove_user(5686147871145984, 'basicuser10')


user.login('basicuser15', 'aaAA11')

occ.report(1129788880, 'basicuser1', 'sexual')
occ.report(1227900393, 'basicuser1', 'offensive')
occ.report(1394036114, 'basicuser1', 'violent')


user.admin_login('admin1', 'aaAA11')
user.list_reports('aaAA11')

user.login('basicuser15', 'aaAA11')
create_occ(20)

user.login('New_Age_Entity_1', 'aaAA11')
entity.ass_occ('basicuser1', 182922174, 'new_age_worker_11')


user.login('basicuser15', 'aaAA11')
occ.report(265150807 , 'basicuser6', 'violent')
occ.report(286750448 , 'basicuser6', 'violent')
occ.report(306168020, 'basicuser6', 'violent')
occ.report(380236815, 'basicuser6', 'sexual')
occ.report(412696197, 'basicuser6', 'violent')
occ.report(425058877, 'basicuser6', 'violent')
occ.report(545660784, 'basicuser6', 'violent')

user.login('basicuser14', 'aaAA11')
occ.report(380236815, 'basicuser6', 'sexual')
occ.report(412696197, 'basicuser6', 'violent')
occ.report(425058877, 'basicuser6', 'violent')
occ.report(545660784, 'basicuser6', 'violent')

volunteer.list_all()

entity.register('novaentity', 'aaAA11', 'novaeeee@entity.com', 'Heroo', 987654321, "2605-243", 123456789, "Rua D. Inês de Castro")

volunteer.get_event(5711898079133696)

user.register()

user.login('volunteerentity', 'aaAA11')
volunteer.create_event('Funciona', 'Temos asdasd', 'volunteerworker', '12/08/2018 13:40', 38.661529, -9.204424)

entity.register('EalAAAAmada', 'aaAA11', 'ensadsadtityalmada@entity.com', 'AAAamara de Almada', 987654321, "2800-158", 123456789, "Largo Luís de Camões")
entity.register('EsintrAAAAa', 'aaAA11', 'entitysadssintra@entity.com', 'CamarAAAAa de Sintra', 987654321, "2714-501", 123456789, "Largo Dr. Virgílio Horta")

entity.register('EalAAs1111AAmada', 'aaAA11', 'ensads7878adtityalmada@entity.com', 'AAAa777mara de Almada', 987654321, "2800-158", 123456789, "Largo Luís de Camões")
entity.register('EsintrAAA787878Aa', 'aaAA11', 'entitys7878adssintra@entity.com', 'Cam7777arAAAAa de Sintra', 987654321, "2714-501", 123456789, "Largo Dr. Virgílio Horta")

notifications.register()

entity.register('entity99', 'aaAA11', 'entity99@entity.com', 'Local de Almada', 987654321, "2800-158", 123456789, "Largo Luís de Camões")
entity.register('entity88', 'aaAA11', 'entity88@entity.com', 'Sitio de Sintra', 987654321, "2714-501", 123456789, "Largo Dr. Virgílio Horta")
'''
