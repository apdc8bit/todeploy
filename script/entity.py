import json
import pprint
import requests
import script.urls as actions
from script.urls import URLS

from script.storage import Storage

urls = URLS()

pp = pprint.PrettyPrinter(indent=4)

st = Storage()


def register(username, password, email, name, phone_number, zip_code, nif, address):
    print('Register Entity:')

    payload = json.dumps(dict(
        username=username,
        password=password,
        confirmation=password,
        email=email,
        name=name,
        phoneNumber=phone_number,
        zipcode=zip_code,
        nif=nif,
        address=address))

    r = requests.post(
        urls.get_url(actions.REGISTER_ENTITY),
        data=payload,
        headers=st.get_headers())

    pp.pprint(r.status_code)
    reply = r.json()
    pp.pprint(reply)

    return r.ok, r.json()


def new_worker(username, password, email, name, phone_number, zipcode, nif, address):
    print('New Worker:')
    payload = json.dumps(dict(
        username=username,
        password=password,
        confirmation=password,
        email=email,
        name=name,
        phoneNumber=phone_number,
        zipcode=zipcode,
        nif=nif,
        address=address))

    r = requests.post(
        urls.get_url(actions.CREATE_WORKER),
        data=payload,
        headers=st.get_headers())

    pp.pprint(r.status_code)
    reply = r.json()
    pp.pprint(reply)

    return r.ok, r.json()


def list_workers():
    print('List Workers:')

    r = requests.get(
        urls.get_url(actions.LIST_WORKERS),
        headers=st.get_headers())

    pp.pprint(r.status_code)
    reply = r.json()
    pp.pprint(reply)

    return r.ok, r.json()


def ass_occ(username_occurrence, id_occurrence, username_worker):
    print('Associate Occurrence:')
    payload = json.dumps(dict(
        username_occurrence=username_occurrence,
        id_occurrence=id_occurrence,
        username_worker=username_worker))

    r = requests.post(
        urls.get_url(actions.ASS_OCC),
        data=payload,
        headers=st.get_headers())

    pp.pprint(r.status_code)
    reply = r.json()
    pp.pprint(reply)

    return r.ok, r.json()


def remove_ass_occ(username_occurrence, id_occurrence, username_worker):
    print('Remove Association Occurrence:')
    payload = json.dumps(dict(
        username_occurrence=username_occurrence,
        id_occurrence=id_occurrence,
        username_worker=username_worker))

    r = requests.put(
        urls.get_url(actions.REMOVE_ASS_OCC),
        data=payload,
        headers=st.get_headers())

    pp.pprint(r.status_code)
    reply = r.json()
    pp.pprint(reply)

    return r.ok, r.json()


def list_jobs(filter_job):
    print('List Jobs:')

    r = requests.get(
        f'{urls.get_url(actions.LIST_JOBS)}?filter={filter_job}',
        headers=st.get_headers())

    pp.pprint(r.status_code)
    reply = r.json()
    pp.pprint(reply)

    return r.ok, r.json()


def list_occ_area():
    print('List Occ Area:')

    r = requests.get(
        urls.get_url(actions.LIST_OCC_AREA),
        headers=st.get_headers())

    pp.pprint(r.status_code)
    reply = r.json()
    pp.pprint(reply)

    return r.ok, r.json()


def confirm_job(username_occurrence, id_occurrence, username_worker, confirm):
    print('Job Confirm:')
    payload = json.dumps(dict(
        username_occurrence=username_occurrence,
        id_occurrence=id_occurrence,
        username_worker=username_worker,
        confirm=confirm))

    r = requests.put(
        urls.get_url(actions.CONF_JOB),
        data=payload,
        headers=st.get_headers())

    pp.pprint(r.status_code)
    reply = r.json()
    pp.pprint(reply)

    return r.ok, r.json()


def list_all():
    print('List All:')
    r = requests.get(
        urls.get_url(actions.LIST_ALL),
        headers=st.get_headers())

    pp.pprint(r.status_code)
    reply = r.json()
    pp.pprint(reply)

    return r.ok, r.json()


